import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { NurseryPlanning } from "../../models/nursery/nurseryPlanning";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class NurseryPlanningService {
  private http: HttpClient;
  private headers: HttpHeaders;
  constructor(private _http: HttpClient) {
    this.http = _http;
    this.headers = new HttpHeaders();
    this.headers.append("Accept", "application/json");
  }

  saveNurseryPlanning(planning: NurseryPlanning) {
    return this.http.post(
      environment.url_server + "/api/nurseryPlanning/",
      planning
    );
  }

  updateNurseryPlanning(planning: NurseryPlanning): Observable<any> {
    return this.http.put(
      environment.url_server + "/api/nurseryPlanning/",
      planning
    );
  }

  deleteNurseryPlanning(id: number) {
    let params = new HttpParams();
    params = params.append("id", id.toString());
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.delete(
      environment.url_server + "/api/nurseryPlanning/",
      options
    );
  }

  getAllNurseryPlanningByNurseryIdAndSeaonId(
    seasonId: string,
    nurseryId: string
  ) {
    let params = new HttpParams();
    params = params.append("nurseryId", nurseryId);
    params = params.append("seasonId", seasonId);
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.get(
      environment.url_server + "/api/nurseryPlanning/",
      options
    );
  }

  getNurseryPlanningById(id: string) {
    return this.http.get(environment.url_server + "/api/nurseryPlanning/" + id);
  }
}
