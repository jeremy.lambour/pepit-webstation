import { TestBed, inject } from '@angular/core/testing';

import { NurseryPlanningService } from './nursery-planning.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('NurseryPlanningService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        NurseryPlanningService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([NurseryPlanningService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
