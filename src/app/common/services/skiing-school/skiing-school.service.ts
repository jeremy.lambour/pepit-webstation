import { Injectable } from "@angular/core";

import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class SkiingSchoolService {
  private http: HttpClient;
  constructor(private _http: HttpClient) {
    this.http = _http;
  }

  getAllSkiingSchool() {
    return this.http.get(environment.url_server + "/api/SkiingSchool/");
  }
}
