import { TestBed, inject } from '@angular/core/testing';

import { SkiingSchoolService } from './skiing-school.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('SkiingSchoolService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        SkiingSchoolService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([SkiingSchoolService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
