import { inject, TestBed } from "@angular/core/testing";
import { PassTypeService } from './passType.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('PassTypeService', () => {

    let service;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                PassTypeService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([PassTypeService], s => {
        service = s;
    }));

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

});
