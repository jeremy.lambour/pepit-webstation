import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class PassTypeService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + '/api/PassType';
    }

    findAll(): Observable<any> {
        return this.http.get<any>(this.url + '/findAll');
    }

}
