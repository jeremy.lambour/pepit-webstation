import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Planning } from "../../models/planning/planning";
import { Lesson } from "../../models/planning/lesson";

@Injectable({
  providedIn: "root"
})
export class PlanningService {
  private http: HttpClient;
  private headers: HttpHeaders;
  constructor(private _http: HttpClient) {
    this.http = _http;
    this.headers = new HttpHeaders();
    this.headers.append("Accept", "application/json");
  }

  createPlanning(planning: Planning) {
    return this.http.post(environment.url_server + "/api/planning/", planning);
  }

  updatePlanning(planning: Planning) {
    return this.http.put(environment.url_server + "/api/planning/", planning);
  }

  getAllPlanningByMonitorIdAndSeasonId(monitorId: string, seasonId: string) {
    let params = new HttpParams();
    params = params.append("monitorId", monitorId);
    params = params.append("seasonId", seasonId);
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.get(environment.url_server + "/api/planning/", options);
  }

  getPlanningById(id: number) {
    return this.http.get(environment.url_server + "/api/planning/" + id);
  }

  deletePlanning(id: number) {
    let params = new HttpParams();
    params = params.append("id", id.toString());
    let options = {
      headers: this.headers,
      params: params
    };

    return this.http.delete(environment.url_server + "/api/planning/", options);
  }
}
