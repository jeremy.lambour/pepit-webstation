import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Lesson } from "../../models/planning/lesson";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class LessonService {
  private http: HttpClient;
  private headers: HttpHeaders;
  constructor(private _http: HttpClient) {
    this.http = _http;
    this.headers = new HttpHeaders();
    this.headers.append("Accept", "application/json");
  }

  saveLesson(lesson: Lesson) {
    return this.http.post(environment.url_server + "/api/lesson/", lesson);
  }

  updateLesson(lesson: Lesson): Observable<any> {
    return this.http.put(environment.url_server + "/api/lesson/", lesson);
  }

  deleteLesson(id: number) {
    let params = new HttpParams();
    params = params.append("id", id.toString());
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.delete(environment.url_server + "/api/lesson/", options);
  }
}
