import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root"
})
export class FlatService {
  public url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url_server + "/api/Flat";
  }

  insertFlat(flat: Flat): Observable<any> {
    return this.http.post(this.url + "/insertFlat", flat);
  }

  getListFlatToDisplay(idHotel: number): Observable<any> {
    return this.http.get(this.url + "/getListFlatToDisplay?idHotel=" + idHotel);
  }

  getFlat(i: number): Observable<any> {
    return this.http.get<any>(this.url + "/getFlat?idFlat=" + i);
  }

  getFlatString(i: string): Observable<any> {
    return this.http.get<any>(this.url + "/getFlatString?i=" + i);
  }

  updateFlat(flat: Flat): Observable<any> {
    return this.http.post(this.url + "/updateFlat", flat);
  }

  deleteFlat(idFlat: number) {
    return this.http.post(this.url + "/deleteFlat", idFlat);
  }

  getAllFlat(): Observable<any> {
    return this.http.get(this.url + "/");
  }
}
