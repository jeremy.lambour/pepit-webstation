
import { FlatService } from './flat.service';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Flat } from 'src/app/common/models/FlatManagement/Flat';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const hotelDto = {
    hotelId: 1,
    hotelName: 'hotelName',
    hotelRoomsNumber: 3,
    phoneNumber: 'phoneNumber',
    mailAddress: 'mailAddress',
    address: 'address',
    userDto: userDto
};

const flatCategoryDto = {
    flatCategoryId: 1,
    flatCategoryName: 'studio 3/4 personnes'
};

const mockData = [
    {
        flatId: 1, flatDescription: 'desc1', flatGuidance: 'guidance1', flatCapacity: 1, hotelDto: hotelDto,
        flatCategoryDto: flatCategoryDto
    },
    {
        flatId: 2, flatDescription: 'desc2', flatGuidance: 'guidance2', flatCapacity: 2, hotelDto: hotelDto,
        flatCategoryDto: flatCategoryDto
    },
    {
        flatId: 3, flatDescription: 'desc3', flatGuidance: 'guidance3', flatCapacity: 3, hotelDto: hotelDto,
        flatCategoryDto: flatCategoryDto
    }
] as Flat[];

const flat = {
    flatId: 1, flatDescription: 'desc1', flatGuidance: 'guidance1', flatCapacity: 1, hotelDto: hotelDto,
    flatCategoryDto: flatCategoryDto
};

const idHotel = 1;
const idFlat = 1;
const idFlatString = '1';

describe('FlatService', () => {

    let service;
    let httpTestingController: HttpTestingController;
    let mockFlats: Flat[] = [];
    let mockFlat: Flat;
    let mockId: number;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FlatService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([FlatService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockFlats = mockData;
        mockFlat = mockFlats[0];
        mockId = mockFlat.flatId;
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('insertFlat', () => {
        it('should return the inserted flat', () => {
            service.insertFlat(mockFlat).subscribe(
                response => expect(response).toEqual(mockFlat), fail
            );

            const req = httpTestingController.expectOne(service.url + '/insertFlat');
            expect(req.request.method).toEqual('POST');
            req.flush(mockFlat);
        });
    });

    describe('getListFlatToDisplay', () => {
        it('should return a list of flat', () => {
            service.getListFlatToDisplay(idHotel).subscribe(
                response => expect(response).toEqual(mockFlats), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getListFlatToDisplay?idHotel=' + idHotel);
            expect(req.request.method).toEqual('GET');
            req.flush(mockFlats);
        });
    });

    describe('getFlat', () => {
        it('should return the flat which has its identifier in parameters as an integer', () => {
            service.getFlat(idFlat).subscribe(
                response => expect(response).toEqual(mockFlat), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getFlat?idFlat=' + idFlat);
            expect(req.request.method).toEqual('GET');
            req.flush(mockFlat);
        });
    });

    describe('getFlatString', () => {
        it('should return the flat which has its identifier in parameters as a string', () => {
            service.getFlatString(idFlatString).subscribe(
                response => expect(response).toEqual(mockFlat), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getFlatString?i=' + idFlatString);
            expect(req.request.method).toEqual('GET');
            req.flush(mockFlat);
        });
    });

    describe('updateFlat', () => {
        it('should return the updated flat', () => {
            service.updateFlat(mockFlat).subscribe(
                response => expect(response).toEqual(mockFlats), fail
            );

            const req = httpTestingController.expectOne(service.url + '/updateFlat');
            expect(req.request.method).toEqual('POST');
            req.flush(mockFlats);
        });
    });

    describe('deleteFlat', () => {
        it('should return the deleted flat', () => {
            service.deleteFlat(idFlat).subscribe(
                response => expect(response).toEqual(mockFlat), fail
            );

            const req = httpTestingController.expectOne(service.url + '/deleteFlat');
            expect(req.request.method).toEqual('POST');
            req.flush(mockFlat);
        });
    });

});
