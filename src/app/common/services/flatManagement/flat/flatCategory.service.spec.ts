
import { TestBed, inject } from '@angular/core/testing';
import { FlatCategoryService } from './flatCategory.service';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

const flatCategory = {
    flatCategoryId: 1,
    flatCategoryName: 'flatCategory'
};

const mockData = [
    {
        flatCategoryId: 1, flatCategoryName: 'flatCategory1'
    },
    {
        flatCategoryId: 2, flatCategoryName: 'flatCategory2'
    },
    {
        flatCategoryId: 3, flatCategoryName: 'flatCategory3'
    }
] as FlatCategory[];


describe('FlatCategoryService', () => {

    let service;
    let httpTestingController: HttpTestingController;

    let mockFlatCategory: FlatCategory;
    let mockFlatCategories: FlatCategory[] = [];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FlatCategoryService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([FlatCategoryService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockFlatCategory = flatCategory;
        mockFlatCategories = mockData;
    });

    // ? je ne sais pas vraiment à quoi ça sert et si le format est bon, je pense que non
    /*const apiUrl = (id: number) => {
        return `${service.url}/${this.mockId}`;
    };*/

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('insertFlatCategory', () => {
        it('should return the inserted flatCategory', () => {
            service.insertFlatCategory(flatCategory).subscribe(
                response => expect(response).toEqual(flatCategory), fail
            );

            const req = httpTestingController.expectOne(service.url + '/insertFlatCategory');
            expect(req.request.method).toEqual('POST');
            req.flush(flatCategory);
        });
    });

    describe('getFlatCategories', () => {
        it('should return a list of flatCategory', () => {
            service.getFlatCategories().subscribe(
                response => expect(response).toEqual(mockFlatCategories), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getFlatCategories');
            expect(req.request.method).toEqual('GET');
            req.flush(mockFlatCategories);
        });
    });

});

