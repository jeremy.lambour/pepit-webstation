import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { Define } from 'src/app/common/models/flatManagement/Define';

@Injectable({
    providedIn: "root"
})
export class DefineService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + "/api/Define";
    }

    getDefine(idFlat): Observable<any> {
        return this.http.get(this.url + "/getDefine?idFlat=" + idFlat);
    }

    updateDefine(define: Define): Observable<any> {
        return this.http.post(this.url + "/updateDefine", define);
    }

}
