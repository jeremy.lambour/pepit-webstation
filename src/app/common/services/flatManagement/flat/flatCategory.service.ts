import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root"
})
export class FlatCategoryService {
  public url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url_server + "/api/FlatCategory";
  }

  insertFlatCategory(flatCategory: FlatCategory): Observable<any> {
    return this.http.post(this.url + "/insertFlatCategory", flatCategory);
  }

  getFlatCategories(): Observable<any> {
    return this.http.get(this.url + "/getFlatCategories");
  }
}
