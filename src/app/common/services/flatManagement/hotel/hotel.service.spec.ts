
import { TestBed, inject } from '@angular/core/testing';
import { HotelService } from './hotel.service';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { Flat } from 'src/app/common/models/flatManagement/Flat';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const hotelDto = {
    hotelId: 1,
    hotelName: 'hotelName',
    hotelRoomsNumber: 3,
    phoneNumber: 'phoneNumber',
    mailAddress: 'mailAddress',
    address: 'address',
    userDto: userDto
};

const mockData = [
    {
        hotelId: 1, hotelName: 'hotel1', hotelRoomsNumber: 1, phoneNumber: '1111111111', mailAddress: 'mailAddress1',
        address: 'address1', userDto: userDto
    },
    {
        hotelId: 2, hotelName: 'hotel2', hotelRoomsNumber: 2, phoneNumber: '2222222222', mailAddress: 'mailAddress2',
        address: 'address2', userDto: userDto
    },
    {
        hotelId: 3, hotelName: 'hotel3', hotelRoomsNumber: 3, phoneNumber: '3333333333', mailAddress: 'mailAddress3',
        address: 'address3', userDto: userDto
    }
] as Hotel[];

const flatCategoryDto = {
    flatCategoryId: 1,
    flatCategoryName: 'studio 3/4 personnes'
};

const mockDataFlats = [
    {
        flatId: 1, flatDescription: 'desc1', flatGuidance: 'guidance1', flatCapacity: 1, hotelDto: hotelDto,
        flatCategoryDto: flatCategoryDto
    },
    {
        flatId: 2, flatDescription: 'desc2', flatGuidance: 'guidance2', flatCapacity: 2, hotelDto: hotelDto,
        flatCategoryDto: flatCategoryDto
    },
    {
        flatId: 3, flatDescription: 'desc3', flatGuidance: 'guidance3', flatCapacity: 3, hotelDto: hotelDto,
        flatCategoryDto: flatCategoryDto
    }
] as Flat[];

describe('HotelService', () => {

    let service;
    let httpTestingController: HttpTestingController;
    let mockHotel: Hotel;
    let mockId: number;
    let mockHotels: Hotel[] = [];
    let mockFlats: Flat[] = [];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                HotelService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([HotelService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockHotels = mockData;
        mockHotel = hotelDto;
        mockId = mockHotel.hotelId;
        mockFlats = mockDataFlats;
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('insertHotel', () => {
        it('should return the inserted hotel', () => {
            service.insertHotel(mockHotel).subscribe(
                response => expect(response).toEqual(mockHotel), fail
            );

            const req = httpTestingController.expectOne(service.url + '/insertHotel');
            expect(req.request.method).toEqual('POST');
            req.flush(mockHotel);
        });
    });

    describe('getListHotelToDisplay', () => {
        it('should return a list of hotel', () => {
            service.getListHotelToDisplay().subscribe(
                response => expect(response).toEqual(mockHotels), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getListHotelToDisplay');
            expect(req.request.method).toEqual('GET');
            req.flush(mockHotels);
        });
    });

    describe('getHotel', () => {
        it('should return the hotel which has its identifier in parameters as an integer', () => {
            service.getHotel(mockId).subscribe(
                response => expect(response).toEqual(mockHotel), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getHotel?idHotel=' + mockId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockHotel);
        });
    });

    describe('updateHotel', () => {
        it('should return the updated hotel', () => {
            service.updateHotel(mockHotel).subscribe(
                response => expect(response).toEqual(mockHotel), fail
            );

            const req = httpTestingController.expectOne(service.url + '/updateHotel');
            expect(req.request.method).toEqual('POST');
            req.flush(mockHotel);
        });
    });

    describe('deleteHotelFlat', () => {
        it('should return the deleted flats which were in the hotel in parameters', () => {
            service.deleteHotelFlat(mockId).subscribe(
                response => expect(response).toEqual(mockFlats), fail
            );

            const req = httpTestingController.expectOne(service.url + '/deleteHotelFlat');
            expect(req.request.method).toEqual('POST');
            req.flush(mockFlats);
        });
    });

    describe('deleteHotel', () => {
        it('should return the deleted hotel', () => {
            service.deleteHotel(mockId).subscribe(
                response => expect(response).toEqual(mockHotel), fail
            );

            const req = httpTestingController.expectOne(service.url + '/deleteHotel');
            expect(req.request.method).toEqual('POST');
            req.flush(mockHotel);
        });
    });

});
