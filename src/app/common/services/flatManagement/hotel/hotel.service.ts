import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root"
})
export class HotelService {
  public url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url_server + "/api/Hotel";
  }

  insertHotel(hotel: Hotel): Observable<any> {
    return this.http.post(this.url + "/insertHotel", hotel);
  }

  getListHotelToDisplay(): Observable<any> {
    return this.http.get(this.url + "/getListHotelToDisplay");
  }

  getHotel(i: number): Observable<any> {
    return this.http.get<any>(this.url + "/getHotel?idHotel=" + i);
  }

  updateHotel(hotel: Hotel): Observable<any> {
    return this.http.post(this.url + "/updateHotel", hotel);
  }

  deleteHotelFlat(idHotel: number) {
    return this.http.post(this.url + "/deleteHotelFlat", idHotel);
  }

  deleteHotel(idHotel: number) {
    return this.http.post(this.url + "/deleteHotel", idHotel);
  }
}
