import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { NurseryPlanning } from "../../models/nursery/nurseryPlanning";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { NurseryPlanningSlot } from "../../models/nursery/nurseryPlanningSlot";

@Injectable({
  providedIn: "root"
})
export class NurseryPlanningSlotService {
  private http: HttpClient;
  private headers: HttpHeaders;
  constructor(private _http: HttpClient) {
    this.http = _http;
    this.headers = new HttpHeaders();
    this.headers.append("Accept", "application/json");
  }

  saveNurseryPlanning(planning: NurseryPlanningSlot) {
    return this.http.post(
      environment.url_server + "/api/nurseryPlanningSlot/",
      planning
    );
  }

  updateNurseryPlanning(planning: NurseryPlanningSlot): Observable<any> {
    return this.http.put(
      environment.url_server + "/api/nurseryPlanningSlot/",
      planning
    );
  }

  deleteNurseryPlanning(id: number) {
    let params = new HttpParams();
    params = params.append("id", id.toString());
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.delete(
      environment.url_server + "/api/nurseryPlanningSlot/",
      options
    );
  }
}
