import { TestBed, inject } from '@angular/core/testing';

import { NurseryPlanningSlotService } from './nursery-planning-slot.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('NurseryPlanningSlotService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        NurseryPlanningSlotService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([NurseryPlanningSlotService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
