import { TestBed, inject } from '@angular/core/testing';

import { NurseryService } from './nursery.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('NurseryService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        NurseryService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([NurseryService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
