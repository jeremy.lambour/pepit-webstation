import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class NurseryService {
  private http: HttpClient;
  private headers: HttpHeaders;
  constructor(private _http: HttpClient) {
    this.http = _http;
    this.headers = new HttpHeaders();
    this.headers.append("Accept", "application/json");
  }

  getAllNursery() {
    return this.http.get(environment.url_server + "/api/nursery/");
  }
  getNurseryById(id: string) {
    return this.http.get(environment.url_server + "/api/nursery/" + id);
  }
}
