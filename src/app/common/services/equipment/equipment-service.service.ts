import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Equipment } from '../../models/equipment/equipement';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EquipmentServiceService {
  public url: string;


  constructor(private http: HttpClient) {
    this.url = environment.url_server + '/api/Equipment';

  }

  insertEquipment(equipment: Equipment): Observable<any> {
    return this.http.post(this.url + '/insertEquipment', equipment);
  }
  getEquipment(i: number): Observable<any> {
    return this.http.get<any>(this.url + '/getEquipment?idEquipment=' + i);
  }

  updateEquipment(equipment: Equipment): Observable<any> {
    return this.http.post(this.url + '/updateEquipment', equipment);
  }

  deleteEquipment(idEquipment: number) {
    return this.http.post(this.url + '/deleteEquipment', idEquipment);
  }

  getListEquipmentToDisplay(): Observable<any> {
    return this.http.get(this.url + '/getListEquipmentToDisplay');
  }

  getListEquipmentByShop(idShop: number): Observable<any> {
    return this.http.get(this.url + '/getEquipmentByShopId?ShopId=' + idShop);
  }

}
