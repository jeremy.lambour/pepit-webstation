
import { TestBed, inject } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { EquipmentServiceService } from './equipment-service.service';
import { Equipment } from '../../models/equipment/equipement';

const authority = {
  authorityDtoId: 1,
  authorityDtoName: 'user'
};

const userDto = {
  userId: 1,
  username: 'BobD',
  password: '1234',
  firstName: 'Bob',
  lastName: 'Dylan',
  birthDate: new Date(),
  age: 35,
  phoneNumber: 1234567890,
  address: 'address',
  email: 'boby.dylan@gmail.com',
  city: 'Paris',
  postalCode: 95000,
  userRoleDto: authority
};

const shopDto = {
  shopId: 1,
  shopName: 'shop1',
  shopAddress: 'address1',
  phoneNumber: '0123456789',
  shopLatitude: 56,
  shopLongitude: 59,
  userDTO: userDto
};

const mockData = [
  {
    equipmentId: 1,
    equipmentName: 'equipment1',
    equipmentDescription: 'description1',
    equipmentState: 'state1',
    equipmentStock: 1,
    equipmentOut: 1,
    equipmentSize: 1,
    priceLocation: 1,
    shop: shopDto
  },
  {
    equipmentId: 2,
    equipmentName: 'equipment2',
    equipmentDescription: 'description2',
    equipmentState: 'state2',
    equipmentStock: 2,
    equipmentOut: 2,
    equipmentSize: 2,
    priceLocation: 2,
    shop: shopDto
  },
  {
    equipmentId: 3,
    equipmentName: 'equipment3',
    equipmentDescription: 'description3',
    equipmentState: 'state3',
    equipmentStock: 3,
    equipmentOut: 3,
    equipmentSize: 3,
    priceLocation: 3,
    shop: shopDto
  }
] as Equipment[];

describe('EquipmentServiceService', () => {

  let service;
  let httpTestingController: HttpTestingController;
  let mockEquipments: Equipment[] = [];
  let mockEquipment: Equipment;
  let mockId: number;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        EquipmentServiceService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([EquipmentServiceService], s => {
    service = s;
  }));

  beforeEach(() => {
    mockEquipments = mockData;
    mockEquipment = mockEquipments[0];
    mockId = mockEquipment.equipmentId;
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('insertEquipment', () => {
    it('should return the inserted equipment', () => {
      service.insertEquipment(mockEquipment).subscribe(
        response => expect(response).toEqual(mockEquipment), fail
      );

      const req = httpTestingController.expectOne(service.url + '/insertEquipment');
      expect(req.request.method).toEqual('POST');
      req.flush(mockEquipment);
    });
  });

  describe('getEquipment', () => {
    it('should return the equipment which has its identifier in parameters as an integer', () => {
      service.getEquipment(mockId).subscribe(
        response => expect(response).toEqual(mockEquipment), fail
      );

      const req = httpTestingController.expectOne(service.url + '/getEquipment?idEquipment=' + mockId);
      expect(req.request.method).toEqual('GET');
      req.flush(mockEquipment);
    });
  });

  describe('updateEquipment', () => {
    it('should return the updated equipment', () => {
      service.updateEquipment(mockEquipment).subscribe(
        response => expect(response).toEqual(mockEquipment), fail
      );

      const req = httpTestingController.expectOne(service.url + '/updateEquipment');
      expect(req.request.method).toEqual('POST');
      req.flush(mockEquipment);
    });
  });

  describe('deleteEquipment', () => {
    it('should return the deleted Equipment', () => {
      service.deleteEquipment(mockId).subscribe(
        response => expect(response).toEqual(mockEquipment), fail
      );

      const req = httpTestingController.expectOne(service.url + '/deleteEquipment');
      expect(req.request.method).toEqual('POST');
      req.flush(mockEquipment);
    });
  });

  describe('getListEquipmentToDisplay', () => {
    it('should return a list of equipment', () => {
      service.getListEquipmentToDisplay().subscribe(
        response => expect(response).toEqual(mockEquipments), fail
      );

      const req = httpTestingController.expectOne(service.url + '/getListEquipmentToDisplay');
      expect(req.request.method).toEqual('GET');
      req.flush(mockEquipments);
    });
  });

  describe('getListEquipmentByShop', () => {
    it('should return a list of flat', () => {
      service.getListEquipmentByShop(shopDto.shopId).subscribe(
        response => expect(response).toEqual(mockEquipments), fail
      );

      const req = httpTestingController.expectOne(service.url + '/getEquipmentByShopId?ShopId=' + shopDto.shopId);
      expect(req.request.method).toEqual('GET');
      req.flush(mockEquipments);
    });
  });

});
