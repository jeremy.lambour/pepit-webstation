import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { User } from '../../models/User/User';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  public http: HttpClient;
  public router: Router;
  private url: String;
  private observableConnect: BehaviorSubject<boolean>;

  constructor(
    private _http: HttpClient,
    private _router: Router,
    private location: Location
  ) {
    this.http = _http;
    this.router = _router;
    this.url = environment.url_server + '/api/user';
    this.observableConnect = new BehaviorSubject(false);
  }

  obtainAccessToken(username: string, password: string) {
    const params = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('grant_type', 'password')
      .set('client_id', environment.clienID);
    const headers = new HttpHeaders()
      .set('Content-type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set(
        'Authorization',
        'Basic ' + btoa(environment.clienID + ':' + environment.clientSecret)
      );
    const options = { headers: headers };

    return this.http.post(
      environment.url_server + '/oauth/token',
      params.toString(),
      options
    );
  }

  saveToken(token) {
    const expireDate = new Date().getTime() + 1000 * token.expires_in;
    Cookie.set('access_token', token.access_token, expireDate);
    this.location.back();
  }

  checkCredentials(): boolean {
    if (!Cookie.check('access_token')) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

  createUser() {
    this.router.navigate(['/createUser']);
  }

  findUser(username: String): Observable<any> {
    return this.http.get<any>(this.url + '/?login=' + username);
  }

  getUser() {
    return sessionStorage.getItem('principal');
  }

  setUser(user: User) {
    sessionStorage.setItem('principal', JSON.stringify(user));
    this.setIsConnect(true);
  }

  setIsConnect(bool: boolean) {
    this.observableConnect.next(bool);
  }

  getIsConnect() {
    let userCo: string;
    userCo = this.getUser();
    if (userCo !== null) {
      this.setIsConnect(true);
    }
    return this.observableConnect;
  }

  account() {
    this.router.navigate(['/account']);
  }

  logOut() {
    this.setIsConnect(false);
    Cookie.delete('access_token');
    sessionStorage.removeItem('principal');
    this.router.navigate(['/']);
  }
}
