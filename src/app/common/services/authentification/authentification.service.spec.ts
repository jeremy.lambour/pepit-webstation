import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthentificationService } from './authentification.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthentificationService', () => {

    let service;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([])
            ],
            providers: [
                AuthentificationService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([AuthentificationService], s => {
        service = s;
    }));

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

});
