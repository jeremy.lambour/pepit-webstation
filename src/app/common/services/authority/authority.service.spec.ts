import { TestBed, inject } from '@angular/core/testing';

import { AuthorityService } from './authority.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { Authority } from '../../models/User/authority';
import { Router } from '@angular/router';

const mockData = [
  {
    authorityDtoId: 1,
    authorityDtoName: 'user'
  },
  {
    authorityDtoId: 2,
    authorityDtoName: 'user2'
  },
  {
    authorityDtoId: 3,
    authorityDtoName: 'user3'
  }
] as Authority[];

describe('AuthorityService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        AuthorityService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([AuthorityService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getAllAuthority', () => {
    it('should return a list of authority', () => {
      service.getAllAuthority().subscribe(
        response => expect(response).toEqual(mockData), fail
      );

      const req = httpTestingController.expectOne('http://localhost:8080/ws-rest/api/authority/');
      expect(req.request.method).toEqual('GET');
      req.flush(mockData);
    });
  });

});
