import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class AuthorityService {
  private http: HttpClient;
  constructor(private _http: HttpClient) {
    this.http = _http;
  }

  getAllAuthority() {
    return this.http.get(environment.url_server + "/api/authority/");
  }
}
