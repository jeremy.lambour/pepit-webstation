import { FestivePeriod } from './../../models/common/FestivePeriod';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Season } from '../../models/common/Season';

@Injectable({
    providedIn: 'root'
})
export class FestivePeriodService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + '/api/FestivePeriod/';
    }

    getFestivePeriod(festivePeriodId: number): Observable<any> {
        return this.http.get(this.url + 'getFestivePeriod?festivePeriodId=' + festivePeriodId);
    }

    updateFestivePeriod(festivePeriod: FestivePeriod): Observable<any> {
        return this.http.post(this.url + 'updateFestivePeriod', festivePeriod);
    }

}

