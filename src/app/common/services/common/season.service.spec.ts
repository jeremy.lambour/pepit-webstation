import { HttpTestingController, HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed, inject } from '@angular/core/testing';
import { SeasonService } from './season.service';

describe('SeasonService', () => {

    let service;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                SeasonService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([SeasonService], s => {
        service = s;
    }));

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

});
