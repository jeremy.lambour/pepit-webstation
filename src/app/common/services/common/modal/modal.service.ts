import { Injectable, Output } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { EventEmitter } from "protractor";
import { Lesson } from "src/app/common/models/planning/lesson";

@Injectable({
  providedIn: "root"
})
export class ModalService {
  private modalService: NgbModal;
  private currentModal: NgbModalRef;

  constructor(private _modalService: NgbModal) {
    this.modalService = _modalService;
  }

  open(content) {
    this.currentModal = this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title"
    });
    return this.currentModal;
  }

  closeCurrent() {
    this.currentModal.close();
  }
}
