import { TestBed, inject } from '@angular/core/testing';

import { CommentEquipmentService } from './comment-equipment.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('CommentEquipmentService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CommentEquipmentService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([CommentEquipmentService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
