import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CommentEquipment } from 'src/app/common/models/common/comment/equipment/CommentEquipment';

@Injectable({
  providedIn: 'root',
})
export class CommentEquipmentService {
  public url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url_server + '/api/CommentEquipment';
  }

  insertCommentEquipment(commentEquipment: CommentEquipment): Observable<any> {
    return this.http.post(this.url + '/', commentEquipment);
  }

  getListCommentEquipment(idEquip: number): Observable<any> {
    return this.http.get(this.url + '/getCommentEquipment?idEquip=' + idEquip);
  }

  deleteCommentEquipment(commentEquipment: CommentEquipment): Observable<any> {
    return this.http.post(this.url + '/deleteCommentHotel', commentEquipment);
  }
}
