import { TestBed, inject } from '@angular/core/testing';
import { CommentHotelService } from './comment-hotel.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('CommentHotelService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CommentHotelService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([CommentHotelService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
