import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommentHotel } from 'src/app/common/models/common/comment/hotel/CommentHotel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentHotelService {

  public url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url_server + '/api/CommentHotel';
  }

  insertCommentHotel(commentHotel: CommentHotel): Observable<any> {
    return this.http.post(this.url + '/', commentHotel);
  }

  getListCommentHotel(idHotel: number): Observable<any> {
    return this.http.get(this.url + '/getCommentHotel?idHotel=' + idHotel);
  }

  deleteCommentHotel(commentHotel: CommentHotel): Observable<any> {
    return this.http.post(this.url + '/deleteCommentHotel', commentHotel);
  }
}
