import { inject, TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { AgeRangeService } from './ageRange.service';

describe('AgeRangeService', () => {

    let service;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                AgeRangeService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([AgeRangeService], s => {
        service = s;
    }));

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

});
