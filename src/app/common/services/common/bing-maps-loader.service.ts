import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


const url = 'http://www.bing.com/api/maps/mapcontrol?callback=__onBingLoaded&branch=release';

@Injectable({
  providedIn: 'root'
})
export class BingMapsLoaderService {
  private static promise;
  urlRoute: string;
  keyApi: string;

  constructor(private http: HttpClient) {
    this.keyApi = 'Amu8hyyQNoJ07Vb9LRKOQcNsGNyqqTbE0i_LIamCVYxVdzWW8tlwUf2K_Waavl6k';
  }

  load() {
    // First time 'load' is called?
    if (!BingMapsLoaderService.promise) {

      // Make promise to load
      BingMapsLoaderService.promise = new Promise(resolve => {

        // Set callback for when bing maps is loaded.
        window['__onBingLoaded'] = (ev) => {
          resolve('Bing Maps API loaded');
        };

        const node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        node.async = true;
        node.defer = true;
        document.getElementsByTagName('head')[0].appendChild(node);
      });
    }

    // Always return promise. When 'load' is called many times, the promise is already resolved.
    return BingMapsLoaderService.promise;
  }


  getRoute(city1: string, city2: string) {

    return this.http.get<any>('http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0='
      + city1 + '%2Cwa&wp.1=' + city2 + '%2Cwa&avoid=minimizeTolls&key=' + this.keyApi);
  }
}
