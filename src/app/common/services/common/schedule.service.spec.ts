import { TestBed, inject } from '@angular/core/testing';
import { ScheduleService } from './schedule.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { Schedule } from '../../models/common/Schedule';

const mockSchedule = {
    scheduleId: 1,
    scheduleDay: 'lundi',
    scheduleOpenHour: '8h',
    scheduleCloseHour: '20h'
};

describe('ScheduleService', () => {
    let service;
    let httpTestingController: HttpTestingController;
    let mockId: number;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                ScheduleService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([ScheduleService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockId = mockSchedule.scheduleId;
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('insertSchedule', () => {
        it('should return the inserted schedule', () => {
            service.insertSchedule(mockSchedule).subscribe(
                response => expect(response).toEqual(mockSchedule), fail
            );

            const req = httpTestingController.expectOne(service.url + 'insertSchedule');
            expect(req.request.method).toEqual('POST');
            req.flush(mockSchedule);
        });
    });

    describe('updateSchedule', () => {
        it('should return the updated schedule', () => {
            service.updateSchedule(mockSchedule).subscribe(
                response => expect(response).toEqual(mockSchedule), fail
            );

            const req = httpTestingController.expectOne(service.url + 'updateSchedule');
            expect(req.request.method).toEqual('POST');
            req.flush(mockSchedule);
        });
    });

    describe('deleteSchedule', () => {
        it('should return the deleted schedule', () => {
            service.deleteSchedule(mockId).subscribe(
                response => expect(response).toEqual(mockSchedule), fail
            );

            const req = httpTestingController.expectOne(service.url + '/deleteSchedule');
            expect(req.request.method).toEqual('POST');
            req.flush(mockSchedule);
        });
    });
});

