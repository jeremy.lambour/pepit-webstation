import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Season } from '../../models/common/Season';

@Injectable({
    providedIn: 'root'
})
export class SeasonService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + '/api/Season/';
    }

    updateSeason(season: Season): Observable<any> {
        return this.http.post(this.url + 'updateSeason', season);
    }

    getSeason(seasonId: number): Observable<any> {
        return this.http.get(this.url + 'getSeason?seasonId=' + seasonId);
    }

    updateDatesSeason(season: Season): Observable<any> {
        return this.http.post(this.url + 'updateDatesSeason', season);
    }

    getSeasonActive(): Observable<any> {
        return this.http.get(this.url + 'getActiveSeason');
    }

    getAllSeasons() {
        return this.http.get(environment.url_server + "/api/Season/");
    }

    getSeasonById(id: number) {
        return this.http.get(environment.url_server + "/api/Season/" + id);
    }

}

