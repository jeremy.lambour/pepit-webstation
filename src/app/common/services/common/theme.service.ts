import { Injectable } from '@angular/core';
import { Season } from '../../models/common/Season';
import { SeasonService } from './season.service';


export const WinterTheme = {
  'background-image': 'url("./assets/background/background.png")',
  'gradient-card': 'linear-gradient(to left bottom, #3256ae, #276ec1, #2285d1, #2b9ce0, #40b3ec)',
  'btn-color': '#007bff',
  'btn-background-color': 'linear-gradient(to bottom,#3256ae 0,#40b3ec 100%)',
  'btn-border-color': '#245580'
};

export const SummerTheme = {
  'background-image': 'url("./assets/background/summer.jpg")',
  'gradient-card': 'linear-gradient(to left bottom, #f12711, #f5af19)',
  'btn-color': '#f12711',
  'btn-background-color': 'linear-gradient(to bottom,#f12711 0,#f5af19 100%)',
  'btn-border-color': '#f12711'
};




@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(private seasonService: SeasonService) {

  }
  private listSeason: Season[] = [];

  toggleEstival() {
    this.setTheme(SummerTheme);

  }

  toggleWinter() {
    this.setTheme(WinterTheme);
  }

  private setTheme(theme: {}) {
    Object.keys(theme).forEach(k =>
      document.documentElement.style.setProperty(`--${k}`, theme[k])
    );
  }

  checkSeason() {
    this.seasonService.getSeasonActive().subscribe(value => {
      this.listSeason = value;
      for (const season of this.listSeason) {
        if (season.seasonId === 2) {
          console.log('la saison est hiver active');
          this.toggleWinter();
        } else {
          console.log('la saison est ete ');
          this.toggleEstival();
        }
      }
    });
  }
}

