import { TestBed, inject } from '@angular/core/testing';

import { BingMapsLoaderService } from './bing-maps-loader.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('BingMapsLoaderService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        BingMapsLoaderService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([BingMapsLoaderService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
