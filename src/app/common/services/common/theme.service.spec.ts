import { TestBed, inject } from '@angular/core/testing';

import { ThemeService } from './theme.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('ThemeService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ThemeService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([ThemeService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
