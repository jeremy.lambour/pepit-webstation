import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Schedule } from '../../models/common/Schedule';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ScheduleService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + '/api/Schedule/';
    }

    insertSchedule(schedule: Schedule): Observable<any> {
        return this.http.post(this.url + 'insertSchedule', schedule);
    }

    updateSchedule(schedule: Schedule): Observable<any> {
        return this.http.post(this.url + 'updateSchedule', schedule);
    }

    deleteSchedule(idSchedule: number) {
        return this.http.post(this.url + '/deleteSchedule', idSchedule);
    }

}

