import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = environment.url_server + '/api/Upload';
  }

  UploadImgShop(shop: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', shop);
    return this.http.post<any>(this.url + 'UploadImgShop', formData);
  }

  downloadImgShop(path: string): Observable<any> {
    return this.http.get<any>(this.url + '/getImgShop?pathImg=' + path);
  }
}

