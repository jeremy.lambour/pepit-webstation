import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { User } from "../../models/User/User";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private http: HttpClient;
  private headers: HttpHeaders;
  constructor(private _http: HttpClient) {
    this.http = _http;
    this.headers = new HttpHeaders();
    this.headers.append("Accept", "application/json");
  }

  getAllUsers: any = () => this.http.get(environment.url_server + "/api/user/all");

  getUserByAuthority: any = (userAuthorityName: string) => {
    let params = new HttpParams();
    params = params.append("authorityName", userAuthorityName);
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.get(
      environment.url_server + "/api/user/userAuthority",
      options
    );
  };

  getUserById = (id: string) => {
    let params = new HttpParams();
    params = params.append("userId", id);
    let options = {
      headers: this.headers,
      params: params
    };
    return this.http.get(environment.url_server + "/api/user/find", options);
  };
}
