import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';

import { RegisterService } from './register.service';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';

const authority = {
  authorityDtoId: 1,
  authorityDtoName: 'user'
};

const userDto = {
  userId: 1,
  username: 'BobD',
  password: '1234',
  firstName: 'Bob',
  lastName: 'Dylan',
  birthDate: new Date(),
  age: 35,
  phoneNumber: 1234567890,
  address: 'address',
  email: 'boby.dylan@gmail.com',
  city: 'Paris',
  postalCode: 95000,
  userRoleDto: authority
};

describe('RegisterService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        RegisterService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([RegisterService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('create', () => {
    it('should return the inserted user', () => {
      service.create(userDto).subscribe(
        response => expect(response).toEqual(userDto), fail
      );

      const req = httpTestingController.expectOne(environment.url_server + '/account/');
      expect(req.request.method).toEqual('POST');
      req.flush(userDto);
    });
  });

  describe('update', () => {
    it('should return the updated user', () => {
      service.updateAccount(userDto).subscribe(
        response => expect(response).toEqual(userDto), fail
      );

      const req = httpTestingController.expectOne(environment.url_server + '/account/update');
      expect(req.request.method).toEqual('POST');
      req.flush(userDto);
    });
  });

});
