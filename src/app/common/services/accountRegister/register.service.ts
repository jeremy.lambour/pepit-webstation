import { Injectable } from '@angular/core';
import { User } from '../../models/User/User';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  public router: Router;
  public http: HttpClient;

  constructor(private _http: HttpClient, private _router: Router) {
    this.router = _router;
    this.http = _http;
  }

  create(user: User): Observable<any> {
    return this.http.post(environment.url_server + '/account/', user);
  }

  updateAccount(user: User): Observable<any> {
    return this.http.post<any>(environment.url_server + '/account/update', user);
}

  goBack() {
    this.router.navigate(['']);
  }
}
