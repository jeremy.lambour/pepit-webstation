import { Injectable } from '@angular/core';

import * as socketIo from 'socket.io-client';
import { Message } from 'src/app/common/models/common/chat/message';
import { Event } from 'src/app/common/models/common/chat/event';
import { Observable, Observer } from 'rxjs';
import { User } from 'src/app/common/models/User/User';

const SERVER_URL = 'https://chat-server-pepit.herokuapp.com/';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  private socket;

  public initSocket(): void {
    this.socket = socketIo(SERVER_URL);
  }

  public sendState(state: String, user: User): void {
    this.socket.emit(state, user);
  }

  public send(message: Message): void {
    this.socket.emit('message', message);
  }

  public refresh(): void {
    this.socket.emit('refresh');
  }

  public logOut(): void {
    this.socket.emit('forceDisconnect');
  }

  public onMessage(): Observable<Message> {
    return new Observable<Message>(observer => {
      this.socket.on('message', (data: Message) => observer.next(data));
    });
  }

  public onCreateRoom(): Observable<Message> {
    return new Observable<Message>(observer => {
      this.socket.on('getRoom', (data: Message) => observer.next(data));
    });
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<Event>(observer => {
      this.socket.on(event, () => observer.next());
    });
  }
}
