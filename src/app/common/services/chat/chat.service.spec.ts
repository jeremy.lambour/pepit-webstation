import { TestBed, inject } from '@angular/core/testing';

import { ChatService } from './chat.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('ChatService', () => {

  let service;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ChatService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([ChatService], s => {
    service = s;
  }));

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
