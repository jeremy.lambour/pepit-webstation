import { Injectable } from '@angular/core';

import * as socketIo from 'socket.io-client';
import { Message } from '../../models/common/chat/message';
import { Observable, Observer } from 'rxjs';
import { User } from '../../models/User/User';
import { ChatModule } from 'src/app/core/shared/chat/chat.module';

const SERVER_URL = 'https://chat-server-pepit.herokuapp.com:15289';

@Injectable({
  providedIn: ChatModule
})
export class ChatService {
  private socket;

  public initSocket(): void {
    this.socket = socketIo(SERVER_URL);
  }

  public send(message: Message): void {
    this.socket.emit('message', message);
  }

  public onCreate(): Observable<User> {
    return new Observable<User>(observer => {
      this.socket.on('createRoom', (data: User) => observer.next(data));
    });
  }

  public onAwaiting(): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.socket.on('awaiting', (data: boolean) => observer.next(data));
    });
  }

  public onMessage(): Observable<Message> {
    return new Observable<Message>(observer => {
      this.socket.on('message', (data: Message) => observer.next(data));
    });
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<Event>(observer => {
      this.socket.on(event, () => observer.next());
    });
  }
}
