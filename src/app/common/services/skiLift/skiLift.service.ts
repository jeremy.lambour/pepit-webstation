import { SkiLift } from './../../models/equipment/skiLift';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class SkiLiftService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + '/api/SkiLift';
    }

    findSkiLift(ageRangeId: string, domain: string, passTypeId: string): Observable<any> {
        if (passTypeId === '1') {
            return this.http.get<any>(this.url + '/findSkiLift/' + ageRangeId + '/' + domain + '/' + passTypeId);
        } else {
            return this.http.get<any>(this.url + '/findSkiLiftNordique/' + ageRangeId + '/' + passTypeId);
        }
    }

    updateSkiLift(skiLiftHalfADay: SkiLift): Observable<any> {
        return this.http.post(this.url + '/updateSkiLift', skiLiftHalfADay);
    }

}
