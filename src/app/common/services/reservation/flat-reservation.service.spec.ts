import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FlatReservationService } from './flat-reservation.service';
import { RFlat } from '../../models/reservation/rFlat';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const hotelDto = {
    hotelId: 1,
    hotelName: 'hotelName',
    hotelRoomsNumber: 3,
    phoneNumber: 'phoneNumber',
    mailAddress: 'mailAddress',
    address: 'address',
    userDto: userDto
};

const flatCategoryDto = {
    flatCategoryId: 1,
    flatCategoryName: 'studio 3/4 personnes'
};

const flatDto = {
    flatId: 1, flatDescription: 'desc1', flatGuidance: 'guidance1', flatCapacity: 1, hotelDto: hotelDto,
    flatCategoryDto: flatCategoryDto
};

const reservationDto = {
    reservationId: 1,
    nbPersons: 1,
    insuranceLossMaterial: 1,
    houseCancelOpt: 1,
    equipmentCancelOpt: 1,
    reservCancelopt: 1,
    user: userDto
};

const mockData = [
    {
        flatDto: flatDto,
        reservationDto: reservationDto,
        startDate: new Date(),
        endDate: new Date(),
        price: 107
    },
    {
        flatDto: flatDto,
        reservationDto: reservationDto,
        startDate: new Date(),
        endDate: new Date(),
        price: 108
    },
    {
        flatDto: flatDto,
        reservationDto: reservationDto,
        startDate: new Date(),
        endDate: new Date(),
        price: 109
    }
] as RFlat[];

describe('FlatReservationService', () => {

    let service;
    let httpTestingController: HttpTestingController;
    let mockRFlats: RFlat[] = [];
    let mockRFlat: RFlat;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FlatReservationService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([FlatReservationService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockRFlats = mockData;
        mockRFlat = mockRFlats[0];
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('getAvailabilityFlat', () => {
        it('should return the potential flat that can be available for the user\'s dates', () => {
            service.getAvailabilityFlat(new Date(), new Date(), mockRFlat.flatDto).subscribe(
                response => expect(response).toEqual(mockRFlat.flatDto), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getAvailabilityFlat/' + new Date() +
                '/' + new Date() + '/' + mockRFlat.flatDto.flatId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockRFlat.flatDto);
        });
    });

    describe('insertReservationFlat', () => {
        it('should return the inserted flat\'s reservation', () => {
            service.insertReservationFlat(mockRFlat).subscribe(
                response => expect(response).toEqual(mockRFlat), fail
            );

            const req = httpTestingController.expectOne(service.url + '/insertReservationFlat');
            expect(req.request.method).toEqual('POST');
            req.flush(mockRFlat);
        });
    });

    describe('getPriceFlatForDates', () => {
        it('should return the flat\'s price for the user\'s dates', () => {
            service.getPriceFlatForDates('1', '2019-06-15', '2019-06-20').subscribe(
                response => expect(response).toEqual(15.20), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getPriceFlatForDates/' + '1' + '/'
                + '2019-06-15' + '/' + '2019-06-20');
            expect(req.request.method).toEqual('GET');
            req.flush(15.20);
        });
    });

    describe('getReservationsFlatUser', () => {
        it('should return a list of flat\'s reservations for a specific customer', () => {
            service.getReservationsFlatUser(userDto).subscribe(
                response => expect(response).toEqual(mockRFlats), fail
            );

            const req = httpTestingController.expectOne(service.url + '/getReservationsFlatUser/' + userDto.userId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockRFlats);
        });
    });

});
