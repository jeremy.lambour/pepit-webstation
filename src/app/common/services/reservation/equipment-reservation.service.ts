
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { REquipment } from '../../models/reservation/rEquipment';
import { User } from '../../models/User/User';

@Injectable({
  providedIn: 'root'
})
export class EquipmentReservationService {
  private url: String;
  public http: HttpClient;

  constructor(private _http: HttpClient) {
    this.http = _http;
    this.url = environment.url_server;
  }

  create(rEquip: REquipment): Observable<any> {
    return this.http.post(this.url + '/api/reservEquipment/', rEquip);
  }

  getListReserv(idUser: number): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/getReservationEquipmentUser?idUser=' +
      idUser
    );
  }

  getListeMateriel(dateStart: Date): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/getListeMateriel?dateStart=' +
      dateStart
    );
  }



  checkAvailability(rEquip: REquipment): Observable<any> {
    return this.http.post(
      this.url + '/api/reservEquipment/checkAvailibity',
      rEquip
    );
  }

  deleteReservationEquipment(rEquip: REquipment): Observable<any> {
    return this.http.post(
      this.url + '/api/reservEquipment/deleteReservation',
      rEquip
    );
  }
  getListReservByDay(dateStart: string): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/getListReservByDay?dateStart=' +
      dateStart
    );
  }

  getClientsByShopManager(user: User): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/getClientsByShopManager?idUser=' +
      user.userId
    );
  }

  getReservationEquipmentUserDateAfterNow(idUser: number): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/getReservationEquipmentUserDateAfterNow?idUser=' +
      idUser
    );
  }

  findReservEquipmentByIdReserv(idReserv): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/findReservEquipmentByIdReserv?idReserv=' +
      idReserv
    );
  }

  getReservationEquipmentUserToTakeBack(idUser: number): Observable<any> {
    return this.http.get(
      this.url +
      '/api/reservEquipment/getReservationEquipmentUserToTakeBack?idUser=' +
      idUser
    );
  }

  updateIsGiven(isGiven: Boolean, idEquipment: number, idReserv: number): Observable<any> {
    return this.http.get(
      this.url + "/api/reservEquipment/updateIsGiven/" + isGiven + "/" + idEquipment + "/" + idReserv
    );
  }
}

