import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { RSkiLift } from '../../models/reservation/rSkiLift';

@Injectable({
  providedIn: "root"
})
export class SkiLiftReservationService {
  private url: string;
  public http: HttpClient;
  public router: Router;

  constructor(private _http: HttpClient, private _router: Router) {
    this.http = _http;
    this.router = _router;
    this.url = environment.url_server + "/api/reservSkiLift/";
  }

  create(rskiLift: RSkiLift): Observable<any> {
    return this.http.post(this.url, rskiLift);
  }

  getLiftUser(idUser: number): Observable<any> {
    return this.http.get(this.url + "getReservLift?idUser=" + idUser);
  }

  deleteReservLift(rskiLift: RSkiLift): Observable<any> {
    return this.http.post(this.url + "deleteReservLift", rskiLift);
  }

  getAll(): Observable<any> {
    return this.http.get(this.url + "all");
  }

  getAgeRange(idAge: number): Observable<any> {
    return this.http.get(this.url + "ageRange?idAge=" + idAge);
  }

  goHome() {
    this.router.navigate(["/home"]);
  }
}
