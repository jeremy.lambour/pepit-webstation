import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SkiLiftReservationService } from './skilift-reservation.service';
import { RSkiLift } from '../../models/reservation/rSkiLift';
import { RouterTestingModule } from '@angular/router/testing';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const passTypeDto = {
    passTypeId: 1,
    passTypeName: '1'
};

const ageRangeDto = {
    ageRangeId: 1,
    ageRangeName: '1'
};

const skiLiftDto = {
    skiLiftId: 1,
    duration: 'duration1',
    domain: 'domain1',
    price: 1,
    passType: passTypeDto,
    ageRange: ageRangeDto
};

const listSkiLiftDto = [
    {
        skiLiftId: 1,
        duration: 'duration1',
        domain: 'domain1',
        price: 1,
        passType: passTypeDto,
        ageRange: ageRangeDto
    }
];

const mockData = [
    {
        skiLiftDto: skiLiftDto,
        userDto: userDto,
        nameReserv: 'reservation1',
        dateStart: new Date(),
        dateEnd: new Date(),
        caution: 1,
        insuranceSnow: 1,
        price: 1
    },
    {
        skiLiftDto: skiLiftDto,
        userDto: userDto,
        nameReserv: 'reservation2',
        dateStart: new Date(),
        dateEnd: new Date(),
        caution: 2,
        insuranceSnow: 2,
        price: 2
    },
    {
        skiLiftDto: skiLiftDto,
        userDto: userDto,
        nameReserv: 'reservation3',
        dateStart: new Date(),
        dateEnd: new Date(),
        caution: 3,
        insuranceSnow: 3,
        price: 3
    }
] as RSkiLift[];

describe('SkiLiftReservationService', () => {

    let service;
    let httpTestingController: HttpTestingController;
    let mockRSkiLifts: RSkiLift[] = [];
    let mockRSkiLift: RSkiLift;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([])
            ],
            providers: [
                SkiLiftReservationService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([SkiLiftReservationService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockRSkiLifts = mockData;
        mockRSkiLift = mockRSkiLifts[0];
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('create', () => {
        it('should return the inserted skilift\'s reservation', () => {
            service.create(mockRSkiLift).subscribe(
                response => expect(response).toEqual(mockRSkiLift), fail
            );

            const req = httpTestingController.expectOne(service.url);
            expect(req.request.method).toEqual('POST');
            req.flush(mockRSkiLift);
        });
    });

    describe('getLiftUser', () => {
        it('should return a list of skiLift for a specific user', () => {
            service.getLiftUser(userDto.userId).subscribe(
                response => expect(response).toEqual(mockRSkiLifts), fail
            );

            const req = httpTestingController.expectOne(service.url + 'getReservLift?idUser=' + userDto.userId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockRSkiLifts);
        });
    });

    describe('deleteReservLift', () => {
        it('should return the deleted skilift\'s reservation', () => {
            service.deleteReservLift(mockRSkiLift).subscribe(
                response => expect(response).toEqual(1), fail
            );

            const req = httpTestingController.expectOne(service.url + 'deleteReservLift');
            expect(req.request.method).toEqual('POST');
            req.flush(1);
        });
    });

    /*describe('getAll', () => {
        it('should return a list of skilft\'s reservation', () => {
            service.getAll().subscribe(
                response => expect(response).toEqual(listSkiLiftDto), fail
            );

            const req = httpTestingController.expectOne(service.url);
            expect(req.request.method).toEqual('GET');
            req.flush(listSkiLiftDto);
        });
    });*/

    describe('getAgeRange', () => {
        it('should return an age range', () => {
            service.getAgeRange(ageRangeDto.ageRangeId).subscribe(
                response => expect(response).toEqual(ageRangeDto), fail
            );

            const req = httpTestingController.expectOne(service.url + 'ageRange?idAge=' + ageRangeDto.ageRangeId);
            expect(req.request.method).toEqual('GET');
            req.flush(ageRangeDto);
        });
    });

});
