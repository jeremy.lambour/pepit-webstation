import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { REquipment } from '../../models/reservation/rEquipment';
import { EquipmentReservationService } from './equipment-reservation.service';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const shopDto = {
    shopId: 1,
    shopName: 'shop1',
    shopAddress: 'address1',
    phoneNumber: '0123456789',
    shopLatitude: 56,
    shopLongitude: 59,
    userDTO: userDto
};

const equipmentDto = {
    equipmentId: 1,
    equipmentName: 'equipment1',
    equipmentDescription: 'description1',
    equipmentState: 'state1',
    equipmentStock: 1,
    equipmentOut: 1,
    equipmentSize: 1,
    priceLocation: 1,
    shop: shopDto
};

const reservationDto = {
    reservationId: 1,
    nbPersons: 1,
    insuranceLossMaterial: 1,
    houseCancelOpt: 1,
    equipmentCancelOpt: 1,
    reservCancelopt: 1,
    user: userDto
};

const mockData = [
    {
        equipmentDto: equipmentDto,
        reservationDto: reservationDto,
        dateStart: new Date(),
        dateEnd: new Date(),
        quantity: 1,
        price: 1,
        isGiven: true
    },
    {
        equipmentDto: equipmentDto,
        reservationDto: reservationDto,
        dateStart: new Date(),
        dateEnd: new Date(),
        quantity: 2,
        price: 2,
        isGiven: false
    },
    {
        equipmentDto: equipmentDto,
        reservationDto: reservationDto,
        dateStart: new Date(),
        dateEnd: new Date(),
        quantity: 3,
        price: 3
    }
] as REquipment[];

describe('EquipmentReservationService', () => {

    let service;
    let httpTestingController: HttpTestingController;
    let mockREquipments: REquipment[] = [];
    let mockREquipment: REquipment;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                EquipmentReservationService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([EquipmentReservationService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockREquipments = mockData;
        mockREquipment = mockREquipments[0];
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('create a reservation for an equipment', () => {
        it('should return the inserted reservation', () => {
            service.create(mockREquipment).subscribe(
                response => expect(response).toEqual(mockREquipment), fail
            );

            const req = httpTestingController.expectOne(service.url + '/api/reservEquipment/');
            expect(req.request.method).toEqual('POST');
            req.flush(mockREquipment);
        });
    });

    describe('getListReserv get the list of equipment\'s reservations of a user', () => {
        it('should return a list of equipment\'s reservation', () => {
            service.getListReserv(userDto.userId).subscribe(
                response => expect(response).toEqual(mockREquipments), fail
            );

            const req = httpTestingController.expectOne(service.url + '/api/reservEquipment/getReservationEquipmentUser?idUser=' +
                userDto.userId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockREquipments);
        });
    });

    describe('checkAvailability', () => {
        it('should return the availibity for an equipment', () => {
            service.checkAvailability(mockREquipment).subscribe(
                response => expect(response).toEqual(1), fail
            );

            const req = httpTestingController.expectOne(service.url + '/api/reservEquipment/checkAvailibity');
            expect(req.request.method).toEqual('POST');
            req.flush(1);
        });
    });

    describe('deleteReservationEquipment', () => {
        it('should return the deleted equipment\'s reservation', () => {
            service.deleteReservationEquipment(mockREquipment).subscribe(
                response => expect(response).toEqual(1), fail
            );

            const req = httpTestingController.expectOne(service.url + '/api/reservEquipment/deleteReservation');
            expect(req.request.method).toEqual('POST');
            req.flush(1);
        });
    });

});
