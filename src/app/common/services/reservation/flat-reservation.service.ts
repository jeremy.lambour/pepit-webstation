import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Flat } from '../../models/flatManagement/Flat';
import { RFlat } from '../../models/reservation/rFlat';
import { User } from '../../models/User/User';

@Injectable({
  providedIn: "root"
})
export class FlatReservationService {
  url: String;
  public http: HttpClient;

  constructor(private _http: HttpClient) {
    this.http = _http;
    this.url = environment.url_server + "/api/ReservationFlat";
  }

  getAvailabilityFlat(
    arrivingDate: Date,
    leavingDate: Date,
    flat: Flat
  ): Observable<any> {
    const flatId = flat.flatId;
    return this.http.get(
      this.url +
      "/getAvailabilityFlat/" +
      arrivingDate +
      "/" +
      leavingDate +
      "/" +
      flatId
    );
  }

  insertReservationFlat(rFlat: RFlat): Observable<any> {
    return this.http.post(this.url + "/insertReservationFlat", rFlat);
  }

  getPriceFlatForDates(
    id: string,
    startDate: string,
    endDate: string
  ): Observable<any> {
    return this.http.get(
      this.url + "/getPriceFlatForDates/" + id + "/" + startDate + "/" + endDate
    );
  }

  getReservationsFlatUser(user: User): Observable<any> {
    const userId = user.userId;
    return this.http.get(this.url + "/getReservationsFlatUser/" + userId);
  }

  getAllReservedFlat(): Observable<any> {
    return this.http.get(this.url + "/");
  }
}
