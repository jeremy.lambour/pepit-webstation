import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Shop } from '../../models/shop/Shop';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShopServiceService {
  public url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url_server + '/api/Shop';
  }

  insertShop(shop: Shop): Observable<any> {
    return this.http.post(this.url + '/insertShop', shop);
  }

  getShop(i: number): Observable<any> {
    return this.http.get<any>(this.url + '/getShop?idShop=' + i);
  }

  updateShop(shop: Shop): Observable<any> {
    return this.http.post(this.url + '/updateShop', shop);
  }

  deleteShop(idShop: number) {
    return this.http.post(this.url + '/deleteShop', idShop);
  }

  getListShopToDisplay(): Observable<any> {
    return this.http.get(this.url + '/getListShopToDisplay');
  }

}
