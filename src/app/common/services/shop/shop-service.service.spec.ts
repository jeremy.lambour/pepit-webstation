
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ShopServiceService } from './shop-service.service';
import { Shop } from '../../models/shop/Shop';

const authority = {
  authorityDtoId: 1,
  authorityDtoName: 'user'
};

const userDto = {
  userId: 1,
  username: 'BobD',
  password: '1234',
  firstName: 'Bob',
  lastName: 'Dylan',
  birthDate: new Date(),
  age: 35,
  phoneNumber: 1234567890,
  address: 'address',
  email: 'boby.dylan@gmail.com',
  city: 'Paris',
  postalCode: 95000,
  userRoleDto: authority
};

const mockData = [
  {
    shopId: 1, shopName: 'name1', shopAddress: 'address1', phoneNumber: '0123456789', shopLatitude: 1,
    shopLongitude: 1, userDTO: userDto
  },
  {
    shopId: 2, shopName: 'name2', shopAddress: 'address2', phoneNumber: '0123456789', shopLatitude: 2,
    shopLongitude: 2, userDTO: userDto
  },
  {
    shopId: 3, shopName: 'name3', shopAddress: 'address3', phoneNumber: '0123456789', shopLatitude: 3,
    shopLongitude: 3, userDTO: userDto
  }
] as Shop[];

describe('ShopServiceService', () => {

  let service;
  let httpTestingController: HttpTestingController;
  let mockShops: Shop[] = [];
  let mockShop: Shop;
  let mockId: number;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ShopServiceService
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject([ShopServiceService], s => {
    service = s;
  }));

  beforeEach(() => {
    mockShops = mockData;
    mockShop = mockShops[0];
    mockId = mockShop.shopId;
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('insertShop', () => {
    it('should return the inserted shop', () => {
      service.insertShop(mockShop).subscribe(
        response => expect(response).toEqual(mockShop), fail
      );

      const req = httpTestingController.expectOne(service.url + '/insertShop');
      expect(req.request.method).toEqual('POST');
      req.flush(mockShop);
    });
  });

  describe('getShop', () => {
    it('should return the shop which has its identifier in parameters as an integer', () => {
      service.getShop(mockId).subscribe(
        response => expect(response).toEqual(mockShop), fail
      );

      const req = httpTestingController.expectOne(service.url + '/getShop?idShop=' + mockId);
      expect(req.request.method).toEqual('GET');
      req.flush(mockShop);
    });
  });

  describe('updateShop', () => {
    it('should return the updated shop', () => {
      service.updateShop(mockShop).subscribe(
        response => expect(response).toEqual(mockShop), fail
      );

      const req = httpTestingController.expectOne(service.url + '/updateShop');
      expect(req.request.method).toEqual('POST');
      req.flush(mockShop);
    });
  });

  describe('deleteShop', () => {
    it('should return the deleted shop', () => {
      service.deleteShop(mockId).subscribe(
        response => expect(response).toEqual(mockShop), fail
      );

      const req = httpTestingController.expectOne(service.url + '/deleteShop');
      expect(req.request.method).toEqual('POST');
      req.flush(mockShop);
    });
  });

  describe('getListShopToDisplay', () => {
    it('should return a list of shop', () => {
      service.getListShopToDisplay().subscribe(
        response => expect(response).toEqual(mockShops), fail
      );

      const req = httpTestingController.expectOne(service.url + '/getListShopToDisplay');
      expect(req.request.method).toEqual('GET');
      req.flush(mockShops);
    });
  });

});