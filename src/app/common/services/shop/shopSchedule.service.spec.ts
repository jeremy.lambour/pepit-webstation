
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ShopSchedule } from '../../models/shop/ShopSchedule';
import { ShopScheduleService } from './shopSchedule.service';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const mockShopDto = {
    shopId: 1, shopName: 'name1', shopAddress: 'address1', phoneNumber: '0123456789', shopLatitude: 1,
    shopLongitude: 1, userDTO: userDto
};

const mockSchedule = {
    scheduleId: 1,
    scheduleDay: 'lundi',
    scheduleOpenHour: '8h',
    scheduleCloseHour: '20h'
};


const mockData = [
    {
        shopDto: mockShopDto, scheduleDto: mockSchedule
    },
    {
        shopDto: mockShopDto, scheduleDto: mockSchedule
    },
    {
        shopDto: mockShopDto, scheduleDto: mockSchedule
    }
] as ShopSchedule[];

describe('ShopScheduleService', () => {

    let service;
    let httpTestingController: HttpTestingController;
    let mockShopSchedules: ShopSchedule[] = [];
    let mockShopSchedule: ShopSchedule;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                ShopScheduleService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });

    beforeEach(inject([ShopScheduleService], s => {
        service = s;
    }));

    beforeEach(() => {
        mockShopSchedules = mockData;
        mockShopSchedule = mockShopSchedules[0];
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    /*describe('insertShopSchedule', () => {
        it('should return the inserted shopSchedule', () => {
            service.insertShopSchedule(mockShopSchedule).subscribe(
                response => expect(response).toEqual(mockShopSchedule), fail
            );

            const req = httpTestingController.expectOne(service.url + '/insertShopSchedule/'
                + mockSchedule.scheduleId + '/' + mockShopDto.shopId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockShopSchedule);
        });
    });*/

    describe('getShopSchedule', () => {
        it('should return the shop schedule which has its shop identifier in parameters as an integer', () => {
            service.getShopSchedule(mockShopDto.shopId).subscribe(
                response => expect(response).toEqual(mockShopSchedules), fail
            );

            const req = httpTestingController.expectOne(service.url + 'getShopSchedule?idShop=' + mockShopDto.shopId);
            expect(req.request.method).toEqual('GET');
            req.flush(mockShopSchedules);
        });
    });

    describe('deleteShopSchedule', () => {
        it('should return the deleted shop schedule', () => {
            service.deleteShopSchedule(mockShopDto.shopId).subscribe(
                response => expect(response).toEqual(mockShopSchedule), fail
            );

            const req = httpTestingController.expectOne(service.url + '/deleteShopschedule');
            expect(req.request.method).toEqual('POST');
            req.flush(mockShopSchedule);
        });
    });

});

