import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root'
})
export class ShopScheduleService {
    public url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url_server + '/api/ShopSchedule/';
    }

    insertShopSchedule(scheduleMondayId: number, shopId: number): Observable<any> {
        return this.http.get(this.url + '/insertShopSchedule/' + scheduleMondayId + '/' + shopId);
    }

    getShopSchedule(id): Observable<any> {
        return this.http.get<any>(this.url + 'getShopSchedule?idShop=' + id);
    }

    deleteShopSchedule(idShop: number) {
        return this.http.post(this.url + '/deleteShopschedule', idShop);
    }

}


