import { Hotel } from './Hotel';
import { FlatCategory } from './FlatCategory';
export class Flat {
  public flatId: number;
  public flatDescription: string;
  public flatGuidance: string;
  public flatCapacity: number;
  public hotelDto: Hotel;
  public flatCategoryDto: FlatCategory;
}
