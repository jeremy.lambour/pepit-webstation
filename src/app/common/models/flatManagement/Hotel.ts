import { User } from '../User/User';

export class Hotel {
  public hotelId: number;
  public hotelName: string;
  public hotelRoomsNumber: number;
  public phoneNumber: string;
  public mailAddress: string;
  public address: string;
  public userDto: User;
}
