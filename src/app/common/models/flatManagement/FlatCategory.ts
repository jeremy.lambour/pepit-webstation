import { Hotel } from './Hotel';
export class FlatCategory {
    public flatCategoryId: number;
    public flatCategoryName: string;
}
