import { Flat } from './Flat';
export class FlatAvailibility {
  public flatAvailibilityId: number;
  public dateAvailibility: Date;
  public availibility: boolean;
  public flatDto: Flat;
}
