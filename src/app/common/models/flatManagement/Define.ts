import { Flat } from './Flat';
import { FestivePeriod } from '../common/FestivePeriod';

// Class between Flat and FestivePeriod. We can get the price of a flat for a period
export class Define {
    public flatDto: Flat;
    public festivePeriodDto: FestivePeriod;
    public flatPricePerNight: number;
}
