import { Equipment } from '../equipment/equipement';
import { Reservation } from './reservation';

export class REquipment {
    equipmentDto: Equipment;
    reservationDto: Reservation;
    dateStart: Date;
    dateEnd: Date;
    quantity: number;
    price: number;
    isGiven: Boolean;
}
