import { Flat } from '../flatManagement/Flat';
import { Reservation } from './reservation';

export class RFlat {
  flatDto: Flat;
  reservationDto: Reservation;
  startDate: Date;
  endDate: Date;
  price: number;
}
