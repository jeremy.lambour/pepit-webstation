import { User } from 'src/app/common/models/User/User';
import { SkiLift } from './../equipment/skiLift';

export class RSkiLift {
    skiLiftDto: SkiLift;
    userDto: User;
    nameReserv: string;
    dateStart: Date;
    dateEnd: Date;
    caution: number;
    insuranceSnow: number;
    price: number;
}
