import { User } from '../User/User';

export class Reservation {
    reservationId: number;
    nbPersons: number;
    insuranceLossMaterial: number;
    houseCancelOpt: number;
    equipmentCancelOpt: number;
    reservCancelopt: number;
    user: User;
}
