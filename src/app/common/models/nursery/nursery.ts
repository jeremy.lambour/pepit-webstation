export class Nursery {
  public nurseryId: number;
  public nurseryName: string;
  public nurseryPhoneNumber: string;
}
