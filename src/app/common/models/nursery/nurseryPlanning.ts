import { Nursery } from "./nursery";
import { Season } from "../common/Season";
import { NurseryPlanningSlot } from "./nurseryPlanningSlot";

export class NurseryPlanning {
  nurseryPlanningId: number;
  nurseryPlanningName: String;
  nursery: Nursery;
  season: Season;
  nurseryPlanningBeginning: Date;
  nurseryPlanningEnd: Date;
  nurseryPlanningSlots: Array<NurseryPlanningSlot>;
}
