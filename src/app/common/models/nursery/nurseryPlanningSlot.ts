import { NurseryPlanning } from "./nurseryPlanning";

export class NurseryPlanningSlot {
  nurseryPlanningSlotId: number;
  nurseryPlanning: number;
  date: Date;
  duration: number;
  startTime: Date;
  endTime: Date;
  nurserySlotNumberPlaces: number;
}
