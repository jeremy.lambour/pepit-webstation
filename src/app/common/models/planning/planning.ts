import { User } from "../User/User";
import { Lesson } from "./lesson";
import { Season } from "../common/Season";

export class Planning {
  planningId: number;
  planningCreator: User;
  planningMonitor: User;
  planningName: string;
  planningBeginning: Date;
  planningEnd: Date;
  lessons: Lesson[];
  planningSeason: Season;
}
