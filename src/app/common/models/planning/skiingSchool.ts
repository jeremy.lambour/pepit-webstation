export class SkiingSchool {
  skiingSchoolId: number;
  skiingSchoolName: string;
  phoneNumber: string;
}
