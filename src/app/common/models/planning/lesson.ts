import { User } from "../User/User";
import { SkiingSchool } from "./skiingSchool";
import { Planning } from "./planning";

export class Lesson {
  lessonId: number;
  lessonMonitor: User;
  lessonSkiingSchool: SkiingSchool;
  lessonLevel: string;
  lessonSport: string;
  lessonClub: string;
  lessonDate: Date;
  lessonStartTime: Date;
  lessonEndTime: Date;
  lessonDuration: number;
  lessonMeetingPlace: string;
  lessonPlanning: number;
  lessonNumberPlaces: number;
}
