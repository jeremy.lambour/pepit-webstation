import { Shop } from './Shop';
import { Schedule } from '../common/Schedule';

export class ShopSchedule {
    public shopDto: Shop;
    public scheduleDto: Schedule;
}
