import { User } from '../User/User';

export class Shop {
    public shopId: number;
    public shopName: string;
    public shopAddress: string;
    public phoneNumber: string;
    public shopLatitude: Number;
    public shopLongitude: Number;
    public userDTO: User;
}
