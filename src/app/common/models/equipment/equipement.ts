import { Shop } from '../shop/Shop';

export class Equipment {
    equipmentId: number;
    equipmentName: string;
    equipmentDescription: string;
    equipmentState: string;
    equipmentStock: number;
    equipmentOut: number;
    equipmentSize: number;
    priceLocation: number;
    shop: Shop;
}
