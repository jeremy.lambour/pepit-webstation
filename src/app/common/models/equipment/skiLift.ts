import { PassType } from '../equipmentType/passType';
import { AgeRange } from '../User/ageRange';

export class SkiLift {
  skiLiftId: number;
  duration: string;
  domain: string;
  price: number;
  passType: PassType;
  ageRange: AgeRange;
}
