import { Season } from './Season';

export class FestivePeriod {
    public festivePeriodId: number;
    public festivePeriodName: String;
    public festivePeriodBeginning: Date;
    public festivePeriodEnd: Date;
    public seasonDto: Season;
}
