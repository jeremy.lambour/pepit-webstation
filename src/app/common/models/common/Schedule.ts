export class Schedule {
    public scheduleId: number;
    public scheduleDay: string;
    public scheduleOpenHour: string;
    public scheduleCloseHour: string;
}
