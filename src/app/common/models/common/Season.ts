export class Season {
  public seasonId: number;
  public seasonName: String;
  public seasonBeginning: Date;
  public seasonEnd: Date;
  public active: Boolean;
}
