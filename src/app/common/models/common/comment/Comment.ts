import { User } from '../../User/User';

export class Comment {
  public commentId: number;
  public content: String;
  public from: User;
  public note: number;
}
