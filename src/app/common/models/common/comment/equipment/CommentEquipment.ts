import { Equipment } from '../../../equipment/equipement';
import { Comment } from '../Comment';

export class CommentEquipment {
    public commentDto: Comment;
    public equipmentDto: Equipment;
}