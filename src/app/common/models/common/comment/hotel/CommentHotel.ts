import { Comment } from '../Comment';
import { Hotel } from '../../../flatManagement/Hotel';

export class CommentHotel {
    public commentDto: Comment;
    public hotelDto: Hotel;
}
