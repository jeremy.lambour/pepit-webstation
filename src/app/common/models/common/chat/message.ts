import { Action } from './action';
import { User } from '../../User/User';

export interface Message {
  from?: User;
  content?: any;
  roomName?: string;
  action?: Action;
}
