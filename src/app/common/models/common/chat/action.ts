export enum Action {
  JOINED,
  AWAITING,
  LEFT,
  RENAME,
}
