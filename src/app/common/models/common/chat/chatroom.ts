import { User } from '../../User/User';
import { Action } from './action';

export interface ChatRoom {
  moderator?: User;
  user?: User;
  action?: Action;
}
