import { Authority } from './authority';

export class User {
  userId: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  age: number;
  phoneNumber: number;
  address: string;
  email: string;
  city: string;
  postalCode: number;
  userRoleDto: Authority;

}
