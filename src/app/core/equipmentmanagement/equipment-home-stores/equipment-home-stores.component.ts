import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';
import { User } from 'src/app/common/models/User/User';

@Component({
    selector: 'app-home-stores',
    templateUrl: './equipment-home-stores.component.html',
    styleUrls: ['./equipment-home-stores.component.css']
})
export class EquipmentHomeStoresComponent implements OnInit {

    public loadingEquipmentHomeStores: boolean;
    public listEquipment: Equipment[] = [];
    public idShop: number;
    private user: User;
    private roleUser: string;

    constructor(private equipmentService: EquipmentServiceService,
        private route: ActivatedRoute,
        private authentificationService: AuthentificationService) { }

    ngOnInit() {
        this.user = JSON.parse(this.authentificationService.getUser());
        this.roleUser = this.user.userRoleDto.authorityDtoName;
        this.route.paramMap.subscribe(params => {
            this.idShop = +params.get('ShopId');
            this.getListEquipmentByShop();
        });
        this.loadingEquipmentHomeStores = false;
    }

    getListEquipmentByShop(): void {
        this.equipmentService.getListEquipmentByShop(this.idShop).subscribe(
            res => {
                this.listEquipment = res;
                this.loadingEquipmentHomeStores = true;
            }
        );
    }

}
