import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { Shop } from 'src/app/common/models/shop/Shop';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';





@Component({
    selector: 'app-equipment-creation',
    templateUrl: './equipment-creation.component.html',
    styleUrls: ['./equipment-creation.component.css'],
    providers: [EquipmentServiceService]
})

export class EquipmentCreationComponent implements OnInit {


    private static adjustedWidth: number;

    private equipmentForm: FormGroup;
    private submit: Boolean;
    private loading: Boolean;
    private loadingShop: boolean;
    private idShop: number;
    private equipment: Equipment;

    public urlCompo = EquipmentCreationComponent;

    private shopDto: Shop;

    public router: Router;

    constructor(public fb: FormBuilder, private equipmentService: EquipmentServiceService,
        private ref: ChangeDetectorRef, private _router: Router, private shopService: ShopServiceService,
        private route: ActivatedRoute, ) {
        this.router = _router;
    }

    ngOnInit() {
        this.loadingShop = false;
        this.loading = false;
        this.initForm();
        this.route.paramMap.subscribe(params => {
            this.idShop = +params.get('ShopId');
            this.getShop(this.idShop);
        });
        EquipmentCreationComponent.adjustedWidth = 0;

    }

    initForm() {
        this.equipmentForm = new FormGroup({
            equipmentName: new FormControl('', Validators.required),
            equipmentStock: new FormControl('', Validators.required),
            equipmentState: new FormControl('', Validators.required),
            equipmentSize: new FormControl('', Validators.required),
            equipmentPrice: new FormControl('', Validators.required),
            equipmentDescription: new FormControl('', Validators.required)
        });
    }

    getShop(i: number): void {
        this.shopService.getShop(i).subscribe(res => {
            this.shopDto = res;
            this.loadingShop = true;
        });
    }

    onSubmit() {
        this.equipment = {
            equipmentId: null,
            equipmentName: this.equipmentForm.get('equipmentName').value,
            equipmentDescription: this.equipmentForm.get('equipmentDescription').value,
            equipmentState: this.equipmentForm.get('equipmentState').value,
            equipmentStock: this.equipmentForm.get('equipmentStock').value,
            equipmentOut: 0,
            equipmentSize: this.equipmentForm.get('equipmentSize').value,
            priceLocation: this.equipmentForm.get('equipmentPrice').value,
            shop: this.shopDto
        };


        this.equipmentService.insertEquipment(this.equipment).subscribe(res => {
            this.equipment = res;
            this.router.navigate(['/homestores']);
        });
    }
}
