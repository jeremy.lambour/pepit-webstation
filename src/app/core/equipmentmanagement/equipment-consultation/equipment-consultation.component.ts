import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { CommentEquipmentService } from 'src/app/common/services/common/comment/equipment/comment-equipment.service';
import { CommentEquipment } from 'src/app/common/models/common/comment/equipment/CommentEquipment';
import { Comment } from 'src/app/common/models/common/comment/Comment';

@Component({
  selector: 'app-equipment-consultation',
  templateUrl: './equipment-consultation.component.html',
  styleUrls: ['./equipment-consultation.component.css'],
  providers: [EquipmentServiceService],
})
export class EquipmentConsultationComponent implements OnInit {
  public loadingConsultEquipment: Boolean;
  public idEquipment: number;
  public equipment: Equipment;
  public listComment: Comment[] = [];
  public listCommentEquip: CommentEquipment[] = [];
  public loadingComment: boolean;
  private comment: Comment;
  private commentEquip: CommentEquipment;

  constructor(
    private equipmentService: EquipmentServiceService,
    private route: ActivatedRoute,
    private commentEquipmentService: CommentEquipmentService
  ) { }

  ngOnInit() {
    this.loadingComment = false;
    this.route.paramMap.subscribe(params => {
      this.idEquipment = +params.get('EquipmentId');
      this.getEquipment(this.idEquipment);
    });
    this.loadingConsultEquipment = false;
  }

  getEquipment(id: number): void {
    this.equipmentService.getEquipment(id).subscribe(res => {
      this.equipment = res;
      this.loadingConsultEquipment = true;
      this.getComment();
    });
  }

  getComment() {
    this.commentEquipmentService.getListCommentEquipment(this.equipment.equipmentId).subscribe(response => {
      console.log(response);
      this.listCommentEquip = response;
      for (const c of this.listCommentEquip) {
        this.comment = {
          commentId: c.commentDto.commentId,
          content: c.commentDto.content,
          from: c.commentDto.from,
          note: c.commentDto.note
        };
        this.listComment.push(this.comment);
      }
      this.loadingComment = true;
    });
  }

  sendCommentEquip($event) {

    this.commentEquip = {
      commentDto: $event,
      equipmentDto: this.equipment
    };
    console.log(this.commentEquip);
    this.commentEquipmentService
      .insertCommentEquipment(this.commentEquip)
      .subscribe(response => {
        console.log('commentInseré');
      });
  }
}
