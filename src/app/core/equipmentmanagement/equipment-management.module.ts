import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EquipmentCreationComponent } from './equipment-creation/equipment-creation.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EquipmentEditionComponent } from './equipment-edition/equipment-edition.component';
import { EquipmentHomeStoresComponent } from './equipment-home-stores/equipment-home-stores.component';
import { EquipmentConsultationComponent } from './equipment-consultation/equipment-consultation.component';
import { SharedModule } from '../shared/shared.module';
import { CommentModule } from '../common/comment/comment.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    CommentModule,
  ],
  declarations: [
    EquipmentCreationComponent,
    EquipmentEditionComponent,
    EquipmentHomeStoresComponent,
    EquipmentConsultationComponent,

  ],
})
export class EquipmentManagementModule { }
