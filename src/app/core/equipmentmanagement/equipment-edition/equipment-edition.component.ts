import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { injectComponentFactoryResolver } from '@angular/core/src/render3';
import { ActivatedRoute, Router } from '@angular/router';
import { Shop } from 'src/app/common/models/shop/Shop';
import { Path } from 'src/app/common/models/common/Path';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';


@Component({
    selector: 'app-equipment-edition',
    templateUrl: './equipment-edition.component.html',
    styleUrls: ['./equipment-edition.component.css']
})
export class EquipmentEditionComponent implements OnInit {

    private static adjustedWidth: number;
    private static loadingPhoto: boolean;



    private equipmentForm: FormGroup;
    private submit: Boolean;
    private loading: Boolean;


    public equipment: Equipment;


    public urlCompo = EquipmentEditionComponent;

    private idEquipment: number;
    private loadingEquipment: Boolean;

    public router: Router;

    constructor(public fb: FormBuilder, private equipmentService: EquipmentServiceService, private ref: ChangeDetectorRef
        , private _router: Router, private route: ActivatedRoute) {
        this.router = _router;
        this.equipmentForm = fb.group({
            equipmentName: ['', Validators.required],
            equipmentDescription: ['', Validators.required],
            equipmentState: ['', Validators.required],
            equipmentStock: ['', Validators.required],
            equipmentOut: ['', Validators.required],
            equipmentSize: ['', Validators.required],
            equipmentPrice: ['', Validators.required]
        });
    }


    ngOnInit() {

        EquipmentEditionComponent.adjustedWidth = 0;
        this.route.paramMap.subscribe(params => {
            this.idEquipment = +params.get('EquipmentId');
            this.getEquipment(this.idEquipment);
        });
        this.loadingEquipment = false;

    }

    getEquipment(i: number): void {
        this.equipmentService.getEquipment(i)
            .subscribe(
                res => {
                    this.equipment = res;
                    this.loadingEquipment = true;
                }
            );
    }

    submitRegistration(value: Object): void {
        console.log(value);
        this.equipment.equipmentName = this.equipmentForm.get('equipmentName').value;
        this.equipment.equipmentDescription = this.equipmentForm.get('equipmentDescription').value;
        this.equipment.equipmentState = this.equipmentForm.get('equipmentState').value;
        this.equipment.equipmentStock = this.equipmentForm.get('equipmentStock').value;
        this.equipment.equipmentOut = this.equipmentForm.get('equipmentOut').value;
        this.equipment.equipmentSize = this.equipmentForm.get('equipmentSize').value;
        this.equipment.priceLocation = this.equipmentForm.get('equipmentPrice').value;

        this.equipmentService.updateEquipment(this.equipment).subscribe(res => {
            this.equipment = res;
            this.router.navigate(['/equipmenthomestores/' + this.equipment.shop.shopId]);
        });
    }



    deleteEquipment() {
        this.equipmentService.deleteEquipment(this.idEquipment).subscribe();
        this.router.navigate(['/equipmenthomestores/' + this.equipment.shop.shopId]);
    }
}
