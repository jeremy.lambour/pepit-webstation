import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { EquipmentEditionComponent } from './equipment-edition.component';

describe('EquipmentEditionComponent', () => {
    let component: EquipmentEditionComponent;
    let fixture: ComponentFixture<EquipmentEditionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EquipmentEditionComponent],
            providers: [EquipmentServiceService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EquipmentEditionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
