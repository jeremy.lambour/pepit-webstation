import { Component, OnInit } from "@angular/core";
import { PlanningService } from "src/app/common/services/planning/planning.service";
import { Planning } from "src/app/common/models/planning/planning";
import { Router, ActivatedRoute } from "@angular/router";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { Season } from "src/app/common/models/common/Season";

@Component({
  selector: "app-planning-home",
  templateUrl: "./planning-home.component.html",
  styleUrls: ["./planning-home.component.css"]
})
export class PlanningHomeComponent implements OnInit {
  private planningService: PlanningService;
  public plannings: Array<Planning>;
  private router: Router;
  private modalService: ModalService;
  private seasonId: string;
  private idMonitor: string;

  constructor(
    private _planningService: PlanningService,
    private _router: Router,
    private _modalService: ModalService,
    private _activatedRoute: ActivatedRoute
  ) {
    this.planningService = _planningService;
    this.router = _router;
    this.modalService = _modalService;
  }

  ngOnInit() {
    this.idMonitor = this._activatedRoute.snapshot.queryParamMap.get(
      "monitorId"
    );
    this.seasonId = this._activatedRoute.snapshot.queryParamMap.get("seasonId");
    this.planningService
      .getAllPlanningByMonitorIdAndSeasonId(this.idMonitor, this.seasonId)
      .subscribe(result => {
        this.plannings = result as Array<Planning>;
      });
  }

  toUpdatePlanning(planningId: number) {
    this.router.navigate(
      ["/planningEdition/:planningId/:seasonId/:monitorId"],
      {
        queryParams: {
          planningId: planningId,
          seasonId: this.seasonId,
          monitorId: this.idMonitor
        }
      }
    );
  }

  showPlanning(planningId: number) {
    this.router.navigate(["/planningView/:planningId"], {
      queryParams: {
        planningId: planningId
      }
    });
  }

  toCreatePlanning() {
    this.router.navigate(["/planningCreation/:seasonId/:monitorId"], {
      queryParams: {
        seasonId: this.seasonId,
        monitorId: this.idMonitor
      }
    });
  }

  deletePlanning(planning: Planning) {
    this.planningService
      .deletePlanning(planning.planningId)
      .subscribe(result => {
        this.plannings.splice(this.plannings.indexOf(planning), 1);
        this.modalService.closeCurrent();
      });
  }

  open(id) {
    this.modalService.open(id);
  }
}
