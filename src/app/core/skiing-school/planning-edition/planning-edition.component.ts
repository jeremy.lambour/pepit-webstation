import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "src/app/common/services/user/user.service";
import { User } from "src/app/common/models/User/User";
import { PlanningService } from "src/app/common/services/planning/planning.service";
import { Planning } from "src/app/common/models/planning/planning";
import { AuthentificationService } from "src/app/common/services/authentification/authentification.service";
import { NgbDate, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";
import { Season } from "src/app/common/models/common/Season";
import { SeasonService } from "src/app/common/services/common/season.service";

@Component({
  selector: "app-planning-edition",
  templateUrl: "./planning-edition.component.html",
  styleUrls: ["./planning-edition.component.css"]
})
export class PlanningEditionComponent implements OnInit {
  public planningForm: FormGroup;
  private userService: UserService;
  private authentificationService: AuthentificationService;
  private planningService: PlanningService;
  private router: Router;
  private editedPlanning: Planning;
  private currentSeason: Season;
  private seasonService: SeasonService;
  public beginDate: NgbDateStruct;
  public endDate: NgbDateStruct;
  private monitor: User;
  constructor(
    private builder: FormBuilder,
    private _userService: UserService,
    private _authenficationService: AuthentificationService,
    private _planningService: PlanningService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    public _dateAdapter: NgbStringAdapter,
    private _seasonService: SeasonService
  ) {
    this.planningForm = builder.group({
      planningName: ["", Validators.required],
      planningMonitor: ["", Validators.required],
      planningDateStart: ["", Validators.required],
      planningDateEnd: ["", Validators.required]
    });
    this.router = _router;
    this.userService = _userService;
    this.authentificationService = _authenficationService;
    this.planningService = _planningService;
    this.editedPlanning = new Planning();
    this.seasonService = _seasonService;
  }

  submitUpdatePlanning() {
    if (this.isEditedDifferentFromInitialPlanning()) {
      this.editedPlanning.planningCreator = JSON.parse(
        this.authentificationService.getUser()
      ) as User;
      this.editedPlanning.planningName = this.planningForm.get(
        "planningName"
      ).value;
      this.editedPlanning.planningBeginning = new Date(
        this.planningForm.get("planningDateStart").value
      );
      this.editedPlanning.planningEnd = new Date(
        this.planningForm.get("planningDateEnd").value
      );
      this.planningService
        .updatePlanning(this.editedPlanning)
        .subscribe(result => {
          this.router.navigate(["/monitor/:monitorId/:seasonId"], {
            queryParams: {
              monitorId: this.editedPlanning.planningMonitor.userId,
              seasonId: this.currentSeason.seasonId
            }
          });
        });
    }
  }

  ngOnInit() {
    let id = this._activatedRoute.snapshot.queryParamMap.get("planningId");
    let seasonId = this._activatedRoute.snapshot.queryParamMap.get("seasonId");
    let monitorId = this._activatedRoute.snapshot.queryParamMap.get(
      "monitorId"
    );
    this.userService.getUserById(monitorId).subscribe(result => {
      this.monitor = result as User;
      this.planningForm.get("planningMonitor").setValue(this.monitor.username);
    });
    this.planningService.getPlanningById(parseInt(id)).subscribe(result => {
      this.editedPlanning = result as Planning;
      let dateStart = new Date(this.editedPlanning.planningBeginning);
      let dateEnd = new Date(this.editedPlanning.planningEnd);
      this.planningForm.setValue({
        planningName: this.editedPlanning.planningName,
        planningMonitor: this.editedPlanning.planningMonitor.username,
        planningDateStart: new Date(dateStart),
        planningDateEnd: new Date(dateEnd)
      });
    });
    this.seasonService.getSeasonById(parseInt(seasonId)).subscribe(result => {
      this.currentSeason = result as Season;
      this.beginDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonBeginning)
      );
      this.endDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonEnd)
      );
    });
  }

  isEditedDifferentFromInitialPlanning() {
    let isDifferent = false;
    let dateStart = new Date(this.editedPlanning.planningBeginning);
    let dateEnd = new Date(this.editedPlanning.planningEnd);
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningMonitor").value !=
        this.editedPlanning.planningMonitor.username;
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningName").value !=
        this.editedPlanning.planningName;
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningDateStart").value.getTime() !=
        dateStart.getTime();
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningDateEnd").value.getTime() !=
        dateEnd.getTime();
    return isDifferent;
  }
}
