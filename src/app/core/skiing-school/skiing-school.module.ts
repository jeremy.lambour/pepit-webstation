import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NgbModule, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ContextMenuModule } from "ngx-contextmenu";
import { SharedModule } from "./../shared/shared.module";
import { PlanningHomeComponent } from "./planning-home/planning-home.component";
import { PlanningCreationComponent } from "./planning-creation/planning-creation.component";
import { PlanningEditionComponent } from "./planning-edition/planning-edition.component";
import { PlanningViewComponent } from "./planning-view/planning-view.component";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { PlanningLessonCreationModalComponent } from "./planning-view/planning-lesson-creation-modal/planning-lesson-creation-modal.component";
import { PlanningLessonEditionModalComponent } from "./planning-view/planning-lesson-edition-modal/planning-lesson-edition-modal.component";
import { MonitorViewComponent } from "./monitor-view/monitor-view.component";
import { MonitorCardComponent } from "./monitor-view/monitor-card/monitor-card.component";

@NgModule({
  declarations: [
    PlanningHomeComponent,
    PlanningCreationComponent,
    PlanningEditionComponent,
    PlanningViewComponent,
    PlanningLessonCreationModalComponent,
    PlanningLessonEditionModalComponent,
    MonitorViewComponent,
    MonitorCardComponent
  ],
  entryComponents: [
    PlanningLessonCreationModalComponent,
    PlanningLessonEditionModalComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    SharedModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    })
  ],
  providers: []
})
export class SkiingSchoolModule {}
