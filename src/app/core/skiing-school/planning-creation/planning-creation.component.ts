import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "src/app/common/services/user/user.service";
import { User } from "src/app/common/models/User/User";
import { PlanningService } from "src/app/common/services/planning/planning.service";
import { Planning } from "src/app/common/models/planning/planning";
import { AuthentificationService } from "src/app/common/services/authentification/authentification.service";
import { NgbDate, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute } from "@angular/router";
import { isDifferent } from "@angular/core/src/render3/util";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";
import { Season } from "src/app/common/models/common/Season";
import { SeasonService } from "src/app/common/services/common/season.service";

@Component({
  selector: "app-planning-creation",
  templateUrl: "./planning-creation.component.html",
  styleUrls: ["./planning-creation.component.css"]
})
export class PlanningCreationComponent implements OnInit {
  public planningForm: FormGroup;
  private userService: UserService;
  private authentificationService: AuthentificationService;
  private planningService: PlanningService;
  private createdPlanning: Planning;
  private router: Router;
  public beginDate: NgbDateStruct;
  public endDate: NgbDateStruct;
  private currentSeason: Season;
  private seasonService: SeasonService;
  private monitor: User;
  constructor(
    private builder: FormBuilder,
    private _userService: UserService,
    private _authenficationService: AuthentificationService,
    private _planningService: PlanningService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    public _dateAdapter: NgbStringAdapter,
    private _seasonService: SeasonService
  ) {
    this.planningForm = builder.group({
      planningName: ["", Validators.required],
      planningMonitor: ["", Validators.required],
      planningDateStart: ["", Validators.required],
      planningDateEnd: ["", Validators.required]
    });
    this.router = _router;
    this.userService = _userService;
    this.authentificationService = _authenficationService;
    this.planningService = _planningService;
    this.createdPlanning = new Planning();
    this.seasonService = this._seasonService;
  }

  submitPlanning() {
    this.createdPlanning.planningCreator = JSON.parse(
      this.authentificationService.getUser()
    ) as User;
    this.createdPlanning.planningName = this.planningForm.get(
      "planningName"
    ).value;
    this.createdPlanning.planningBeginning = new Date(
      this.planningForm.get("planningDateStart").value
    );
    this.createdPlanning.planningEnd = new Date(
      this.planningForm.get("planningDateEnd").value
    );
    this.createdPlanning.planningMonitor = this.monitor;
    this.createdPlanning.planningSeason = this.currentSeason;
    this.planningService
      .createPlanning(this.createdPlanning)
      .subscribe(result => {
        this.router.navigate(["/monitor/:monitorId/:seasonId"], {
          queryParams: {
            monitorId: this.createdPlanning.planningMonitor.userId,
            seasonId: this.currentSeason.seasonId
          }
        });
      });
  }

  ngOnInit() {
    let seasonId = this._activatedRoute.snapshot.queryParamMap.get("seasonId");
    let monitorId = this._activatedRoute.snapshot.queryParamMap.get(
      "monitorId"
    );
    this.userService.getUserById(monitorId).subscribe(result => {
      this.monitor = result as User;
      this.planningForm.get("planningMonitor").setValue(this.monitor.username);
    });
    this.seasonService.getSeasonById(parseInt(seasonId)).subscribe(result => {
      this.currentSeason = result as Season;
      this.beginDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonBeginning)
      );
      this.endDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonEnd)
      );
    });
  }
}
