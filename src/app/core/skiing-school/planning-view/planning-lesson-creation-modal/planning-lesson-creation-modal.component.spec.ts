import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlanningLessonCreationModalComponent } from "./planning-lesson-creation-modal.component";

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PlanningLessonCreationModalComponent', () => {
  let component: PlanningLessonCreationModalComponent;
  let fixture: ComponentFixture<PlanningLessonCreationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningLessonCreationModalComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule,
        NgbModule.forRoot(), BrowserModule, BrowserDynamicTestingModule],
      providers: [NgbModal, NgbActiveModal, DatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningLessonCreationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
