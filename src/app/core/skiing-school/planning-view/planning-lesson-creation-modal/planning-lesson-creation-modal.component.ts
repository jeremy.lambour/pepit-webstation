import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormControl
} from "@angular/forms";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { Lesson } from "src/app/common/models/planning/lesson";
import { NgbActiveModal, NgbDateAdapter } from "@ng-bootstrap/ng-bootstrap";
import { SkiingSchool } from "src/app/common/models/planning/skiingSchool";
import { PlanningService } from "src/app/common/services/planning/planning.service";
import { Planning } from "src/app/common/models/planning/planning";
import { LessonService } from "src/app/common/services/lesson/lesson.service";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";

@Component({
  selector: "app-planning-lesson-creation-modal",
  templateUrl: "./planning-lesson-creation-modal.component.html",
  styleUrls: ["./planning-lesson-creation-modal.component.css"]
})
export class PlanningLessonCreationModalComponent implements OnInit {
  @Input() beginDate: Date;
  @Input() planning: Planning;
  @Input() skiingSchools: Array<SkiingSchool>;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  public lessonCreationForm: FormGroup;
  public lessonStartTime: any;
  public lessonEndTime: any;
  private planningService: PlanningService;
  private lessonService: LessonService;

  constructor(
    private _builder: FormBuilder,
    private _modalService: ModalService,
    public activeModal: NgbActiveModal,
    private _planningService: PlanningService,
    private _lessonService: LessonService,
    public _dateFormatter: NgbStringAdapter
  ) {
    this.lessonCreationForm = _builder.group({
      lessonLevel: ["", Validators.required],
      lessonSkiingSchool: ["", Validators.required],
      lessonSport: ["", Validators.required],
      lessonClub: ["", Validators.required],
      lessonDate: ["", Validators.required],
      lessonStartTime: ["", Validators.required],
      lessonEndTime: ["", Validators.required],
      lessonMeetingPlace: ["", Validators.required],
      lessonNumberPlaces: ["", Validators.required]
    });
    this.lessonCreationForm
      .get("lessonEndTime")
      .setValidators([
        Validators.required,
        this.isAfterStartTime(
          this.lessonCreationForm.controls["lessonStartTime"].value
        )
      ]);
    this.planningService = _planningService;
    this.lessonService = _lessonService;
  }

  submitLesson() {
    let lesson = new Lesson();
    console.log(this.lessonCreationForm);
    lesson.lessonLevel = this.lessonCreationForm.controls["lessonLevel"].value;
    lesson.lessonClub = this.lessonCreationForm.controls["lessonClub"].value;
    lesson.lessonSport = this.lessonCreationForm.controls["lessonSport"].value;
    lesson.lessonDate = this.lessonCreationForm.controls["lessonDate"].value;
    lesson.lessonDuration =
      parseInt(this.lessonCreationForm.controls["lessonEndTime"].value) -
      parseInt(this.lessonCreationForm.controls["lessonStartTime"].value);
    let startTime = this.lessonCreationForm.controls[
      "lessonStartTime"
    ].value.split(":");
    lesson.lessonStartTime = new Date(lesson.lessonDate.getTime());
    lesson.lessonStartTime.setHours(startTime[0], startTime[1]);
    let endTime = this.lessonCreationForm.controls["lessonEndTime"].value.split(
      ":"
    );
    lesson.lessonEndTime = new Date(lesson.lessonDate.getTime());
    lesson.lessonEndTime.setHours(endTime[0], endTime[1]);
    lesson.lessonMeetingPlace = this.lessonCreationForm.controls[
      "lessonMeetingPlace"
    ].value;
    lesson.lessonSkiingSchool = this.skiingSchools
      .filter(
        school =>
          this.lessonCreationForm.controls["lessonSkiingSchool"].value ==
          school.skiingSchoolId.toString()
      )
      .shift();
    lesson.lessonMonitor = this.planning.planningMonitor;
    lesson.lessonPlanning = this.planning.planningId;
    lesson.lessonNumberPlaces = this.lessonCreationForm.controls[
      "lessonNumberPlaces"
    ].value;
    this.lessonService.saveLesson(lesson).subscribe(result => {
      this.activeModal.close(result);
    });
  }

  ngOnInit() {
    console.log(this.beginDate);
    this.lessonCreationForm.controls["lessonDate"].setValue(this.beginDate);
  }

  dismiss() {
    console.log(this.lessonCreationForm);
    this._modalService.closeCurrent();
  }
  isAfterStartTime(time: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = new Date(time) > new Date(control.value);
      return forbidden ? { forbiddenTime: { value: control.value } } : null;
    };
  }
}
