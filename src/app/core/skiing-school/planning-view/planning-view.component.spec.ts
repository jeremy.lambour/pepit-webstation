import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningViewComponent } from './planning-view.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Lesson } from 'src/app/common/models/planning/lesson';
import { SkiingSchool } from 'src/app/common/models/planning/skiingSchool';
import { Planning } from 'src/app/common/models/planning/planning';
import { CalendarEvent } from 'calendar-utils';
import { ChildActivationEnd } from '@angular/router';

describe('PlanningViewComponent', () => {
  let component: PlanningViewComponent;
  let fixture: ComponentFixture<PlanningViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningViewComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule,
        NgbModule.forRoot(), BrowserModule, BrowserDynamicTestingModule],
      providers: [NgbModal, NgbActiveModal, DatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {

    expect(component).toBeTruthy();
  });*/

});