import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit
} from "@angular/core";
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  endOfWeek,
  startOfWeek,
  addWeeks,
  addMonths,
  subWeeks,
  subMonths,
  startOfMonth
} from "date-fns";
import { Subject } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarMonthViewDay
} from "angular-calendar";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { Action } from "rxjs/internal/scheduler/Action";
import { Planning } from "src/app/common/models/planning/planning";
import { PlanningService } from "src/app/common/services/planning/planning.service";
import { ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { PlanningLessonCreationModalComponent } from "./planning-lesson-creation-modal/planning-lesson-creation-modal.component";
import { Lesson } from "src/app/common/models/planning/lesson";
import { PlanningLessonEditionModalComponent } from "./planning-lesson-edition-modal/planning-lesson-edition-modal.component";
import { SkiingSchool } from "src/app/common/models/planning/skiingSchool";
import { SkiingSchoolService } from "src/app/common/services/skiing-school/skiing-school.service";
import { LessonService } from "src/app/common/services/lesson/lesson.service";

const colors: any = {
  red: {
    primary: "#ad2121",
    secondary: "#FAE3E3"
  },
  blue: {
    primary: "#1e90ff",
    secondary: "#D1E8FF"
  },
  yellow: {
    primary: "#e3bc08",
    secondary: "#FDF1BA"
  }
};

@Component({
  selector: "app-planning-view",
  templateUrl: "./planning-view.component.html",
  styleUrls: ["./planning-view.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanningViewComponent implements OnInit {
  @ViewChild("modalLesson") modalLesson: TemplateRef<any>;

  public view: CalendarView = CalendarView.Month;

  public CalendarView = CalendarView;

  public viewDate: Date;

  public currentPlanning: Planning;

  private planningService: PlanningService;
  private skiingSchoolService: SkiingSchoolService;
  private modalService: ModalService;
  private createLesson: FormGroup;
  private skiingSchools: Array<SkiingSchool>;
  private minDate: Date;
  private maxDate: Date;
  public prevBtnDisabled: Boolean;
  public nextBtnDisabled: Boolean;

  refresh: Subject<any> = new Subject();

  skiingLessons: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  constructor(
    private _modalService: ModalService,
    private _planningService: PlanningService,
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _skiingSchoolService: SkiingSchoolService
  ) {
    this.planningService = _planningService;
    this.modalService = _modalService;
    this.skiingSchoolService = _skiingSchoolService;
  }
  addPeriod(period: CalendarView, date: Date, amount: number): Date {
    return {
      day: addDays,
      week: addWeeks,
      month: addMonths
    }[period](date, amount);
  }

  subPeriod(period: CalendarView, date: Date, amount: number): Date {
    return {
      day: subDays,
      week: subWeeks,
      month: subMonths
    }[period](date, amount);
  }

  startOfPeriod(period: CalendarView, date: Date): Date {
    return {
      day: startOfDay,
      week: startOfWeek,
      month: startOfMonth
    }[period](date);
  }

  endOfPeriod(period: CalendarView, date: Date): Date {
    return {
      day: endOfDay,
      week: endOfWeek,
      month: endOfMonth
    }[period](date);
  }

  previous(): void {
    this.changeDate(this.subPeriod(this.view, this.viewDate, 1));
  }

  next(): void {
    this.changeDate(this.addPeriod(this.view, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  changeDate(date: Date): void {
    this.viewDate = date;
    this.dateOrViewChanged();
  }
  changeView(view: CalendarView): void {
    this.view = view;
    this.dateOrViewChanged();
  }

  dateOrViewChanged(): void {
    this.prevBtnDisabled = !this.dateIsValid(
      this.endOfPeriod(this.view, this.subPeriod(this.view, this.viewDate, 1))
    );
    this.nextBtnDisabled = !this.dateIsValid(
      this.startOfPeriod(this.view, this.addPeriod(this.view, this.viewDate, 1))
    );
    if (this.viewDate < this.minDate) {
      this.changeDate(this.minDate);
    } else if (this.viewDate > this.maxDate) {
      this.changeDate(this.maxDate);
    }
  }
  createNewCours(date: Date) {
    let modal = this.modalService.open(PlanningLessonCreationModalComponent);
    modal.componentInstance.minDate = this.minDate;
    modal.componentInstance.maxDate = this.maxDate;
    modal.componentInstance.beginDate = date;
    modal.componentInstance.skiingSchools = this.skiingSchools;
    modal.componentInstance.planning = this.currentPlanning;
    modal.result.then(lesson => {
      if (lesson) {
        lesson.lessonDate = new Date(lesson.lessonDate);
        lesson.lessonEndTime = new Date(lesson.lessonEndTime);
        lesson.lessonStartTime = new Date(lesson.lessonStartTime);
        this.addNewLesson(lesson);
        this.currentPlanning.lessons.push(lesson);
      }
    });
  }

  dayClicked({ date }: { date: Date }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
    }
  }
  addNewLesson(lesson: Lesson) {
    let newLesson = {
      id: lesson.lessonId,
      start: new Date(
        lesson.lessonDate.getTime() + lesson.lessonStartTime.getTime()
      ),
      end: new Date(
        lesson.lessonEndTime.getTime() + lesson.lessonDate.getTime()
      ),
      title:
        "Cours de " +
        lesson.lessonMonitor.username +
        " RDV fixé à " +
        lesson.lessonStartTime.toLocaleTimeString() +
        " à " +
        lesson.lessonMeetingPlace,
      color: colors.red,
      allDay: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      },
      draggable: false
    };
    this.skiingLessons.push(newLesson);
    this.refresh.next();
  }

  showDayDetails(): void {
    let modal = this.modalService.open(PlanningLessonEditionModalComponent);
    modal.componentInstance.editingLesson = this.currentPlanning.lessons
      .filter(lesson => lesson.lessonDate.getTime() == this.viewDate.getTime())
      .shift();
    modal.componentInstance.minDate = this.minDate;
    modal.componentInstance.maxDate = this.maxDate;
    modal.componentInstance.skiingSchools = this.skiingSchools;
    modal.componentInstance.currentPlanning = this.currentPlanning;
    modal.componentInstance.planningLessons = this.skiingLessons;
    modal.result.then(lesson => {
      if (lesson) {
        lesson.lessonDate = new Date(lesson.lessonDate);
        lesson.lessonEndTime = new Date(lesson.lessonEndTime);
        lesson.lessonStartTime = new Date(lesson.lessonStartTime);
        this.addNewLesson(lesson);
      }
    });
  }

  dateIsValid(date: Date): boolean {
    return date >= this.minDate && date <= this.maxDate;
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      if (!this.dateIsValid(day.date)) {
        day.cssClass = "cal-disabled-cell";
      }
    });
  }

  ngOnInit() {
    let id = this._activatedRoute.snapshot.queryParamMap.get("planningId");
    this.planningService.getPlanningById(parseInt(id)).subscribe(result => {
      this.currentPlanning = result as Planning;
      this.minDate = new Date(this.currentPlanning.planningBeginning);
      this.viewDate = new Date(this.currentPlanning.planningBeginning);
      this.maxDate = new Date(this.currentPlanning.planningEnd);
      this.dateOrViewChanged();
      this.currentPlanning.lessons.map(
        lesson => (lesson.lessonStartTime = new Date(lesson.lessonStartTime))
      );
      this.currentPlanning.lessons.map(
        lesson => (lesson.lessonEndTime = new Date(lesson.lessonEndTime))
      );
      this.currentPlanning.lessons.map(
        lesson => (lesson.lessonDate = new Date(lesson.lessonDate))
      );
      this.currentPlanning.lessons.map(lesson => this.addNewLesson(lesson));
    });
    this.skiingSchoolService.getAllSkiingSchool().subscribe(result => {
      this.skiingSchools = result as Array<SkiingSchool>;
    });
  }
}
