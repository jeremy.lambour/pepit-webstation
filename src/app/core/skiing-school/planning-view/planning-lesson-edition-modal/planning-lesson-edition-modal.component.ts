import { Component, OnInit, Input } from "@angular/core";
import { Lesson } from "src/app/common/models/planning/lesson";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormControl
} from "@angular/forms";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { SkiingSchool } from "src/app/common/models/planning/skiingSchool";
import { LessonService } from "src/app/common/services/lesson/lesson.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Planning } from "src/app/common/models/planning/planning";
import { CalendarEvent } from "calendar-utils";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";

@Component({
  selector: "app-planning-lesson-edition-modal",
  templateUrl: "./planning-lesson-edition-modal.component.html",
  styleUrls: ["./planning-lesson-edition-modal.component.css"]
})
export class PlanningLessonEditionModalComponent implements OnInit {
  @Input() editingLesson: Lesson;
  @Input() skiingSchools: Array<SkiingSchool>;
  @Input() currentPlanning: Planning;
  @Input() planningLessons: Array<CalendarEvent>;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  public editingLessonForm: FormGroup;
  private modalService: ModalService;
  public editLesson: Boolean;
  private lessonService: LessonService;

  constructor(
    private _builder: FormBuilder,
    private _modalService: ModalService,
    private _lessonService: LessonService,
    public activeModal: NgbActiveModal,
    public _dateFormatter: NgbStringAdapter
  ) {
    this.editingLessonForm = _builder.group({
      lessonLevel: ["", Validators.required],
      lessonSport: ["", Validators.required],
      lessonClub: ["", Validators.required],
      lessonDate: ["", Validators.required],
      lessonStartTime: ["", Validators.required],
      lessonEndTime: ["", Validators.required],
      lessonMeetingPlace: ["", Validators.required],
      lessonSkiingSchool: ["", Validators.required],
      lessonNumberPlaces: ["", Validators.required]
    });
    this.editingLessonForm
      .get("lessonEndTime")
      .setValidators([
        Validators.required,
        this.isAfterStartTime(
          this.editingLessonForm.get("lessonStartTime").value
        )
      ]);
    this.modalService = _modalService;
    this.editLesson = false;
    this.editingLessonForm.controls.disable;
    this.editingLessonForm.get("lessonLevel").disable();
    this.editingLessonForm.get("lessonSport").disable();
    this.editingLessonForm.get("lessonClub").disable();
    this.editingLessonForm.get("lessonDate").disable();
    this.editingLessonForm.get("lessonStartTime").disable();
    this.editingLessonForm.get("lessonEndTime").disable();
    this.editingLessonForm.get("lessonMeetingPlace").disable();
    this.editingLessonForm.get("lessonSkiingSchool").disable();
    this.editingLessonForm.get("lessonNumberPlaces").disable();

    this.lessonService = _lessonService;
  }

  updateLesson() {
    let lesson = new Lesson();
    lesson.lessonId = this.editingLesson.lessonId;
    lesson.lessonMonitor = this.editingLesson.lessonMonitor;
    lesson.lessonPlanning = this.editingLesson.lessonPlanning;
    lesson.lessonLevel = this.editingLessonForm.controls["lessonLevel"].value;
    lesson.lessonClub = this.editingLessonForm.controls["lessonClub"].value;
    lesson.lessonSport = this.editingLessonForm.controls["lessonSport"].value;
    lesson.lessonDate = this.editingLessonForm.controls["lessonDate"].value;
    lesson.lessonDuration =
      parseInt(this.editingLessonForm.controls["lessonEndTime"].value) -
      parseInt(this.editingLessonForm.controls["lessonStartTime"].value);
    let startTime = this.editingLessonForm.controls[
      "lessonStartTime"
    ].value.split(":");
    lesson.lessonStartTime = new Date(lesson.lessonDate.getTime());
    lesson.lessonStartTime.setHours(startTime[0], startTime[1]);
    let endTime = this.editingLessonForm.controls["lessonEndTime"].value.split(
      ":"
    );
    lesson.lessonEndTime = new Date(lesson.lessonDate.getTime());
    lesson.lessonEndTime.setHours(endTime[0], endTime[1]);
    lesson.lessonMeetingPlace = this.editingLessonForm.controls[
      "lessonMeetingPlace"
    ].value;
    lesson.lessonNumberPlaces = this.editingLessonForm.controls[
      "lessonNumberPlaces"
    ].value;
    lesson.lessonSkiingSchool = this.skiingSchools
      .filter(
        school =>
          this.editingLessonForm.controls["lessonSkiingSchool"].value ==
          school.skiingSchoolId.toString()
      )
      .shift();
    this.lessonService.updateLesson(lesson).subscribe(result => {
      let lesson = result as Lesson;
      this.removeOldValues(lesson);
      this.activeModal.close(lesson);
    });
  }

  deleteLesson() {
    this.lessonService
      .deleteLesson(this.editingLesson.lessonId)
      .subscribe(result => {
        if (result) {
          this.removeOldValues(result as Lesson);
          this.activeModal.close();
        }
      });
  }
  changeState() {
    this.editLesson = !this.editLesson;
    this.editingLessonForm.get("lessonLevel").disabled
      ? this.editingLessonForm.get("lessonLevel").enable()
      : this.editingLessonForm.get("lessonLevel").disable();
    this.editingLessonForm.get("lessonSport").disabled
      ? this.editingLessonForm.get("lessonSport").enable()
      : this.editingLessonForm.get("lessonSport").disable();
    this.editingLessonForm.get("lessonClub").disabled
      ? this.editingLessonForm.get("lessonClub").enable()
      : this.editingLessonForm.get("lessonClub").disable();
    this.editingLessonForm.get("lessonDate").disabled
      ? this.editingLessonForm.get("lessonDate").enable()
      : this.editingLessonForm.get("lessonDate").disable();
    this.editingLessonForm.get("lessonStartTime").disabled
      ? this.editingLessonForm.get("lessonStartTime").enable()
      : this.editingLessonForm.get("lessonStartTime").disable();
    this.editingLessonForm.get("lessonEndTime").disabled
      ? this.editingLessonForm.get("lessonEndTime").enable()
      : this.editingLessonForm.get("lessonEndTime").disable();
    this.editingLessonForm.get("lessonMeetingPlace").disabled
      ? this.editingLessonForm.get("lessonMeetingPlace").enable()
      : this.editingLessonForm.get("lessonMeetingPlace").disable();
    this.editingLessonForm.get("lessonSkiingSchool").disabled
      ? this.editingLessonForm.get("lessonSkiingSchool").enable()
      : this.editingLessonForm.get("lessonSkiingSchool").disable();
    this.editingLessonForm.get("lessonNumberPlaces").disabled
      ? this.editingLessonForm.get("lessonNumberPlaces").enable()
      : this.editingLessonForm.get("lessonNumberPlaces").disable();
  }
  ngOnInit() {
    this.editingLessonForm
      .get("lessonLevel")
      .setValue(this.editingLesson.lessonLevel);
    this.editingLessonForm
      .get("lessonSport")
      .setValue(this.editingLesson.lessonSport);
    this.editingLessonForm
      .get("lessonClub")
      .setValue(this.editingLesson.lessonClub);
    this.editingLessonForm
      .get("lessonDate")
      .setValue(this.editingLesson.lessonDate);
    this.editingLessonForm
      .get("lessonStartTime")
      .setValue(this.editingLesson.lessonStartTime.toTimeString());
    this.editingLessonForm
      .get("lessonEndTime")
      .setValue(this.editingLesson.lessonEndTime.toTimeString());
    this.editingLessonForm
      .get("lessonMeetingPlace")
      .setValue(this.editingLesson.lessonMeetingPlace);
    this.editingLessonForm
      .get("lessonSkiingSchool")
      .setValue(this.editingLesson.lessonSkiingSchool.skiingSchoolId);
    this.editingLessonForm
      .get("lessonNumberPlaces")
      .setValue(this.editingLesson.lessonNumberPlaces);
  }

  dismiss() {
    console.log(this.editingLessonForm);
    this.modalService.closeCurrent();
  }

  removeOldValues(lesson: Lesson) {
    const replaceItemIndex = this.currentPlanning.lessons.findIndex(
      x => x.lessonId == lesson.lessonId
    );
    this.currentPlanning.lessons.splice(replaceItemIndex, 1, lesson);
    const itemIndex = this.planningLessons.findIndex(
      x => x.id == lesson.lessonId
    );
    this.planningLessons.splice(itemIndex, 1);
  }

  isAfterStartTime(time: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = new Date(time) > new Date(control.value);
      return forbidden ? { forbiddenTime: { value: control.value } } : null;
    };
  }
}
