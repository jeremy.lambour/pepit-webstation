import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningLessonEditionModalComponent } from './planning-lesson-edition-modal.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Lesson } from 'src/app/common/models/planning/lesson';
import { SkiingSchool } from 'src/app/common/models/planning/skiingSchool';
import { Planning } from 'src/app/common/models/planning/planning';
import { CalendarEvent } from 'calendar-utils';

describe('PlanningLessonEditionModalComponent', () => {
  let component: PlanningLessonEditionModalComponent;
  let fixture: ComponentFixture<PlanningLessonEditionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningLessonEditionModalComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule,
        NgbModule.forRoot(), BrowserModule, BrowserDynamicTestingModule],
      providers: [NgbModal, NgbActiveModal, DatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningLessonEditionModalComponent);
    component = fixture.componentInstance;
    component.editingLesson = new Lesson();
    component.editingLesson.lessonId = 1;
    component.editingLesson.lessonStartTime = new Date();
    component.editingLesson.lessonEndTime = new Date();
    component.editingLesson.lessonSkiingSchool = new SkiingSchool();
    component.editingLesson.lessonSkiingSchool.skiingSchoolId = 1;
    component.skiingSchools = Array<SkiingSchool>();
    component.currentPlanning = new Planning();
    component.currentPlanning.planningId = 1;
    component.planningLessons = Array<CalendarEvent>();
    component.minDate = new Date();
    component.maxDate = new Date();
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
