import { Component, OnInit, Input } from "@angular/core";
import { User } from "src/app/common/models/User/User";
import { Router } from "@angular/router";

@Component({
  selector: "app-monitor-card",
  templateUrl: "./monitor-card.component.html",
  styleUrls: ["./monitor-card.component.css"]
})
export class MonitorCardComponent implements OnInit {
  @Input("monitor") monitor: User;
  @Input("seasonId") seasonId: number;

  private router: Router;
  constructor(private _router: Router) {
    this.router = _router;
  }

  toMonitorPlanning() {
    this.router.navigate(["/monitor/:monitorId/:seasonId"], {
      queryParams: {
        monitorId: this.monitor.userId,
        seasonId: this.seasonId
      }
    });
  }
  ngOnInit() {}
}
