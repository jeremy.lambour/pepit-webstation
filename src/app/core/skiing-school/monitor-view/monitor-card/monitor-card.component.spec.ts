import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorCardComponent } from './monitor-card.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { User } from 'src/app/common/models/User/User';

describe('MonitorCardComponent', () => {
  let component: MonitorCardComponent;
  let fixture: ComponentFixture<MonitorCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MonitorCardComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      providers: [DatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorCardComponent);
    component = fixture.componentInstance;
    component.monitor = new User();
    component.monitor.userId = 1;
    component.seasonId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
