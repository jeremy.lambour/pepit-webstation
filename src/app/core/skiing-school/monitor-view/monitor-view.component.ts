import { Component, OnInit } from "@angular/core";
import { User } from "src/app/common/models/User/User";
import { UserService } from "src/app/common/services/user/user.service";
import { AuthorityService } from "src/app/common/services/authority/authority.service";
import { Authority } from "src/app/common/models/User/authority";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-monitor-view",
  templateUrl: "./monitor-view.component.html",
  styleUrls: ["./monitor-view.component.css"]
})
export class MonitorViewComponent implements OnInit {
  public monitors: Array<User> = [];
  private userService: UserService;
  private authorityService: AuthorityService;
  private activatedRoute: ActivatedRoute;
  public currentSeasonId: number;
  constructor(
    private _userService: UserService,
    private _authorityService: AuthorityService,
    private _activeRoute: ActivatedRoute
  ) {
    this.userService = _userService;
    this.authorityService = _authorityService;
    this.activatedRoute = _activeRoute;
  }

  ngOnInit() {
    this.authorityService.getAllAuthority().subscribe(result => {
      let authorities = result as Array<Authority>;
      let monitorRole = authorities
        .filter(element => element.authorityDtoName.match("MONITOR"))
        .shift();
      if (monitorRole) {
        this.userService
          .getUserByAuthority(monitorRole.authorityDtoName)
          .subscribe(users => {
            this.monitors = users;
          });
      }
    });
    this.currentSeasonId = parseInt(
      this.activatedRoute.snapshot.queryParamMap.get("seasonId")
    );
  }
}
