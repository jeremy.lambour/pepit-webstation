import { PieChartEquipmentStatisticsComponent } from "./pie-chart-equipment-statistics.component";
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PieChartEquipmentStatisticsComponent', () => {
    let component: PieChartEquipmentStatisticsComponent;
    let fixture: ComponentFixture<PieChartEquipmentStatisticsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PieChartEquipmentStatisticsComponent],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PieChartEquipmentStatisticsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {

        expect(component).toBeTruthy();
    });

});