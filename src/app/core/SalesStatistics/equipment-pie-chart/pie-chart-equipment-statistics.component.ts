import { OnInit, Component } from '@angular/core';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { Shop } from 'src/app/common/models/shop/Shop';
import { User } from 'src/app/common/models/User/User';


@Component({
    selector: 'app-pie-chart-equipment-statistics',
    templateUrl: './pie-chart-equipment-statistics.component.html',
    styleUrls: ['./pie-chart-equipment-statistics.component.css']
})
export class PieChartEquipmentStatisticsComponent implements OnInit {

    public pieChartLabels: string[] = [];
    public pieChartData: string[] = [];
    public pieChartType = 'pie';

    public loadingPie: Boolean;

    public width: number;
    public height: number;

    private options: any = {
        legend: { position: 'right' },
        responsive: true,
        onResize: this.OnResize,

    };

    private stock: number;
    private out: number;
    private rest: number;
    public statsForm: FormGroup;
    public router: Router;
    private equipID: number;
    private equipmentList: Equipment[] = [];
    private shopList: Shop[] = [];
    private principal: User;

    constructor(private equipmentService: EquipmentServiceService,
        private shopService: ShopServiceService
    ) {

    }

    ngOnInit() {

        this.loadingPie = false;
        this.getShops();
    }

    // events
    public chartClicked(e: any): void {

    }

    public chartHovered(e: any): void {
    }

    OnResize(event) {
        console.log(event);
    }

    getShops() {
        this.shopService.getListShopToDisplay().subscribe(
            res => {
                this.shopList = res;
            }

        );
    }

    getEquipmentShop(shopID: number) {
        this.equipmentService.getListEquipmentByShop(shopID).subscribe(
            res => {
                this.equipmentList = res;
            }
        );
    }

    getEquipment(equipID: number) {

        this.equipmentService.getEquipment(equipID).subscribe(
            res => {

                this.stock = res.equipmentStock;
                this.out = res.equipmentOut;
                this.rest = (this.stock - this.out);
                this.pieChartLabels.push('Equipments en stock: ' + this.rest.toFixed(0));
                this.pieChartLabels.push('Equipments reserves: ' + this.out.toFixed(0));

                this.pieChartData.push(this.out.toFixed(0));
                this.pieChartData.push(this.rest.toFixed(0));
                this.loadingPie = true;


            }

        );

    }

    disableChart() {
        this.pieChartLabels = [];
        this.pieChartData = [];
        this.ngOnInit();
    }

}
