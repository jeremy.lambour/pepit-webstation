import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { PieChartEquipmentStatisticsComponent } from './pie-chart-equipment-statistics.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MaterialModule } from '../material.module';


@NgModule({
    declarations: [PieChartEquipmentStatisticsComponent],
    imports: [
        CommonModule, ChartsModule, MaterialModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PieChartEquipmentStatisticsModule { }
