import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { LineChartFlatReservationComponent } from './line-chart-flat-statistics.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MaterialModule } from '../material.module';


@NgModule({
    declarations: [LineChartFlatReservationComponent],
    imports: [
        CommonModule, ChartsModule, MaterialModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [DatePipe]
})
export class LineChartFlatReservationModule { }
