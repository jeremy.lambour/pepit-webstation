import { OnInit, Component } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { ChartDataSets } from 'chart.js';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'app-line-chart-flat-statistics',
    templateUrl: './line-chart-flat-statistics.component.html',
    styleUrls: ['./line-chart-flat-statistics.component.css']
})
export class LineChartFlatReservationComponent implements OnInit {

    public lineChartLabels: Array<any> = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
        'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

    public lineChartData: ChartDataSets[] = [];
    public lineChartOptions: any = {
        responsive: true
    };

    public lineChartLegend = true;
    public lineChartType = 'line';

    public loadingLine: Boolean;
    private allReservedFlat: RFlat[] = [];
    private allFlat: Flat[] = [];

    private finalIsReservedFlat: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    private finalIsFreeFlat: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public date: Date;
    public date1: string;
    public date2: Date;
    public date3: string;





    constructor(private flatReservationService: FlatReservationService,
        private flatService: FlatService, public datepipe: DatePipe,
        public datepipe1: DatePipe
    ) {
    }

    ngOnInit() {
        this.loadingLine = false;

        this.flatIsReserved();
        this.flatIsFree();
    }

    getAllFlats() {
        this.flatService.getAllFlat().subscribe(
            res => {
                this.allFlat = res;
            }
        );
    }

    getFlats() {
        this.flatReservationService.getAllReservedFlat().subscribe(
            res => {
                this.allReservedFlat = res;

            }
        );

    }
    flatIsReserved() {
        this.flatReservationService.getAllReservedFlat().subscribe(
            res => {
                this.allReservedFlat = res;
                this.allReservedFlat.forEach(element => {
                    this.date = new Date(element.startDate);
                    this.date1 = this.datepipe.transform(this.date, 'yyyy-MM-dd');
                    this.date2 = new Date(element.endDate);
                    this.date3 = this.datepipe1.transform(this.date2, 'yyyy-MM-dd');
                    if (this.date1.substr(5, 2) === '01' || (this.date3.substr(5, 2) === '01')) {
                        this.finalIsReservedFlat[0] = this.finalIsReservedFlat[0] + 1;
                    } if (this.date1.substr(5, 2) === '02' || (this.date3.substr(5, 2) === '02')) {
                        this.finalIsReservedFlat[1] = this.finalIsReservedFlat[1] + 1;
                    } if (this.date1.substr(5, 2) === '03' || (this.date3.substr(5, 2) === '03')) {
                        this.finalIsReservedFlat[2] = this.finalIsReservedFlat[2] + 1;
                    } if (this.date1.substr(5, 2) === '04' || (this.date3.substr(5, 2) === '04')) {
                        this.finalIsReservedFlat[3] = this.finalIsReservedFlat[3] + 1;
                    } if (this.date1.substr(5, 2) === '05' || this.date3.substr(5, 2) === '05') {
                        this.finalIsReservedFlat[4] = this.finalIsReservedFlat[4] + 1;
                    } if (this.date1.substr(5, 2) === '06' || this.date3.substr(5, 2) === '06') {
                        this.finalIsReservedFlat[5] = this.finalIsReservedFlat[5] + 1;
                    } if (this.date1.substr(5, 2) === '07' || this.date3.substr(5, 2) === '07') {
                        this.finalIsReservedFlat[6] = this.finalIsReservedFlat[6] + 1;
                    } if (this.date1.substr(5, 2) === '08' || this.date3.substr(5, 2) === '08') {
                        this.finalIsReservedFlat[7] = this.finalIsReservedFlat[7] + 1;
                    } if (this.date1.substr(5, 2) === '09' || this.date3.substr(5, 2) === '09') {
                        this.finalIsReservedFlat[8] = this.finalIsReservedFlat[8] + 1;
                    } if (this.date1.substr(5, 2) === '10' || this.date3.substr(5, 2) === '10') {
                        this.finalIsReservedFlat[9] = this.finalIsReservedFlat[9] + 1;
                    } if (this.date1.substr(5, 2) === '11' || this.date3.substr(5, 2) === '11') {
                        this.finalIsReservedFlat[10] = this.finalIsReservedFlat[10] + 1;
                    } if (this.date1.substr(5, 2) === '12' || this.date3.substr(5, 2) === '12') {
                        this.finalIsReservedFlat[11] = this.finalIsReservedFlat[11] + 1;
                    }

                });
                this.lineChartData.push({ data: this.finalIsReservedFlat, label: 'Logements réservés' });
            });
    }
    flatIsFree() {
        this.flatService.getAllFlat().subscribe(
            res => {
                this.allFlat = res;
                const allFlat = this.allFlat.length;
                this.finalIsFreeFlat[0] = allFlat - this.finalIsReservedFlat[0];
                this.finalIsFreeFlat[1] = allFlat - this.finalIsReservedFlat[1];
                this.finalIsFreeFlat[2] = allFlat - this.finalIsReservedFlat[2];
                this.finalIsFreeFlat[3] = allFlat - this.finalIsReservedFlat[3];
                this.finalIsFreeFlat[4] = allFlat - this.finalIsReservedFlat[4];
                this.finalIsFreeFlat[5] = allFlat - this.finalIsReservedFlat[5];
                this.finalIsFreeFlat[6] = allFlat - this.finalIsReservedFlat[6];
                this.finalIsFreeFlat[7] = allFlat - this.finalIsReservedFlat[7];
                this.finalIsFreeFlat[8] = allFlat - this.finalIsReservedFlat[8];
                this.finalIsFreeFlat[9] = allFlat - this.finalIsReservedFlat[9];
                this.finalIsFreeFlat[10] = allFlat - this.finalIsReservedFlat[10];
                this.finalIsFreeFlat[11] = allFlat - this.finalIsReservedFlat[11];
                this.lineChartData.push({ data: this.finalIsFreeFlat, label: 'Logements non réservés' });
                this.loadingLine = true;
            });
    }
    // events
    public chartClicked(e: any): void {
    }

    public chartHovered(e: any): void {
    }
}


