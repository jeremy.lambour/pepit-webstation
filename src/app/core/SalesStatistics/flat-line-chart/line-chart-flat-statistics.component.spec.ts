import { async, TestBed, ComponentFixture } from "@angular/core/testing";
import { LineChartFlatReservationComponent } from './line-chart-flat-statistics.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

describe('LineChartFlatReservationComponent', () => {
    let component: LineChartFlatReservationComponent;
    let fixture: ComponentFixture<LineChartFlatReservationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LineChartFlatReservationComponent],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            providers: [DatePipe],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LineChartFlatReservationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {

        expect(component).toBeTruthy();
    });

});