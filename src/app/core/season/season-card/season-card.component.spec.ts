import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonCardComponent } from './season-card.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Season } from 'src/app/common/models/common/Season';

describe('SeasonCardComponent', () => {
  let component: SeasonCardComponent;
  let fixture: ComponentFixture<SeasonCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeasonCardComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      providers: [DatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonCardComponent);
    component = fixture.componentInstance;
    component.currentSeason = new Season();
    component.currentSeason.seasonId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});