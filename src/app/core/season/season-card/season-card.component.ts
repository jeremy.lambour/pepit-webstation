import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { Season } from "src/app/common/models/common/Season";

@Component({
  selector: "app-season-card",
  templateUrl: "./season-card.component.html",
  styleUrls: ["./season-card.component.css"]
})
export class SeasonCardComponent implements OnInit {
  @Input("season") currentSeason: Season;
  private router: Router;
  constructor(private _router: Router) {
    this.router = _router;
  }

  ngOnInit() {}

  toMonitorView() {
    this.router.navigate(["/monitors/:seasonId"], {
      queryParams: {
        seasonId: this.currentSeason.seasonId
      }
    });
  }

  toKinderGartenPlanning() {
    this.router.navigate(["/kindergarten/:seasonId"], {
      queryParams: {
        seasonId: this.currentSeason.seasonId
      }
    });
  }
}
