import { Component, OnInit } from "@angular/core";
import { Season } from "src/app/common/models/common/Season";
import { SeasonService } from "src/app/common/services/common/season.service";

@Component({
  selector: "app-season-view",
  templateUrl: "./season-view.component.html",
  styleUrls: ["./season-view.component.css"]
})
export class SeasonViewComponent implements OnInit {
  public seasons: Array<Season> = [];
  private seasonService: SeasonService;

  constructor(private _seasonService: SeasonService) {
    this.seasonService = _seasonService;
  }

  ngOnInit() {
    this.seasonService.getAllSeasons().subscribe(result => {
      this.seasons = result as Array<Season>;
    });
  }
}
