import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonViewComponent } from './season-view.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Season } from 'src/app/common/models/common/Season';
describe('SeasonViewComponent', () => {
  let component: SeasonViewComponent;
  let fixture: ComponentFixture<SeasonViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeasonViewComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      providers: [DatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonViewComponent);
    component = fixture.componentInstance;
    component.seasons = Array<Season>();
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});