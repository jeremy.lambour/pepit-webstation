import { FestivePeriod } from './../../../common/models/common/FestivePeriod';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { HotelService } from './../../../common/services/flatManagement/hotel/hotel.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { NOT_FOUND_CHECK_ONLY_ELEMENT_INJECTOR } from '@angular/core/src/view/provider';
import { DefineService } from 'src/app/common/services/flatManagement/flat/define.service';
import { Define } from 'src/app/common/models/flatManagement/Define';

@Component({
  selector: 'app-flat-admin',
  templateUrl: './flat-admin.component.html',
  styleUrls: ['./flat-admin.component.css']
})
export class FlatAdminComponent implements OnInit {

  public ShowFilter: Boolean;
  public ShowPrice: Boolean;
  public loadingPage: Boolean;
  public formGroup: FormGroup;
  public listHotels: Hotel[] = [];
  private hotelId: number;
  public listFlats: Flat[] = [];
  public loadingFlatCategories: Boolean;
  public listCategory: FlatCategory[] = [];
  public flatCategoriesForm: FormGroup;
  public loadingPrices: Boolean;
  private defines: any[] = [];
  public pricesForm: FormGroup;

  public priceWinterInterSeason: number;
  public priceChristmas: number;
  public priceNewYear: number;
  public priceJanFebWeek: number;
  public priceFebVacation: number;
  public priceMarchApril: number;
  public priceEaster: number;
  public priceSummerInterSeason: number;
  public priceEndJune: number;
  public priceMiddleJuly: number;
  public priceJulyAugust: number;
  public priceAugust: number;
  public priceSeptember: number;
  public priceNight: number;

  public startWinterInterSeason: Date;
  public endWinterInterSeason: Date;
  public startChristmas: Date;
  public endChristmas: Date;
  public startNewYear: Date;
  public endNewYear: Date;
  public startJanFebWeek: Date;
  public endJanFebWeek: Date;
  public startFebVacation: Date;
  public endFebVacation: Date;
  public startMarchApril: Date;
  public endMarchApril: Date;
  public startEaster: Date;
  public endEaster: Date;
  public startSummerInterSeason: Date;
  public endSummerInterSeason: Date;
  public startEndJune: Date;
  public endEndJune: Date;
  public startMiddleJuly: Date;
  public endMiddleJuly: Date;
  public startJulyAugust: Date;
  public endJulyAugust: Date;
  public startAugust: Date;
  public endAugust: Date;
  public startSeptember: Date;
  public endSeptember: Date;

  public defineWinterInterSeason: Define = new Define();
  public defineChristmas: Define = new Define();
  public defineNewYear: Define = new Define();
  public defineJanFebWeek: Define = new Define();
  public defineFebVacation: Define = new Define();
  public defineMarchApril: Define = new Define();
  public defineEaster: Define = new Define();
  public defineSummerInterSeason: Define = new Define();
  public defineEndJune: Define = new Define();
  public defineMiddleJuly: Define = new Define();
  public defineJulyAugust: Define = new Define();
  public defineAugust: Define = new Define();
  public defineSeptember: Define = new Define();
  public defineNight: Define = new Define();
  public flat: Flat;

  constructor(private formBuilder: FormBuilder, private hotelService: HotelService, private flatService: FlatService,
    public flatCategoryService: FlatCategoryService, private defineService: DefineService) {
    this.formGroup = this.formBuilder.group({
      hotel: ['', Validators.required]
    });
    this.flatCategoriesForm = this.formBuilder.group({
      category: ['', Validators.required]
    });
    this.pricesForm = this.formBuilder.group({
      priceWinterInterSeason: ['', Validators.required],
      priceChristmas: ['', Validators.required],
      priceNewYear: ['', Validators.required],
      priceJanFebWeek: ['', Validators.required],
      priceFebVacation: ['', Validators.required],
      priceMarchApril: ['', Validators.required],
      priceEaster: ['', Validators.required],
      priceSummerInterSeason: ['', Validators.required],
      priceEndJune: ['', Validators.required],
      priceMiddleJuly: ['', Validators.required],
      priceJulyAugust: ['', Validators.required],
      priceAugust: ['', Validators.required],
      priceSeptember: ['', Validators.required],
      priceNight: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.ShowFilter = true;
    this.ShowPrice = false;
    this.loadingPage = false;
    this.loadingFlatCategories = false;
    this.loadingPrices = false;
    this.getHotels();
  }

  getHotels() {
    this.listHotels.length = 0;
    this.hotelService.getListHotelToDisplay().subscribe(
      listHotels => {
        this.listHotels = listHotels;
        this.loadingPage = true;
      }
    );
  }

  onFormSubmit(formValue: any) {
    this.listFlats.length = 0;
    this.listCategory.length = 0;
    this.hotelId = this.formGroup.get('hotel').value;
    this.flatService.getListFlatToDisplay(this.hotelId).subscribe(
      listFlats => {
        this.listFlats = listFlats;
        this.listFlats.forEach(flat => {
          let found = false;
          this.listCategory.forEach(flatCategory => {
            if (flat.flatCategoryDto.flatCategoryId === flatCategory.flatCategoryId) {
              found = true;
            }
          });
          if (found === false) {
            this.listCategory.push(flat.flatCategoryDto);
          }
        });
        this.ShowFilter = true;
        this.ShowPrice = false;
        this.loadingFlatCategories = true;
      }
    );
  }

  selectCategory(flatCategoriesFormValue: any) {
    this.defines.length = 0;
    this.listFlats.forEach(flat => {
      this.defineService.getDefine(flat.flatId).subscribe(
        define => {
          define.forEach(element => {
            const flatCategoryId = '' + element.flatDto.flatCategoryDto.flatCategoryId;

            if (flatCategoryId === this.flatCategoriesForm.get('category').value) {
              this.flat = flat;
              if (element.festivePeriodDto.festivePeriodId === 1) {
                this.startWinterInterSeason = element.festivePeriodDto.festivePeriodBeginning;
                this.endWinterInterSeason = element.festivePeriodDto.festivePeriodEnd;
                this.priceWinterInterSeason = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 2) {
                this.startChristmas = element.festivePeriodDto.festivePeriodBeginning;
                this.endChristmas = element.festivePeriodDto.festivePeriodEnd;
                this.priceChristmas = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 3) {
                this.startNewYear = element.festivePeriodDto.festivePeriodBeginning;
                this.endNewYear = element.festivePeriodDto.festivePeriodEnd;
                this.priceNewYear = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 4) {
                this.startJanFebWeek = element.festivePeriodDto.festivePeriodBeginning;
                this.endJanFebWeek = element.festivePeriodDto.festivePeriodEnd;
                this.priceJanFebWeek = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 5) {
                this.startFebVacation = element.festivePeriodDto.festivePeriodBeginning;
                this.endFebVacation = element.festivePeriodDto.festivePeriodEnd;
                this.priceFebVacation = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 6) {
                this.startMarchApril = element.festivePeriodDto.festivePeriodBeginning;
                this.endMarchApril = element.festivePeriodDto.festivePeriodEnd;
                this.priceMarchApril = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 7) {
                this.startEaster = element.festivePeriodDto.festivePeriodBeginning;
                this.endEaster = element.festivePeriodDto.festivePeriodEnd;
                this.priceEaster = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 8) {
                this.startSummerInterSeason = element.festivePeriodDto.festivePeriodBeginning;
                this.endSummerInterSeason = element.festivePeriodDto.festivePeriodEnd;
                this.priceSummerInterSeason = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 9) {
                this.startEndJune = element.festivePeriodDto.festivePeriodBeginning;
                this.endEndJune = element.festivePeriodDto.festivePeriodEnd;
                this.priceEndJune = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 10) {
                this.startMiddleJuly = element.festivePeriodDto.festivePeriodBeginning;
                this.endMiddleJuly = element.festivePeriodDto.festivePeriodEnd;
                this.priceMiddleJuly = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 11) {
                this.startJulyAugust = element.festivePeriodDto.festivePeriodBeginning;
                this.endJulyAugust = element.festivePeriodDto.festivePeriodEnd;
                this.priceJulyAugust = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 12) {
                this.startAugust = element.festivePeriodDto.festivePeriodBeginning;
                this.endAugust = element.festivePeriodDto.festivePeriodEnd;
                this.priceAugust = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 13) {
                this.startSeptember = element.festivePeriodDto.festivePeriodBeginning;
                this.endSeptember = element.festivePeriodDto.festivePeriodEnd;
                this.priceSeptember = element.flatPricePerNight;
              } else if (element.festivePeriodDto.festivePeriodId === 14) {
                this.priceNight = element.flatPricePerNight;
              }
            }
          });
          this.ShowFilter = false;
          this.ShowPrice = true;
          this.loadingPrices = true;
        });
    });
  }

  changePrices(pricesFormValue: any) {
    this.priceWinterInterSeason = this.pricesForm.get('priceWinterInterSeason').value;
    this.defineWinterInterSeason.flatDto = this.flat;
    this.defineWinterInterSeason.festivePeriodDto = new FestivePeriod();
    this.defineWinterInterSeason.festivePeriodDto.festivePeriodId = 1;
    this.defineWinterInterSeason.flatPricePerNight = this.priceWinterInterSeason;
    this.defineService.updateDefine(this.defineWinterInterSeason).subscribe(
      defineWinterInterSeason => {
        this.priceChristmas = this.pricesForm.get('priceChristmas').value;
        this.defineChristmas.flatDto = this.flat;
        this.defineChristmas.festivePeriodDto = new FestivePeriod();
        this.defineChristmas.festivePeriodDto.festivePeriodId = 2;
        this.defineChristmas.flatPricePerNight = this.priceChristmas;
        this.defineService.updateDefine(this.defineChristmas).subscribe(
          defineChristmas => {
            this.priceNewYear = this.pricesForm.get('priceNewYear').value;
            this.defineNewYear.flatDto = this.flat;
            this.defineNewYear.festivePeriodDto = new FestivePeriod();
            this.defineNewYear.festivePeriodDto.festivePeriodId = 3;
            this.defineNewYear.flatPricePerNight = this.priceChristmas;
            this.defineService.updateDefine(this.defineNewYear).subscribe(
              defineNewYear => {
                this.priceJanFebWeek = this.pricesForm.get('priceJanFebWeek').value;
                this.defineJanFebWeek.flatDto = this.flat;
                this.defineJanFebWeek.festivePeriodDto = new FestivePeriod();
                this.defineJanFebWeek.festivePeriodDto.festivePeriodId = 4;
                this.defineJanFebWeek.flatPricePerNight = this.priceJanFebWeek;
                this.defineService.updateDefine(this.defineJanFebWeek).subscribe(
                  defineJanFebWeek => {
                    this.priceFebVacation = this.pricesForm.get('priceFebVacation').value;
                    this.defineFebVacation.flatDto = this.flat;
                    this.defineFebVacation.festivePeriodDto = new FestivePeriod();
                    this.defineFebVacation.festivePeriodDto.festivePeriodId = 5;
                    this.defineFebVacation.flatPricePerNight = this.priceFebVacation;
                    this.defineService.updateDefine(this.defineFebVacation).subscribe(
                      defineFebVacation => {
                        this.priceMarchApril = this.pricesForm.get('priceMarchApril').value;
                        this.defineMarchApril.flatDto = this.flat;
                        this.defineMarchApril.festivePeriodDto = new FestivePeriod();
                        this.defineMarchApril.festivePeriodDto.festivePeriodId = 6;
                        this.defineMarchApril.flatPricePerNight = this.priceMarchApril;
                        this.defineService.updateDefine(this.defineMarchApril).subscribe(
                          defineMarchApril => {
                            this.priceEaster = this.pricesForm.get('priceEaster').value;
                            this.defineEaster.flatDto = this.flat;
                            this.defineEaster.festivePeriodDto = new FestivePeriod();
                            this.defineEaster.festivePeriodDto.festivePeriodId = 7;
                            this.defineEaster.flatPricePerNight = this.priceEaster;
                            this.defineService.updateDefine(this.defineEaster).subscribe(
                              defineEaster => {
                                this.priceSummerInterSeason = this.pricesForm.get('priceSummerInterSeason').value;
                                this.defineSummerInterSeason.flatDto = this.flat;
                                this.defineSummerInterSeason.festivePeriodDto = new FestivePeriod();
                                this.defineSummerInterSeason.festivePeriodDto.festivePeriodId = 8;
                                this.defineSummerInterSeason.flatPricePerNight = this.priceSummerInterSeason;
                                this.defineService.updateDefine(this.defineSummerInterSeason).subscribe(
                                  defineSummerInterSeason => {
                                    this.priceEndJune = this.pricesForm.get('priceEndJune').value;
                                    this.defineEndJune.flatDto = this.flat;
                                    this.defineEndJune.festivePeriodDto = new FestivePeriod();
                                    this.defineEndJune.festivePeriodDto.festivePeriodId = 9;
                                    this.defineEndJune.flatPricePerNight = this.priceEndJune;
                                    this.defineService.updateDefine(this.defineEndJune).subscribe(
                                      defineEndJune => {
                                        this.priceMiddleJuly = this.pricesForm.get('priceMiddleJuly').value;
                                        this.defineMiddleJuly.flatDto = this.flat;
                                        this.defineMiddleJuly.festivePeriodDto = new FestivePeriod();
                                        this.defineMiddleJuly.festivePeriodDto.festivePeriodId = 10;
                                        this.defineMiddleJuly.flatPricePerNight = this.priceMiddleJuly;
                                        this.defineService.updateDefine(this.defineMiddleJuly).subscribe(
                                          defineMiddleJuly => {
                                            this.priceJulyAugust = this.pricesForm.get('priceJulyAugust').value;
                                            this.defineJulyAugust.flatDto = this.flat;
                                            this.defineJulyAugust.festivePeriodDto = new FestivePeriod();
                                            this.defineJulyAugust.festivePeriodDto.festivePeriodId = 11;
                                            this.defineJulyAugust.flatPricePerNight = this.priceJulyAugust;
                                            this.defineService.updateDefine(this.defineJulyAugust).subscribe(
                                              defineJulyAugust => {
                                                this.priceAugust = this.pricesForm.get('priceAugust').value;
                                                this.defineAugust.flatDto = this.flat;
                                                this.defineAugust.festivePeriodDto = new FestivePeriod();
                                                this.defineAugust.festivePeriodDto.festivePeriodId = 12;
                                                this.defineAugust.flatPricePerNight = this.priceAugust;
                                                this.defineService.updateDefine(this.defineAugust).subscribe(
                                                  defineAugust => {
                                                    this.priceSeptember = this.pricesForm.get('priceSeptember').value;
                                                    this.defineSeptember.flatDto = this.flat;
                                                    this.defineSeptember.festivePeriodDto = new FestivePeriod();
                                                    this.defineSeptember.festivePeriodDto.festivePeriodId = 13;
                                                    this.defineSeptember.flatPricePerNight = this.priceSeptember;
                                                    this.defineService.updateDefine(this.defineSeptember).subscribe(
                                                      defineSeptember => {
                                                        this.priceNight = this.pricesForm.get('priceNight').value;
                                                        this.defineNight.flatDto = this.flat;
                                                        this.defineNight.festivePeriodDto = new FestivePeriod();
                                                        this.defineNight.festivePeriodDto.festivePeriodId = 14;
                                                        this.defineNight.flatPricePerNight = this.priceNight;
                                                        this.defineService.updateDefine(this.defineNight).subscribe(
                                                          defineNight => {

                                                          }
                                                        );
                                                      }
                                                    );
                                                  }
                                                );
                                              }
                                            );
                                          }
                                        );
                                      }
                                    );
                                  }
                                );
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  goBack() {
    this.ShowFilter = true;
    this.ShowPrice = false;
  }


}
