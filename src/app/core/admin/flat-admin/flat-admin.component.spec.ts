import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlatAdminComponent } from './flat-admin.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { DefineService } from 'src/app/common/services/flatManagement/flat/define.service';

describe('FlatAdminComponent', () => {
  let component: FlatAdminComponent;
  let fixture: ComponentFixture<FlatAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlatAdminComponent],
      providers: [HotelService, FlatService, FlatCategoryService, DefineService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlatAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});

