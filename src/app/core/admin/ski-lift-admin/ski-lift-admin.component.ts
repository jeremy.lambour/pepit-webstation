import { PassType } from './../../../common/models/equipmentType/passType';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SkiLift } from 'src/app/common/models/equipment/skiLift';
import { PassTypeService } from 'src/app/common/services/passType/passType.service';
import { AgeRangeService } from 'src/app/common/services/common/ageRange.service';
import { AgeRange } from 'src/app/common/models/User/ageRange';
import { SkiLiftService } from 'src/app/common/services/skiLift/skiLift.service';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-ski-lift-admin',
  templateUrl: './ski-lift-admin.component.html',
  styleUrls: ['./ski-lift-admin.component.css']
})
export class SkiLiftAdminComponent implements OnInit {

  public ShowFilter: Boolean;
  public ShowPrice: Boolean;
  public loadingPage: Boolean;
  public loadingDomains: Boolean;
  public loadingAgeRanges: Boolean;
  public loadingPricesToutSchuss: Boolean;
  public loadingPricesNordique: Boolean;
  public formGroup: FormGroup;
  public domainsForm: FormGroup;
  public ageRangesForm: FormGroup;
  public pricesToutSchussForm: FormGroup;
  public pricesNordiqueForm: FormGroup;
  public listSkiLift: SkiLift[] = [];
  public listPassType: PassType[] = [];
  public listDomains: string[] = [];
  public passType: string;
  public domain: string;
  public listAgeRanges: AgeRange[] = [];
  public cmpt = 0;
  public ageRange: string;
  public listSkiLiftPrices: SkiLift[] = [];

  public priceHalfADay: number;
  public priceOneDay: number;
  public priceTwoDays: number;
  public priceThreeDays: number;
  public priceFourDays: number;
  public priceFiveDays: number;
  public priceSixDays: number;
  public priceSevenDays: number;
  public priceEightDays: number;
  public priceSeason: number;

  public skiLiftHalfADay: SkiLift;
  public skiLiftOneDay: SkiLift;
  public skiLiftTwoDays: SkiLift;
  public skiLiftThreeDays: SkiLift;
  public skiLiftFourDays: SkiLift;
  public skiLiftFiveDays: SkiLift;
  public skiLiftSixDays: SkiLift;
  public skiLiftSevenDays: SkiLift;
  public skiLiftEightDays: SkiLift;
  public skiLiftSeason: SkiLift;

  constructor(private formBuilder: FormBuilder, private skiLiftService: SkiLiftService,
    private skiLiftReservationService: SkiLiftReservationService,
    private passTypeService: PassTypeService, private ageRangeService: AgeRangeService) {
    this.formGroup = this.formBuilder.group({
      passType: ['', Validators.required]
    });
    this.domainsForm = this.formBuilder.group({
      domain: ['', Validators.required]
    });
    this.ageRangesForm = this.formBuilder.group({
      ageRange: ['', Validators.required]
    });
    this.pricesToutSchussForm = this.formBuilder.group({
      priceHalfADay: ['', Validators.required],
      priceOneDay: ['', Validators.required],
      priceTwoDays: ['', Validators.required],
      priceThreeDays: ['', Validators.required],
      priceFourDays: ['', Validators.required],
      priceFiveDays: ['', Validators.required],
      priceSixDays: ['', Validators.required],
      priceSevenDays: ['', Validators.required],
      priceEightDays: ['', Validators.required],
      priceSeason: ['', Validators.required]
    });

    this.pricesNordiqueForm = this.formBuilder.group({
      priceHalfADay: ['', Validators.required],
      priceOneDay: ['', Validators.required],
      priceTwoDays: ['', Validators.required],
      priceThreeDays: ['', Validators.required],
      priceFourDays: ['', Validators.required],
      priceFiveDays: ['', Validators.required],
      priceSixDays: ['', Validators.required],
      priceSevenDays: ['', Validators.required],
      priceSeason: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.ShowFilter = true;
    this.ShowPrice = false;
    this.loadingPage = false;
    this.loadingDomains = false;
    this.loadingAgeRanges = false;
    this.loadingPricesToutSchuss = false;
    this.loadingPricesNordique = false;
    this.getPassTypes();
  }

  getPassTypes() {
    this.listPassType.length = 0;
    this.passTypeService.findAll().subscribe(
      listPassType => {
        this.listPassType = listPassType;
        this.loadingPage = true;
      }
    );
  }

  getDomains() {
    this.listDomains.length = 0;
    this.skiLiftReservationService.getAll().subscribe(
      listSkiLift => {
        this.listSkiLift = listSkiLift;
        this.listSkiLift.forEach(skiLift => {
          let found = false;
          this.listDomains.forEach(domain => {
            if (skiLift.domain === domain) {
              found = true;
            }
          });
          if (found === false && skiLift.domain !== null) {
            this.listDomains.push(skiLift.domain);
          }
        });
        this.loadingDomains = true;
      }
    );
  }

  onFormSubmit(formValue: any) {
    this.passType = this.formGroup.get('passType').value;
    if (this.passType === '1') {
      this.loadingPricesToutSchuss = false;
      this.loadingPricesNordique = false;
      this.getDomains();
    } else {
      this.cmpt = 0;
      this.loadingPricesToutSchuss = false;
      this.loadingPricesNordique = false;
      this.getAgeRanges();
    }

  }

  selectDomain(domainsFormValue: any) {
    this.domain = this.domainsForm.get('domain').value;
    this.cmpt = 0;
    this.loadingPricesToutSchuss = false;
    this.loadingPricesNordique = false;
    this.getAgeRanges();
  }

  getAgeRanges() {
    this.listAgeRanges.length = 0;
    this.ageRangeService.findAll().subscribe(
      listAgeRange => {
        const l = listAgeRange;
        if (this.cmpt === 0) {
          l.forEach(element => {
            if (element.ageRangeId !== 1 && element.ageRangeId !== 2) {
              this.listAgeRanges.push(element);
              this.cmpt = this.cmpt + 1;
            }
          });
        }
        this.loadingAgeRanges = true;
      }
    );
  }

  selectAgeRange(ageRangesFormValue: any) {
    this.ageRange = this.ageRangesForm.get('ageRange').value;
    this.skiLiftService.findSkiLift(this.ageRange, this.domain, this.passType).subscribe(
      listSkiList => {
        this.listSkiLiftPrices = listSkiList;
        if (this.passType === '1') {
          this.listSkiLiftPrices.forEach(element => {
            if (element.duration === '1/2') {
              this.priceHalfADay = element.price;
            } else if (element.duration === '1') {
              this.priceOneDay = element.price;
            } else if (element.duration === '2') {
              this.priceTwoDays = element.price;
            } else if (element.duration === '3') {
              this.priceThreeDays = element.price;
            } else if (element.duration === '4') {
              this.priceFourDays = element.price;
            } else if (element.duration === '5') {
              this.priceFiveDays = element.price;
            } else if (element.duration === '6') {
              this.priceSixDays = element.price;
            } else if (element.duration === '7') {
              this.priceSevenDays = element.price;
            } else if (element.duration === '8') {
              this.priceEightDays = element.price;
            } else if (element.duration === 'saison') {
              this.priceSeason = element.price;
            }
          });
          this.ShowPrice = true;
          this.ShowFilter = false;
          this.loadingPricesToutSchuss = true;
        } else {
          this.listSkiLiftPrices.forEach(element => {
            if (element.duration === '1/2') {
              this.priceHalfADay = element.price;
            } else if (element.duration === '1') {
              this.priceOneDay = element.price;
            } else if (element.duration === '2') {
              this.priceTwoDays = element.price;
            } else if (element.duration === '3') {
              this.priceThreeDays = element.price;
            } else if (element.duration === '4') {
              this.priceFourDays = element.price;
            } else if (element.duration === '5') {
              this.priceFiveDays = element.price;
            } else if (element.duration === '6') {
              this.priceSixDays = element.price;
            } else if (element.duration === '7') {
              this.priceSevenDays = element.price;
            } else if (element.duration === 'saison') {
              this.priceSeason = element.price;
            }
          });
          this.ShowPrice = true;
          this.ShowFilter = false;
          this.loadingPricesNordique = true;
        }
      }
    );
  }

  changePricesToutSchuss(pricesToutSchussFormValue: any) {
    this.priceHalfADay = this.pricesToutSchussForm.get('priceHalfADay').value;
    this.skiLiftHalfADay = new SkiLift();
    this.skiLiftHalfADay.duration = '1/2';
    this.skiLiftHalfADay.domain = this.domain;
    this.skiLiftHalfADay.passType = new PassType();
    this.skiLiftHalfADay.passType.passTypeId = +this.passType;
    this.skiLiftHalfADay.price = this.priceHalfADay;
    this.skiLiftHalfADay.ageRange = new AgeRange();
    this.skiLiftHalfADay.ageRange.ageRangeId = +this.ageRange;
    this.skiLiftService.updateSkiLift(this.skiLiftHalfADay).subscribe(
      skiLiftHalfADay => {
        this.priceOneDay = this.pricesToutSchussForm.get('priceOneDay').value;
        this.skiLiftOneDay = new SkiLift();
        this.skiLiftOneDay.duration = '1';
        this.skiLiftOneDay.domain = this.domain;
        this.skiLiftOneDay.passType = new PassType();
        this.skiLiftOneDay.passType.passTypeId = +this.passType;
        this.skiLiftOneDay.price = this.priceOneDay;
        this.skiLiftOneDay.ageRange = new AgeRange();
        this.skiLiftOneDay.ageRange.ageRangeId = +this.ageRange;
        this.skiLiftService.updateSkiLift(this.skiLiftOneDay).subscribe(
          skiLiftOneDay => {
            this.priceTwoDays = this.pricesToutSchussForm.get('priceTwoDays').value;
            this.skiLiftTwoDays = new SkiLift();
            this.skiLiftTwoDays.duration = '2';
            this.skiLiftTwoDays.domain = this.domain;
            this.skiLiftTwoDays.passType = new PassType();
            this.skiLiftTwoDays.passType.passTypeId = +this.passType;
            this.skiLiftTwoDays.price = this.priceTwoDays;
            this.skiLiftTwoDays.ageRange = new AgeRange();
            this.skiLiftTwoDays.ageRange.ageRangeId = +this.ageRange;
            this.skiLiftService.updateSkiLift(this.skiLiftTwoDays).subscribe(
              skiLiftTwoDays => {
                this.priceThreeDays = this.pricesToutSchussForm.get('priceThreeDays').value;
                this.skiLiftThreeDays = new SkiLift();
                this.skiLiftThreeDays.duration = '3';
                this.skiLiftThreeDays.domain = this.domain;
                this.skiLiftThreeDays.passType = new PassType();
                this.skiLiftThreeDays.passType.passTypeId = +this.passType;
                this.skiLiftThreeDays.price = this.priceThreeDays;
                this.skiLiftThreeDays.ageRange = new AgeRange();
                this.skiLiftThreeDays.ageRange.ageRangeId = +this.ageRange;
                this.skiLiftService.updateSkiLift(this.skiLiftThreeDays).subscribe(
                  skiLiftThreeDays => {
                    this.priceFourDays = this.pricesToutSchussForm.get('priceFourDays').value;
                    this.skiLiftFourDays = new SkiLift();
                    this.skiLiftFourDays.duration = '4';
                    this.skiLiftFourDays.domain = this.domain;
                    this.skiLiftFourDays.passType = new PassType();
                    this.skiLiftFourDays.passType.passTypeId = +this.passType;
                    this.skiLiftFourDays.price = this.priceFourDays;
                    this.skiLiftFourDays.ageRange = new AgeRange();
                    this.skiLiftFourDays.ageRange.ageRangeId = +this.ageRange;
                    this.skiLiftService.updateSkiLift(this.skiLiftFourDays).subscribe(
                      skiLiftFourDays => {
                        this.priceFiveDays = this.pricesToutSchussForm.get('priceFiveDays').value;
                        this.skiLiftFiveDays = new SkiLift();
                        this.skiLiftFiveDays.duration = '5';
                        this.skiLiftFiveDays.domain = this.domain;
                        this.skiLiftFiveDays.passType = new PassType();
                        this.skiLiftFiveDays.passType.passTypeId = +this.passType;
                        this.skiLiftFiveDays.price = this.priceFiveDays;
                        this.skiLiftFiveDays.ageRange = new AgeRange();
                        this.skiLiftFiveDays.ageRange.ageRangeId = +this.ageRange;
                        this.skiLiftService.updateSkiLift(this.skiLiftFiveDays).subscribe(
                          skiLiftFiveDays => {
                            this.priceSixDays = this.pricesToutSchussForm.get('priceSixDays').value;
                            this.skiLiftSixDays = new SkiLift();
                            this.skiLiftSixDays.duration = '6';
                            this.skiLiftSixDays.domain = this.domain;
                            this.skiLiftSixDays.passType = new PassType();
                            this.skiLiftSixDays.passType.passTypeId = +this.passType;
                            this.skiLiftSixDays.price = this.priceSixDays;
                            this.skiLiftSixDays.ageRange = new AgeRange();
                            this.skiLiftSixDays.ageRange.ageRangeId = +this.ageRange;
                            this.skiLiftService.updateSkiLift(this.skiLiftSixDays).subscribe(
                              skiLiftSixDays => {
                                this.priceSevenDays = this.pricesToutSchussForm.get('priceSevenDays').value;
                                this.skiLiftSevenDays = new SkiLift();
                                this.skiLiftSevenDays.duration = '7';
                                this.skiLiftSevenDays.domain = this.domain;
                                this.skiLiftSevenDays.passType = new PassType();
                                this.skiLiftSevenDays.passType.passTypeId = +this.passType;
                                this.skiLiftSevenDays.price = this.priceSevenDays;
                                this.skiLiftSevenDays.ageRange = new AgeRange();
                                this.skiLiftSevenDays.ageRange.ageRangeId = +this.ageRange;
                                this.skiLiftService.updateSkiLift(this.skiLiftSevenDays).subscribe(
                                  skiLiftSevenDays => {
                                    this.priceEightDays = this.pricesToutSchussForm.get('priceEightDays').value;
                                    this.skiLiftEightDays = new SkiLift();
                                    this.skiLiftEightDays.duration = '8';
                                    this.skiLiftEightDays.domain = this.domain;
                                    this.skiLiftEightDays.passType = new PassType();
                                    this.skiLiftEightDays.passType.passTypeId = +this.passType;
                                    this.skiLiftEightDays.price = this.priceEightDays;
                                    this.skiLiftEightDays.ageRange = new AgeRange();
                                    this.skiLiftEightDays.ageRange.ageRangeId = +this.ageRange;
                                    this.skiLiftService.updateSkiLift(this.skiLiftEightDays).subscribe(
                                      skiLiftEightDays => {
                                        this.priceSeason = this.pricesToutSchussForm.get('priceSeason').value;
                                        this.skiLiftSeason = new SkiLift();
                                        this.skiLiftSeason.duration = 'saison';
                                        this.skiLiftSeason.domain = this.domain;
                                        this.skiLiftSeason.passType = new PassType();
                                        this.skiLiftSeason.passType.passTypeId = +this.passType;
                                        this.skiLiftSeason.price = this.priceSeason;
                                        this.skiLiftSeason.ageRange = new AgeRange();
                                        this.skiLiftSeason.ageRange.ageRangeId = +this.ageRange;
                                        this.skiLiftService.updateSkiLift(this.skiLiftSeason).subscribe(
                                          skiLiftSeason => {

                                          }
                                        );
                                      }
                                    );
                                  }
                                );
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  changePricesNordique(pricesNordiqueFormValue: any) {
    this.priceHalfADay = this.pricesNordiqueForm.get('priceHalfADay').value;
    this.skiLiftHalfADay = new SkiLift();
    this.skiLiftHalfADay.duration = '1/2';
    this.skiLiftHalfADay.domain = '';
    this.skiLiftHalfADay.passType = new PassType();
    this.skiLiftHalfADay.passType.passTypeId = +this.passType;
    this.skiLiftHalfADay.price = this.priceHalfADay;
    this.skiLiftHalfADay.ageRange = new AgeRange();
    this.skiLiftHalfADay.ageRange.ageRangeId = +this.ageRange;
    this.skiLiftService.updateSkiLift(this.skiLiftHalfADay).subscribe(
      skiLiftHalfADay => {
        this.priceOneDay = this.pricesNordiqueForm.get('priceOneDay').value;
        this.skiLiftOneDay = new SkiLift();
        this.skiLiftOneDay.duration = '1';
        this.skiLiftOneDay.domain = '';
        this.skiLiftOneDay.passType = new PassType();
        this.skiLiftOneDay.passType.passTypeId = +this.passType;
        this.skiLiftOneDay.price = this.priceOneDay;
        this.skiLiftOneDay.ageRange = new AgeRange();
        this.skiLiftOneDay.ageRange.ageRangeId = +this.ageRange;
        this.skiLiftService.updateSkiLift(this.skiLiftOneDay).subscribe(
          skiLiftOneDay => {
            this.priceTwoDays = this.pricesNordiqueForm.get('priceTwoDays').value;
            this.skiLiftTwoDays = new SkiLift();
            this.skiLiftTwoDays.duration = '2';
            this.skiLiftTwoDays.domain = '';
            this.skiLiftTwoDays.passType = new PassType();
            this.skiLiftTwoDays.passType.passTypeId = +this.passType;
            this.skiLiftTwoDays.price = this.priceTwoDays;
            this.skiLiftTwoDays.ageRange = new AgeRange();
            this.skiLiftTwoDays.ageRange.ageRangeId = +this.ageRange;
            this.skiLiftService.updateSkiLift(this.skiLiftTwoDays).subscribe(
              skiLiftTwoDays => {
                this.priceThreeDays = this.pricesNordiqueForm.get('priceThreeDays').value;
                this.skiLiftThreeDays = new SkiLift();
                this.skiLiftThreeDays.duration = '3';
                this.skiLiftThreeDays.domain = '';
                this.skiLiftThreeDays.passType = new PassType();
                this.skiLiftThreeDays.passType.passTypeId = +this.passType;
                this.skiLiftThreeDays.price = this.priceThreeDays;
                this.skiLiftThreeDays.ageRange = new AgeRange();
                this.skiLiftThreeDays.ageRange.ageRangeId = +this.ageRange;
                this.skiLiftService.updateSkiLift(this.skiLiftThreeDays).subscribe(
                  skiLiftThreeDays => {
                    this.priceFourDays = this.pricesNordiqueForm.get('priceFourDays').value;
                    this.skiLiftFourDays = new SkiLift();
                    this.skiLiftFourDays.duration = '4';
                    this.skiLiftFourDays.domain = '';
                    this.skiLiftFourDays.passType = new PassType();
                    this.skiLiftFourDays.passType.passTypeId = +this.passType;
                    this.skiLiftFourDays.price = this.priceFourDays;
                    this.skiLiftFourDays.ageRange = new AgeRange();
                    this.skiLiftFourDays.ageRange.ageRangeId = +this.ageRange;
                    this.skiLiftService.updateSkiLift(this.skiLiftFourDays).subscribe(
                      skiLiftFourDays => {
                        this.priceFiveDays = this.pricesNordiqueForm.get('priceFiveDays').value;
                        this.skiLiftFiveDays = new SkiLift();
                        this.skiLiftFiveDays.duration = '5';
                        this.skiLiftFiveDays.domain = '';
                        this.skiLiftFiveDays.passType = new PassType();
                        this.skiLiftFiveDays.passType.passTypeId = +this.passType;
                        this.skiLiftFiveDays.price = this.priceFiveDays;
                        this.skiLiftFiveDays.ageRange = new AgeRange();
                        this.skiLiftFiveDays.ageRange.ageRangeId = +this.ageRange;
                        this.skiLiftService.updateSkiLift(this.skiLiftFiveDays).subscribe(
                          skiLiftFiveDays => {
                            this.priceSixDays = this.pricesNordiqueForm.get('priceSixDays').value;
                            this.skiLiftSixDays = new SkiLift();
                            this.skiLiftSixDays.duration = '6';
                            this.skiLiftSixDays.domain = '';
                            this.skiLiftSixDays.passType = new PassType();
                            this.skiLiftSixDays.passType.passTypeId = +this.passType;
                            this.skiLiftSixDays.price = this.priceSixDays;
                            this.skiLiftSixDays.ageRange = new AgeRange();
                            this.skiLiftSixDays.ageRange.ageRangeId = +this.ageRange;
                            this.skiLiftService.updateSkiLift(this.skiLiftSixDays).subscribe(
                              skiLiftSixDays => {
                                this.priceSevenDays = this.pricesNordiqueForm.get('priceSevenDays').value;
                                this.skiLiftSevenDays = new SkiLift();
                                this.skiLiftSevenDays.duration = '7';
                                this.skiLiftSevenDays.domain = '';
                                this.skiLiftSevenDays.passType = new PassType();
                                this.skiLiftSevenDays.passType.passTypeId = +this.passType;
                                this.skiLiftSevenDays.price = this.priceSevenDays;
                                this.skiLiftSevenDays.ageRange = new AgeRange();
                                this.skiLiftSevenDays.ageRange.ageRangeId = +this.ageRange;
                                this.skiLiftService.updateSkiLift(this.skiLiftSevenDays).subscribe(
                                  skiLiftSevenDays => {
                                    this.priceSeason = this.pricesNordiqueForm.get('priceSeason').value;
                                    this.skiLiftSeason = new SkiLift();
                                    this.skiLiftSeason.duration = 'saison';
                                    this.skiLiftSeason.domain = '';
                                    this.skiLiftSeason.passType = new PassType();
                                    this.skiLiftSeason.passType.passTypeId = +this.passType;
                                    this.skiLiftSeason.price = this.priceSeason;
                                    this.skiLiftSeason.ageRange = new AgeRange();
                                    this.skiLiftSeason.ageRange.ageRangeId = +this.ageRange;
                                    this.skiLiftService.updateSkiLift(this.skiLiftSeason).subscribe(
                                      skiLiftSeason => {

                                      }
                                    );
                                  }
                                );
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  goBack() {
    this.ShowFilter = true;
    this.ShowPrice = false;
  }

}
