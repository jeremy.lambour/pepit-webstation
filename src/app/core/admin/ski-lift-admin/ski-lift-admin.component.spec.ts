import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiLiftAdminComponent } from './ski-lift-admin.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SkiLiftService } from 'src/app/common/services/skiLift/skiLift.service';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { PassTypeService } from 'src/app/common/services/passType/passType.service';
import { AgeRangeService } from 'src/app/common/services/common/ageRange.service';

describe('SkiLiftAdminComponent', () => {
  let component: SkiLiftAdminComponent;
  let fixture: ComponentFixture<SkiLiftAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SkiLiftAdminComponent],
      providers: [SkiLiftService, SkiLiftReservationService, PassTypeService, AgeRangeService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiLiftAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
