import { FestivePeriod } from './../../../common/models/common/FestivePeriod';
import { FestivePeriodService } from './../../../common/services/common/festivePeriod.service';
import { Router } from '@angular/router';
import { SeasonService } from './../../../common/services/common/season.service';
import { Season } from './../../../common/models/common/Season';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/common/services/common/theme.service';

@Component({
  selector: 'app-season-admin',
  templateUrl: './season-admin.component.html',
  styleUrls: ['./season-admin.component.css']
})
export class SeasonAdminComponent implements OnInit {
  isChecked = true;
  formGroup: FormGroup;
  seasonForm: FormGroup;
  festivePeriodForm: FormGroup;
  seasonWinter: Season;
  seasonInterWinter: Season;
  seasonInterSummer: Season;
  seasonSummer: Season;
  loadingPage: Boolean;

  private router: Router;

  winterInterSeason: FestivePeriod;
  christmas: FestivePeriod;
  newYear: FestivePeriod;
  janFebWeek: FestivePeriod;
  febVacation: FestivePeriod;
  marchApril: FestivePeriod;
  easter: FestivePeriod;
  summerInterSeason: FestivePeriod;
  endJune: FestivePeriod;
  middleJuly: FestivePeriod;
  julyAugust: FestivePeriod;
  august: FestivePeriod;
  september: FestivePeriod;
  startWinterInterSeason: Date;
  endWinterInterSeason: Date;
  startChristmas: Date;
  endChristmas: Date;
  startNewYear: Date;
  endNewYear: Date;
  startJanFebWeek: Date;
  endJanFebWeek: Date;
  startFebVacation: Date;
  endFebVacation: Date;
  startMarchApril: Date;
  endMarchApril: Date;
  startEaster: Date;
  endEaster: Date;
  startSummerInterSeason: Date;
  endSummerInterSeason: Date;
  startEndJune: Date;
  endEndJune: Date;
  startMiddleJuly: Date;
  endMiddleJuly: Date;
  startJulyAugust: Date;
  endJulyAugust: Date;
  startAugust: Date;
  endAugust: Date;
  startSeptember: Date;
  endSeptember: Date;

  startSummerSeason: Date;
  endSummerSeason: Date;
  startInterSummerSeason: Date;
  endInterSummerSeason: Date;
  startWinterSeason: Date;
  endWinterSeason: Date;
  startInterWinterSeason: Date;
  endInterWinterSeason: Date;

  constructor(private formBuilder: FormBuilder, private seasonService: SeasonService, private _router: Router,
    private festivePeriodService: FestivePeriodService, private themeService: ThemeService) {
    this.router = _router;
    this.seasonForm = this.formBuilder.group({
      startSummerSeason: ['', Validators.required],
      endSummerSeason: ['', Validators.required],
      startInterSummerSeason: ['', Validators.required],
      endInterSummerSeason: ['', Validators.required],
      startWinterSeason: ['', Validators.required],
      endWinterSeason: ['', Validators.required],
      startInterWinterSeason: ['', Validators.required],
      endInterWinterSeason: ['', Validators.required]
    });
    this.festivePeriodForm = this.formBuilder.group({
      startWinterInterSeason: ['', Validators.required],
      endWinterInterSeason: ['', Validators.required],
      startChristmas: ['', Validators.required],
      endChristmas: ['', Validators.required],
      startNewYear: ['', Validators.required],
      endNewYear: ['', Validators.required],
      startJanFebWeek: ['', Validators.required],
      endJanFebWeek: ['', Validators.required],
      startFebVacation: ['', Validators.required],
      endFebVacation: ['', Validators.required],
      startMarchApril: ['', Validators.required],
      endMarchApril: ['', Validators.required],
      startEaster: ['', Validators.required],
      endEaster: ['', Validators.required],
      startSummerInterSeason: ['', Validators.required],
      endSummerInterSeason: ['', Validators.required],
      startEndJune: ['', Validators.required],
      endEndJune: ['', Validators.required],
      startMiddleJuly: ['', Validators.required],
      endMiddleJuly: ['', Validators.required],
      startJulyAugust: ['', Validators.required],
      endJulyAugust: ['', Validators.required],
      startAugust: ['', Validators.required],
      endAugust: ['', Validators.required],
      startSeptember: ['', Validators.required],
      endSeptember: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.loadingPage = false;
    this.getInterWinterSeason();

  }

  getInterWinterSeason() {
    this.seasonService.getSeason(1).subscribe(
      seasonInterWinter => {
        this.seasonInterWinter = seasonInterWinter;
        this.getWinterSeason();
      }
    );
  }

  getWinterSeason() {
    this.seasonService.getSeason(2).subscribe(
      seasonWinter => {
        this.seasonWinter = seasonWinter;
        this.getInterSummerSeason();
      }
    );
  }

  getInterSummerSeason() {
    this.seasonService.getSeason(3).subscribe(
      seasonInterSummer => {
        this.seasonInterSummer = seasonInterSummer;
        this.getSummerSeason();
      }
    );
  }

  getSummerSeason() {
    this.seasonService.getSeason(4).subscribe(
      seasonSummer => {
        this.seasonSummer = seasonSummer;
        if (this.seasonWinter.active === true) {
          this.formGroup = this.formBuilder.group({
            summer: false,
            winter: true
          });
        } else {
          this.formGroup = this.formBuilder.group({
            summer: true,
            winter: false
          });
        }
        this.getWinterInterSeason();
      }
    );
  }

  getWinterInterSeason() {
    this.festivePeriodService.getFestivePeriod(1).subscribe(
      winterInterSeason => {
        this.winterInterSeason = winterInterSeason;
        this.getChristmas();
      }
    );
  }

  getChristmas() {
    this.festivePeriodService.getFestivePeriod(2).subscribe(
      christmas => {
        this.christmas = christmas;
        this.getNewYear();
      }
    );
  }

  getNewYear() {
    this.festivePeriodService.getFestivePeriod(3).subscribe(
      newYear => {
        this.newYear = newYear;
        this.getJanFebWeek();
      }
    );
  }

  getJanFebWeek() {
    this.festivePeriodService.getFestivePeriod(4).subscribe(
      janFebWeek => {
        this.janFebWeek = janFebWeek;
        this.getFebVacation();
      }
    );
  }

  getFebVacation() {
    this.festivePeriodService.getFestivePeriod(5).subscribe(
      febVacation => {
        this.febVacation = febVacation;
        this.getMarchApril();
      }
    );
  }

  getMarchApril() {
    this.festivePeriodService.getFestivePeriod(6).subscribe(
      marchApril => {
        this.marchApril = marchApril;
        this.getEaster();
      }
    );
  }

  getEaster() {
    this.festivePeriodService.getFestivePeriod(7).subscribe(
      easter => {
        this.easter = easter;
        this.getSummerInterSeason();
      }
    );
  }

  getSummerInterSeason() {
    this.festivePeriodService.getFestivePeriod(8).subscribe(
      summerInterSeason => {
        this.summerInterSeason = summerInterSeason;
        this.getEndJune();
      }
    );
  }

  getEndJune() {
    this.festivePeriodService.getFestivePeriod(9).subscribe(
      endJune => {
        this.endJune = endJune;
        this.getMiddleJuly();
      }
    );
  }

  getMiddleJuly() {
    this.festivePeriodService.getFestivePeriod(10).subscribe(
      middleJuly => {
        this.middleJuly = middleJuly;
        this.getJulyAugust();
      }
    );
  }

  getJulyAugust() {
    this.festivePeriodService.getFestivePeriod(11).subscribe(
      julyAugust => {
        this.julyAugust = julyAugust;
        this.getAugust();
      }
    );
  }

  getAugust() {
    this.festivePeriodService.getFestivePeriod(12).subscribe(
      august => {
        this.august = august;
        this.getSeptember();
      }
    );
  }

  getSeptember() {
    this.festivePeriodService.getFestivePeriod(13).subscribe(
      september => {
        this.september = september;
        this.loadingPage = true;
      }
    );
  }

  onFormSubmit(formValue: any) {
    if (this.formGroup.get('winter').value === true) {
      this.seasonService.updateSeason(this.seasonWinter).subscribe(
        res => {
          this.themeService.checkSeason();
          this.router.navigate(['/home']);
        }
      );
    }

    if (this.formGroup.get('summer').value === true) {
      this.seasonService.updateSeason(this.seasonSummer).subscribe(
        res => {
          this.themeService.checkSeason();
          this.router.navigate(['/home']);
        }
      );
    }
  }

  updateSeasonDates(seasonFormValue: any) {
    this.startSummerSeason = this.seasonForm.get('startSummerSeason').value;
    this.endSummerSeason = this.seasonForm.get('endSummerSeason').value;
    this.seasonSummer.seasonBeginning = this.startSummerSeason;
    this.seasonSummer.seasonEnd = this.endSummerSeason;
    this.seasonService.updateDatesSeason(this.seasonSummer).subscribe(
      summerSeason => {
        this.startInterSummerSeason = this.seasonForm.get('startInterSummerSeason').value;
        this.endInterSummerSeason = this.seasonForm.get('endInterSummerSeason').value;
        this.seasonInterSummer.seasonBeginning = this.startInterSummerSeason;
        this.seasonInterSummer.seasonEnd = this.endInterSummerSeason;
        this.seasonService.updateDatesSeason(this.seasonInterSummer).subscribe(
          seasonInterSummer => {
            this.startWinterSeason = this.seasonForm.get('startWinterSeason').value;
            this.endWinterSeason = this.seasonForm.get('endWinterSeason').value;
            this.seasonWinter.seasonBeginning = this.startWinterSeason;
            this.seasonWinter.seasonEnd = this.endWinterSeason;
            this.seasonService.updateDatesSeason(this.seasonWinter).subscribe(
              seasonWinter => {
                this.startInterWinterSeason = this.seasonForm.get('startInterWinterSeason').value;
                this.endInterWinterSeason = this.seasonForm.get('endInterWinterSeason').value;
                this.seasonInterWinter.seasonBeginning = this.startInterWinterSeason;
                this.seasonInterWinter.seasonEnd = this.endInterWinterSeason;
                this.seasonService.updateDatesSeason(this.seasonInterWinter).subscribe(
                  seasonInterWinter => {
                    this.router.navigate(['/home']);
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  updateFestivePeriodDates() {
    this.startWinterInterSeason = this.festivePeriodForm.get('startWinterInterSeason').value;
    this.endWinterInterSeason = this.festivePeriodForm.get('endWinterInterSeason').value;
    this.winterInterSeason.festivePeriodBeginning = this.startWinterInterSeason;
    this.winterInterSeason.festivePeriodEnd = this.endWinterInterSeason;
    this.festivePeriodService.updateFestivePeriod(this.winterInterSeason).subscribe(
      winterInterSeason => {
        this.startChristmas = this.festivePeriodForm.get('startChristmas').value;
        this.endChristmas = this.festivePeriodForm.get('endChristmas').value;
        this.christmas.festivePeriodBeginning = this.startChristmas;
        this.christmas.festivePeriodEnd = this.endChristmas;
        this.festivePeriodService.updateFestivePeriod(this.christmas).subscribe(
          christmas => {
            this.startNewYear = this.festivePeriodForm.get('startNewYear').value;
            this.endNewYear = this.festivePeriodForm.get('endNewYear').value;
            this.newYear.festivePeriodBeginning = this.startNewYear;
            this.newYear.festivePeriodEnd = this.endNewYear;
            this.festivePeriodService.updateFestivePeriod(this.newYear).subscribe(
              newYear => {
                this.startJanFebWeek = this.festivePeriodForm.get('startJanFebWeek').value;
                this.endJanFebWeek = this.festivePeriodForm.get('endJanFebWeek').value;
                this.janFebWeek.festivePeriodBeginning = this.startJanFebWeek;
                this.janFebWeek.festivePeriodEnd = this.endJanFebWeek;
                this.festivePeriodService.updateFestivePeriod(this.janFebWeek).subscribe(
                  janFebWeek => {
                    this.startFebVacation = this.festivePeriodForm.get('startFebVacation').value;
                    this.endFebVacation = this.festivePeriodForm.get('endFebVacation').value;
                    this.febVacation.festivePeriodBeginning = this.startFebVacation;
                    this.febVacation.festivePeriodEnd = this.endFebVacation;
                    this.festivePeriodService.updateFestivePeriod(this.febVacation).subscribe(
                      febVacation => {
                        this.startMarchApril = this.festivePeriodForm.get('startMarchApril').value;
                        this.endMarchApril = this.festivePeriodForm.get('endMarchApril').value;
                        this.marchApril.festivePeriodBeginning = this.startMarchApril;
                        this.marchApril.festivePeriodEnd = this.endMarchApril;
                        this.festivePeriodService.updateFestivePeriod(this.marchApril).subscribe(
                          marchApril => {
                            this.startEaster = this.festivePeriodForm.get('startEaster').value;
                            this.endEaster = this.festivePeriodForm.get('endEaster').value;
                            this.easter.festivePeriodBeginning = this.startEaster;
                            this.easter.festivePeriodEnd = this.endEaster;
                            this.festivePeriodService.updateFestivePeriod(this.easter).subscribe(
                              easter => {
                                this.startSummerInterSeason = this.festivePeriodForm.get('startSummerInterSeason').value;
                                this.endSummerInterSeason = this.festivePeriodForm.get('endSummerInterSeason').value;
                                this.summerInterSeason.festivePeriodBeginning = this.startSummerInterSeason;
                                this.summerInterSeason.festivePeriodEnd = this.endSummerInterSeason;
                                this.festivePeriodService.updateFestivePeriod(this.summerInterSeason).subscribe(
                                  summerInterSeason => {
                                    this.startEndJune = this.festivePeriodForm.get('startEndJune').value;
                                    this.endEndJune = this.festivePeriodForm.get('endEndJune').value;
                                    this.endJune.festivePeriodBeginning = this.startEndJune;
                                    this.endJune.festivePeriodEnd = this.endEndJune;
                                    this.festivePeriodService.updateFestivePeriod(this.endJune).subscribe(
                                      endJune => {
                                        this.startMiddleJuly = this.festivePeriodForm.get('startMiddleJuly').value;
                                        this.startMiddleJuly = this.festivePeriodForm.get('endMiddleJuly').value;
                                        this.middleJuly.festivePeriodBeginning = this.startMiddleJuly;
                                        this.middleJuly.festivePeriodEnd = this.startMiddleJuly;
                                        this.festivePeriodService.updateFestivePeriod(this.middleJuly).subscribe(
                                          middleJuly => {
                                            this.startJulyAugust = this.festivePeriodForm.get('startJulyAugust').value;
                                            this.endJulyAugust = this.festivePeriodForm.get('endJulyAugust').value;
                                            this.julyAugust.festivePeriodBeginning = this.startJulyAugust;
                                            this.julyAugust.festivePeriodEnd = this.endJulyAugust;
                                            this.festivePeriodService.updateFestivePeriod(this.julyAugust).subscribe(
                                              julyAugust => {
                                                this.startAugust = this.festivePeriodForm.get('startAugust').value;
                                                this.endAugust = this.festivePeriodForm.get('endAugust').value;
                                                this.august.festivePeriodBeginning = this.startAugust;
                                                this.august.festivePeriodEnd = this.endAugust;
                                                this.festivePeriodService.updateFestivePeriod(this.august).subscribe(
                                                  august => {
                                                    this.startSeptember = this.festivePeriodForm.get('startSeptember').value;
                                                    this.endSeptember = this.festivePeriodForm.get('endSeptember').value;
                                                    this.september.festivePeriodBeginning = this.startSeptember;
                                                    this.september.festivePeriodEnd = this.endSeptember;
                                                    this.festivePeriodService.updateFestivePeriod(this.september).subscribe(
                                                      september => {
                                                        this.router.navigate(['/home']);
                                                      }
                                                    );
                                                  }
                                                );
                                              }
                                            );
                                          }
                                        );
                                      }
                                    );
                                  }
                                );
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );

  }

}
