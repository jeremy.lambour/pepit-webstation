import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonAdminComponent } from './season-admin.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ThemeService } from 'src/app/common/services/common/theme.service';
import { FestivePeriodService } from 'src/app/common/services/common/festivePeriod.service';
import { SeasonService } from 'src/app/common/services/common/season.service';
describe('SeasonAdminComponent', () => {
  let component: SeasonAdminComponent;
  let fixture: ComponentFixture<SeasonAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeasonAdminComponent],
      providers: [SeasonService, FestivePeriodService, ThemeService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
