import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentAdminComponent } from './equipment-admin.component';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('EquipmentAdminComponent', () => {
  let component: EquipmentAdminComponent;
  let fixture: ComponentFixture<EquipmentAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentAdminComponent],
      providers: [ShopServiceService, EquipmentServiceService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});

