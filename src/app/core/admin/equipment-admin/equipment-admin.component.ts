import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Shop } from 'src/app/common/models/shop/Shop';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';

@Component({
  selector: 'app-equipment-admin',
  templateUrl: './equipment-admin.component.html',
  styleUrls: ['./equipment-admin.component.css']
})
export class EquipmentAdminComponent implements OnInit {

  public loadingPage: Boolean;
  public formGroup: FormGroup;
  public listShops: Shop[] = [];
  private shopId: number;
  public loadingEquipments: Boolean;
  public listEquipments: Equipment[] = [];
  public equipmentsForm: FormGroup;
  public loadingPrices: Boolean;
  public pricesForm: FormGroup;
  public price: number;
  public equipment: Equipment;
  public equipmentId: number;
  public cmpt = 0;

  constructor(private formBuilder: FormBuilder, private shopService: ShopServiceService, public equipmentService: EquipmentServiceService) {
    this.formGroup = this.formBuilder.group({
      shop: ['', Validators.required]
    });
    this.equipmentsForm = this.formBuilder.group({
      equipment: ['', Validators.required]
    });
    this.pricesForm = this.formBuilder.group({
      price: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.loadingPage = false;
    this.loadingEquipments = false;
    this.loadingPrices = false;
    this.getHotels();
  }

  getHotels() {
    this.listShops.length = 0;
    this.shopService.getListShopToDisplay().subscribe(
      listShops => {
        this.listShops = listShops;
        this.loadingPage = true;
        this.cmpt = 0;
      }
    );
  }

  onFormSubmit(formValue: any) {
    this.listEquipments.length = 0;
    this.cmpt = 1;
    this.shopId = this.formGroup.get('shop').value;

    this.equipmentService.getListEquipmentByShop(this.shopId).subscribe(
      equipments => {
        equipments.forEach(element => {
          if (this.cmpt <= equipments.length) {
            this.listEquipments.push(element);
            this.cmpt = this.cmpt + 1;
          }
        });

        this.loadingEquipments = true;
      }
    );

  }

  selectEquipment(equipmentsFormValue: any) {
    this.equipmentId = this.equipmentsForm.get('equipment').value;
    this.equipmentService.getEquipment(this.equipmentId).subscribe(
      equipmentDto => {
        this.equipment = equipmentDto;
        this.price = this.equipment.priceLocation;
        this.loadingPrices = true;
      }
    );
  }

  changePrices(pricesFormValue: any) {
    this.price = this.pricesForm.get('price').value;
    this.equipment.priceLocation = this.price;
    this.equipmentService.updateEquipment(this.equipment).subscribe(
      equipment => {

      }
    );
  }

}
