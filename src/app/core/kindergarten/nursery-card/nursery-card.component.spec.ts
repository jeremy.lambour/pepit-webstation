import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseryCardComponent } from './nursery-card.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Nursery } from 'src/app/common/models/nursery/nursery';

describe('NurseryCardComponent', () => {
  let component: NurseryCardComponent;
  let fixture: ComponentFixture<NurseryCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NurseryCardComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseryCardComponent);
    component = fixture.componentInstance;
    component.nursery = new Nursery();
    component.nursery.nurseryId = 1;
    component.seasonId = '';
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
