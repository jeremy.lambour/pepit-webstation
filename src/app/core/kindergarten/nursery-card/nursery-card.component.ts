import { Component, OnInit, Input } from "@angular/core";
import { Nursery } from "src/app/common/models/nursery/nursery";
import { Router } from "@angular/router";

@Component({
  selector: "app-nursery-card",
  templateUrl: "./nursery-card.component.html",
  styleUrls: ["./nursery-card.component.css"]
})
export class NurseryCardComponent implements OnInit {
  @Input("nursery") nursery: Nursery;
  @Input("seasonId") seasonId: string;
  public router: Router;
  constructor(private _router: Router) {
    this.router = _router;
  }

  ngOnInit() {}

  toNurseryPlanning() {
    this.router.navigate(["/kindergartenPlanning/:seasonId/:nurseryId"], {
      queryParams: {
        nurseryId: this.nursery.nurseryId,
        seasonId: this.seasonId
      }
    });
  }
}
