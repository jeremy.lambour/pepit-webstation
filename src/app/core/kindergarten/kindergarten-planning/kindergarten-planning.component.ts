import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Season } from "src/app/common/models/common/Season";
import { NurseryPlanning } from "src/app/common/models/nursery/nurseryPlanning";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { NurseryPlanningService } from "src/app/common/services/nurseryPlanning/nursery-planning.service";

@Component({
  selector: "app-kindergarten-planning",
  templateUrl: "./kindergarten-planning.component.html",
  styleUrls: ["./kindergarten-planning.component.css"]
})
export class KindergartenPlanningComponent implements OnInit {
  private router: Router;
  private seasonId: string;
  private nurseryId: string;
  public nurseryPlannings: Array<NurseryPlanning>;
  private modalService: ModalService;
  private nurseryPlanningService: NurseryPlanningService;
  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _nurseryPlanningService: NurseryPlanningService
  ) {
    this.router = _router;
    this.modalService = _modalService;
    this.nurseryPlanningService = _nurseryPlanningService;
  }

  ngOnInit() {
    this.seasonId = this._activatedRoute.snapshot.queryParamMap.get("seasonId");
    this.nurseryId = this._activatedRoute.snapshot.queryParamMap.get(
      "nurseryId"
    );
    this.nurseryPlanningService
      .getAllNurseryPlanningByNurseryIdAndSeaonId(this.seasonId, this.nurseryId)
      .subscribe(result => {
        this.nurseryPlannings = result as Array<NurseryPlanning>;
      });
  }

  toCreateNurseryPlanning() {
    this.router.navigate(["/kinderGartenCreation/:seasonId/:nurseryId"], {
      queryParams: {
        seasonId: this.seasonId,
        nurseryId: this.nurseryId
      }
    });
  }

  open(id) {
    this.modalService.open(id);
  }

  deleteNurseryPlanning(nurseryPlanning: NurseryPlanning) {
    this.nurseryPlanningService
      .deleteNurseryPlanning(nurseryPlanning.nurseryPlanningId)
      .subscribe(result => {
        this.nurseryPlannings.splice(
          this.nurseryPlannings.indexOf(nurseryPlanning),
          1
        );
        this.modalService.closeCurrent();
      });
  }

  toUpdateNurseryPlanning(nurseryPlanninId: number) {
    this.router.navigate(
      ["/kinderGartenUpdate/:nurseryPlanningId/:seasonId/:nurseryId"],
      {
        queryParams: {
          nurseryPlanningId: nurseryPlanninId,
          seasonId: this.seasonId,
          nurseryId: this.nurseryId
        }
      }
    );
  }

  showPlanning(planningId: number) {
    this.router.navigate(["/kinderGartenPlanningView/:nurseryPlanningId"], {
      queryParams: {
        nurseryPlanningId: planningId
      }
    });
  }
}
