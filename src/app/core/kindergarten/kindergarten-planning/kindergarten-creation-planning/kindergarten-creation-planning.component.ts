import { Component, OnInit } from "@angular/core";
import { NurseryPlanning } from "src/app/common/models/nursery/nurseryPlanning";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Season } from "src/app/common/models/common/Season";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";
import { NurseryPlanningService } from "src/app/common/services/nurseryPlanning/nursery-planning.service";
import { NurseryService } from "src/app/common/services/nursery/nursery.service";
import { Nursery } from "src/app/common/models/nursery/nursery";
import { SeasonService } from "src/app/common/services/common/season.service";

@Component({
  selector: "app-kindergarten-creation-planning",
  templateUrl: "./kindergarten-creation-planning.component.html",
  styleUrls: ["./kindergarten-creation-planning.component.css"]
})
export class KindergartenCreationPlanningComponent implements OnInit {
  public planningForm: FormGroup;
  private createdPlanning: NurseryPlanning;
  private router: Router;
  public beginDate: NgbDateStruct;
  public endDate: NgbDateStruct;
  private currentSeason: Season;
  private seasonService: SeasonService;
  private nurseryPlanningService: NurseryPlanningService;
  private nurseryService: NurseryService;
  constructor(
    private builder: FormBuilder,
    private _planningService: NurseryPlanningService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    public _dateAdapter: NgbStringAdapter,
    private _seasonService: SeasonService,
    private _nurseryService: NurseryService
  ) {
    this.planningForm = builder.group({
      planningName: ["", Validators.required],
      planningDateStart: ["", Validators.required],
      planningDateEnd: ["", Validators.required]
    });
    this.router = _router;
    this.createdPlanning = new NurseryPlanning();
    this.seasonService = this._seasonService;
    this.nurseryPlanningService = _planningService;
    this.nurseryService = _nurseryService;
  }

  submitPlanning() {
    this.createdPlanning.nurseryPlanningName = this.planningForm.get(
      "planningName"
    ).value;
    this.createdPlanning.nurseryPlanningBeginning = new Date(
      this.planningForm.get("planningDateStart").value
    );
    this.createdPlanning.nurseryPlanningEnd = new Date(
      this.planningForm.get("planningDateEnd").value
    );
    this.createdPlanning.season = this.currentSeason;
    this.nurseryPlanningService
      .saveNurseryPlanning(this.createdPlanning)
      .subscribe(result => {
        this.router.navigate(["/kindergartenPlanning/:seasonId/:nurseryId"], {
          queryParams: {
            seasonId: this.currentSeason.seasonId,
            nurseryId: this.createdPlanning.nursery.nurseryId
          }
        });
      });
  }

  ngOnInit() {
    let seasonId = this._activatedRoute.snapshot.queryParamMap.get("seasonId");
    let nurseryId = this._activatedRoute.snapshot.queryParamMap.get(
      "nurseryId"
    );
    this.nurseryService.getNurseryById(nurseryId).subscribe(result => {
      this.createdPlanning.nursery = result as Nursery;
    });
    this.seasonService.getSeasonById(parseInt(seasonId)).subscribe(result => {
      this.currentSeason = result as Season;
      this.beginDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonBeginning)
      );
      this.endDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonEnd)
      );
    });
  }
}
