import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindergartenCreationPlanningComponent } from './kindergarten-creation-planning.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbStringAdapter } from 'src/app/common/services/common/ngbStringAdapter';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('KindergartenCreationPlanningComponent', () => {
  let component: KindergartenCreationPlanningComponent;
  let fixture: ComponentFixture<KindergartenCreationPlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KindergartenCreationPlanningComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule, NgbModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindergartenCreationPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
