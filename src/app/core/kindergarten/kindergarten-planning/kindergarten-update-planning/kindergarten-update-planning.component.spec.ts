import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindergartenUpdatePlanningComponent } from './kindergarten-update-planning.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('KindergartenUpdatePlanningComponent', () => {
  let component: KindergartenUpdatePlanningComponent;
  let fixture: ComponentFixture<KindergartenUpdatePlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KindergartenUpdatePlanningComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule, NgbModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindergartenUpdatePlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
