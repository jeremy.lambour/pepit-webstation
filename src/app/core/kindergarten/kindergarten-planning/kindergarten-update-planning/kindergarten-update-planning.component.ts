import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NurseryPlanning } from "src/app/common/models/nursery/nurseryPlanning";
import { Season } from "src/app/common/models/common/Season";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";
import { NurseryPlanningService } from "src/app/common/services/nurseryPlanning/nursery-planning.service";
import { Nursery } from "src/app/common/models/nursery/nursery";
import { NurseryService } from "src/app/common/services/nursery/nursery.service";
import { SeasonService } from "src/app/common/services/common/season.service";

@Component({
  selector: "app-kindergarten-update-planning",
  templateUrl: "./kindergarten-update-planning.component.html",
  styleUrls: ["./kindergarten-update-planning.component.css"]
})
export class KindergartenUpdatePlanningComponent implements OnInit {
  public planningForm: FormGroup;
  private NurseryplanningService: NurseryPlanningService;
  private router: Router;
  private editedPlanning: NurseryPlanning;
  private nurseryService: NurseryService;
  private currentSeason: Season;
  private seasonService: SeasonService;
  public beginDate: NgbDateStruct;
  public endDate: NgbDateStruct;
  constructor(
    private builder: FormBuilder,
    private _planningService: NurseryPlanningService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    public _dateAdapter: NgbStringAdapter,
    private _seasonService: SeasonService,
    private _nurseryService: NurseryService
  ) {
    this.planningForm = builder.group({
      planningName: ["", Validators.required],
      planningDateStart: ["", Validators.required],
      planningDateEnd: ["", Validators.required]
    });
    this.router = _router;
    this.NurseryplanningService = _planningService;
    this.editedPlanning = new NurseryPlanning();
    this.seasonService = _seasonService;
    this.nurseryService = _nurseryService;
  }

  submitUpdatePlanning() {
    if (this.isEditedDifferentFromInitialPlanning()) {
      this.editedPlanning.nurseryPlanningName = this.planningForm.get(
        "planningName"
      ).value;
      this.editedPlanning.nurseryPlanningBeginning = new Date(
        this.planningForm.get("planningDateStart").value
      );
      this.editedPlanning.nurseryPlanningEnd = new Date(
        this.planningForm.get("planningDateEnd").value
      );
      this.NurseryplanningService.updateNurseryPlanning(
        this.editedPlanning
      ).subscribe(result => {
        this.router.navigate(["/kindergartenPlanning/:seasonId/:nurseryId"], {
          queryParams: {
            seasonId: this.currentSeason.seasonId,
            nurseryId: this.editedPlanning.nursery.nurseryId
          }
        });
      });
    }
  }

  ngOnInit() {
    let id = this._activatedRoute.snapshot.queryParamMap.get(
      "nurseryPlanningId"
    );
    let seasonId = this._activatedRoute.snapshot.queryParamMap.get("seasonId");
    let nurseryId = this._activatedRoute.snapshot.queryParamMap.get(
      "nurseryId"
    );
    this.nurseryService.getNurseryById(nurseryId).subscribe(result => {
      this.editedPlanning.nursery = result as Nursery;
    });
    this.NurseryplanningService.getNurseryPlanningById(id).subscribe(result => {
      this.editedPlanning = result as NurseryPlanning;
      let dateStart = new Date(this.editedPlanning.nurseryPlanningBeginning);
      let dateEnd = new Date(this.editedPlanning.nurseryPlanningEnd);
      this.planningForm.setValue({
        planningName: this.editedPlanning.nurseryPlanningName,
        planningDateStart: new Date(dateStart),
        planningDateEnd: new Date(dateEnd)
      });
    });
    this.seasonService.getSeasonById(parseInt(seasonId)).subscribe(result => {
      this.currentSeason = result as Season;
      this.beginDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonBeginning)
      );
      this.endDate = this._dateAdapter.fromModel(
        new Date(this.currentSeason.seasonEnd)
      );
    });
  }
  isEditedDifferentFromInitialPlanning() {
    let isDifferent = false;
    let dateStart = new Date(this.editedPlanning.nurseryPlanningBeginning);
    let dateEnd = new Date(this.editedPlanning.nurseryPlanningEnd);
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningName").value !=
        this.editedPlanning.nurseryPlanningName;
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningDateStart").value.getTime() !=
        dateStart.getTime();
    isDifferent =
      isDifferent ||
      this.planningForm.get("planningDateEnd").value.getTime() !=
        dateEnd.getTime();
    return isDifferent;
  }
}
