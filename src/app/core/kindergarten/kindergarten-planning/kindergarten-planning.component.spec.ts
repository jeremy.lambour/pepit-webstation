import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindergartenPlanningComponent } from './kindergarten-planning.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('KindergartenPlanningComponent', () => {
  let component: KindergartenPlanningComponent;
  let fixture: ComponentFixture<KindergartenPlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KindergartenPlanningComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule, NgbModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindergartenPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
