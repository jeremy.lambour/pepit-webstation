import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindergartenPlanningSlotEditionModalComponent } from './kindergarten-planning-slot-edition-modal.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NurseryPlanningSlot } from 'src/app/common/models/nursery/nurseryPlanningSlot';
import { NurseryPlanning } from 'src/app/common/models/nursery/nurseryPlanning';
import { CalendarEvent } from 'calendar-utils';

describe('KindergartenPlanningSlotEditionModalComponent', () => {
  let component: KindergartenPlanningSlotEditionModalComponent;
  let fixture: ComponentFixture<KindergartenPlanningSlotEditionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KindergartenPlanningSlotEditionModalComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule,
        NgbModule.forRoot(), BrowserModule, BrowserDynamicTestingModule],
      providers: [NgbModal, NgbActiveModal],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindergartenPlanningSlotEditionModalComponent);
    component = fixture.componentInstance;
    component.editingPlanningSlot = new NurseryPlanningSlot();
    component.editingPlanningSlot.startTime = new Date();
    component.editingPlanningSlot.endTime = new Date();
    component.nurseryPlanning = new NurseryPlanning();
    component.planningSlots = Array<CalendarEvent>();
    component.minDate = new Date();
    component.maxDate = new Date();
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
