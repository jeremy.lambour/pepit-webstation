import { Component, OnInit, Input } from "@angular/core";
import {
  FormBuilder,
  AbstractControl,
  ValidatorFn,
  Validators,
  FormGroup
} from "@angular/forms";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";
import { CalendarEvent } from "calendar-utils";
import { NurseryPlanningSlot } from "src/app/common/models/nursery/nurseryPlanningSlot";
import { NurseryPlanning } from "src/app/common/models/nursery/nurseryPlanning";
import { NurseryPlanningSlotService } from "src/app/common/services/nurseryPlanningSlot/nursery-planning-slot.service";

@Component({
  selector: "app-kindergarten-planning-slot-edition-modal",
  templateUrl: "./kindergarten-planning-slot-edition-modal.component.html",
  styleUrls: ["./kindergarten-planning-slot-edition-modal.component.css"]
})
export class KindergartenPlanningSlotEditionModalComponent implements OnInit {
  @Input() editingPlanningSlot: NurseryPlanningSlot;
  @Input() nurseryPlanning: NurseryPlanning;
  @Input() planningSlots: Array<CalendarEvent>;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  public editingLessonForm: FormGroup;
  private modalService: ModalService;
  public editLesson: Boolean;
  private nurseryPlanningSlotService: NurseryPlanningSlotService;

  constructor(
    private _builder: FormBuilder,
    private _modalService: ModalService,
    private _nurseryPlanningSlot: NurseryPlanningSlotService,
    public activeModal: NgbActiveModal,
    public _dateFormatter: NgbStringAdapter
  ) {
    this.editingLessonForm = _builder.group({
      lessonDate: ["", Validators.required],
      lessonStartTime: ["", Validators.required],
      lessonEndTime: ["", Validators.required],
      lessonNumberPlaces: ["", Validators.required]
    });
    this.editingLessonForm
      .get("lessonEndTime")
      .setValidators([
        Validators.required,
        this.isAfterStartTime(
          this.editingLessonForm.get("lessonStartTime").value
        )
      ]);
    this.modalService = _modalService;
    this.editLesson = false;
    this.editingLessonForm.get("lessonDate").disable();
    this.editingLessonForm.get("lessonStartTime").disable();
    this.editingLessonForm.get("lessonEndTime").disable();
    this.editingLessonForm.get("lessonNumberPlaces").disable();

    this.nurseryPlanningSlotService = _nurseryPlanningSlot;
  }

  updateLesson() {
    let slot = new NurseryPlanningSlot();
    slot.nurseryPlanningSlotId = this.editingPlanningSlot.nurseryPlanningSlotId;
    slot.nurseryPlanning = this.editingPlanningSlot.nurseryPlanning;
    slot.date = this.editingLessonForm.controls["lessonDate"].value;
    slot.duration =
      parseInt(this.editingLessonForm.controls["lessonEndTime"].value) -
      parseInt(this.editingLessonForm.controls["lessonStartTime"].value);
    let startTime = this.editingLessonForm.controls[
      "lessonStartTime"
    ].value.split(":");
    slot.startTime = new Date(slot.date.getTime());
    slot.startTime.setHours(startTime[0], startTime[1]);
    let endTime = this.editingLessonForm.controls["lessonEndTime"].value.split(
      ":"
    );
    slot.endTime = new Date(slot.date.getTime());
    slot.endTime.setHours(endTime[0], endTime[1]);
    slot.nurserySlotNumberPlaces = this.editingLessonForm.controls[
      "lessonNumberPlaces"
    ].value;
    this.nurseryPlanningSlotService
      .updateNurseryPlanning(slot)
      .subscribe(result => {
        let updated = result as NurseryPlanningSlot;
        this.removeOldValues(updated);
        this.activeModal.close(updated);
      });
  }

  deleteLesson() {
    this.nurseryPlanningSlotService
      .deleteNurseryPlanning(this.editingPlanningSlot.nurseryPlanningSlotId)
      .subscribe(result => {
        if (result) {
          this.removeOldValues(result as NurseryPlanningSlot);
          this.activeModal.close();
        }
      });
  }
  changeState() {
    this.editLesson = !this.editLesson;
    this.editingLessonForm.get("lessonDate").disabled
      ? this.editingLessonForm.get("lessonDate").enable()
      : this.editingLessonForm.get("lessonDate").disable();
    this.editingLessonForm.get("lessonStartTime").disabled
      ? this.editingLessonForm.get("lessonStartTime").enable()
      : this.editingLessonForm.get("lessonStartTime").disable();
    this.editingLessonForm.get("lessonEndTime").disabled
      ? this.editingLessonForm.get("lessonEndTime").enable()
      : this.editingLessonForm.get("lessonEndTime").disable();
    this.editingLessonForm.get("lessonNumberPlaces").disabled
      ? this.editingLessonForm.get("lessonNumberPlaces").enable()
      : this.editingLessonForm.get("lessonNumberPlaces").disable();
  }
  ngOnInit() {
    this.editingLessonForm
      .get("lessonDate")
      .setValue(this.editingPlanningSlot.date);
    this.editingLessonForm
      .get("lessonStartTime")
      .setValue(this.editingPlanningSlot.startTime.toTimeString());
    this.editingLessonForm
      .get("lessonEndTime")
      .setValue(this.editingPlanningSlot.endTime.toTimeString());
    this.editingLessonForm
      .get("lessonNumberPlaces")
      .setValue(this.editingPlanningSlot.nurserySlotNumberPlaces);
  }

  dismiss() {
    console.log(this.editingLessonForm);
    this.modalService.closeCurrent();
  }

  removeOldValues(slot: NurseryPlanningSlot) {
    const replaceItemIndex = this.nurseryPlanning.nurseryPlanningSlots.findIndex(
      x => x.nurseryPlanningSlotId == slot.nurseryPlanningSlotId
    );
    this.nurseryPlanning.nurseryPlanningSlots.splice(replaceItemIndex, 1, slot);
    const itemIndex = this.planningSlots.findIndex(
      x => x.id == slot.nurseryPlanningSlotId
    );
    this.planningSlots.splice(itemIndex, 1);
  }

  isAfterStartTime(time: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = new Date(time) > new Date(control.value);
      return forbidden ? { forbiddenTime: { value: control.value } } : null;
    };
  }
}
