import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit
} from "@angular/core";
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  endOfWeek,
  startOfWeek,
  addWeeks,
  addMonths,
  subWeeks,
  subMonths,
  startOfMonth
} from "date-fns";
import { Subject } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarMonthViewDay
} from "angular-calendar";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { Action } from "rxjs/internal/scheduler/Action";
import { ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { NurseryPlanning } from "src/app/common/models/nursery/nurseryPlanning";
import { NurseryPlanningService } from "src/app/common/services/nurseryPlanning/nursery-planning.service";
import { KindergartenPlanningSlotCreationModalComponent } from "./kindergarten-planning-slot-creation-modal/kindergarten-planning-slot-creation-modal.component";
import { KindergartenPlanningSlotEditionModalComponent } from "./kindergarten-planning-slot-edition-modal/kindergarten-planning-slot-edition-modal.component";
import { NurseryPlanningSlot } from "src/app/common/models/nursery/nurseryPlanningSlot";

const colors: any = {
  red: {
    primary: "#ad2121",
    secondary: "#FAE3E3"
  },
  blue: {
    primary: "#1e90ff",
    secondary: "#D1E8FF"
  },
  yellow: {
    primary: "#e3bc08",
    secondary: "#FDF1BA"
  }
};

@Component({
  selector: "app-kindergarten-planning-view",
  templateUrl: "./kindergarten-planning-view.component.html",
  styleUrls: ["./kindergarten-planning-view.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KindergartenPlanningViewComponent implements OnInit {
  public view: CalendarView = CalendarView.Month;

  public CalendarView = CalendarView;

  public viewDate: Date;

  public currentPlanning: NurseryPlanning;

  private nurseryPlanningService: NurseryPlanningService;
  private modalService: ModalService;
  private createLesson: FormGroup;
  private minDate: Date;
  private maxDate: Date;
  public prevBtnDisabled: Boolean;
  public nextBtnDisabled: Boolean;

  refresh: Subject<any> = new Subject();

  nurseryPlanningSlots: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  constructor(
    private _modalService: ModalService,
    private _planningService: NurseryPlanningService,
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder
  ) {
    this.nurseryPlanningService = _planningService;
    this.modalService = _modalService;
  }
  addPeriod(period: CalendarView, date: Date, amount: number): Date {
    return {
      day: addDays,
      week: addWeeks,
      month: addMonths
    }[period](date, amount);
  }

  subPeriod(period: CalendarView, date: Date, amount: number): Date {
    return {
      day: subDays,
      week: subWeeks,
      month: subMonths
    }[period](date, amount);
  }

  startOfPeriod(period: CalendarView, date: Date): Date {
    return {
      day: startOfDay,
      week: startOfWeek,
      month: startOfMonth
    }[period](date);
  }

  endOfPeriod(period: CalendarView, date: Date): Date {
    return {
      day: endOfDay,
      week: endOfWeek,
      month: endOfMonth
    }[period](date);
  }

  previous(): void {
    this.changeDate(this.subPeriod(this.view, this.viewDate, 1));
  }

  next(): void {
    this.changeDate(this.addPeriod(this.view, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  changeDate(date: Date): void {
    this.viewDate = date;
    this.dateOrViewChanged();
  }
  changeView(view: CalendarView): void {
    this.view = view;
    this.dateOrViewChanged();
  }

  dateOrViewChanged(): void {
    this.prevBtnDisabled = !this.dateIsValid(
      this.endOfPeriod(this.view, this.subPeriod(this.view, this.viewDate, 1))
    );
    this.nextBtnDisabled = !this.dateIsValid(
      this.startOfPeriod(this.view, this.addPeriod(this.view, this.viewDate, 1))
    );
    if (this.viewDate < this.minDate) {
      this.changeDate(this.minDate);
    } else if (this.viewDate > this.maxDate) {
      this.changeDate(this.maxDate);
    }
  }
  createNewSlot(date: Date) {
    let modal = this.modalService.open(
      KindergartenPlanningSlotCreationModalComponent
    );
    modal.componentInstance.minDate = this.minDate;
    modal.componentInstance.maxDate = this.maxDate;
    modal.componentInstance.beginDate = date;
    modal.componentInstance.nurseryPlanning = this.currentPlanning;
    modal.result.then(nurserySlot => {
      if (nurserySlot) {
        nurserySlot.date = new Date(nurserySlot.date);
        nurserySlot.endTime = new Date(nurserySlot.endTime);
        nurserySlot.startTime = new Date(nurserySlot.startTime);
        this.addNewSlot(nurserySlot);
        this.currentPlanning.nurseryPlanningSlots.push(nurserySlot);
      }
    });
  }

  dayClicked({
    date,
    nurseryPlanningSlots
  }: {
    date: Date;
    nurseryPlanningSlots: CalendarEvent[];
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (isSameDay(this.viewDate, date) && nurseryPlanningSlots.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }
  addNewSlot(slot: NurseryPlanningSlot) {
    let newSlot = {
      id: slot.nurseryPlanningSlotId,
      start: new Date(slot.date.getTime() + slot.startTime.getTime()),
      end: new Date(slot.endTime.getTime() + slot.date.getTime()),
      title:
        "Créneau de garderie entre : " +
        new Date(slot.startTime.getTime()) +
        "et" +
        new Date(slot.endTime.getTime()),
      color: colors.red,
      allDay: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      },
      draggable: false
    };
    this.nurseryPlanningSlots.push(newSlot);
    this.refresh.next();
  }

  showDayDetails(): void {
    let modal = this.modalService.open(
      KindergartenPlanningSlotEditionModalComponent
    );
    modal.componentInstance.editingPlanningSlot = this.currentPlanning.nurseryPlanningSlots
      .filter(slot => slot.date.getTime() == this.viewDate.getTime())
      .shift();
    modal.componentInstance.minDate = this.minDate;
    modal.componentInstance.maxDate = this.maxDate;
    modal.componentInstance.nurseryPlanning = this.currentPlanning;
    modal.componentInstance.planningSlots = this.nurseryPlanningSlots;
    modal.result.then(nurserySlot => {
      if (nurserySlot) {
        nurserySlot.date = new Date(nurserySlot.date);
        nurserySlot.endTime = new Date(nurserySlot.endTime);
        nurserySlot.startTime = new Date(nurserySlot.startTime);
        this.addNewSlot(nurserySlot);
      }
    });
  }

  dateIsValid(date: Date): boolean {
    return date >= this.minDate && date <= this.maxDate;
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      if (!this.dateIsValid(day.date)) {
        day.cssClass = "cal-disabled-cell";
      }
    });
  }

  ngOnInit() {
    let id = this._activatedRoute.snapshot.queryParamMap.get(
      "nurseryPlanningId"
    );
    this.nurseryPlanningService.getNurseryPlanningById(id).subscribe(result => {
      this.currentPlanning = result as NurseryPlanning;
      this.minDate = new Date(this.currentPlanning.nurseryPlanningBeginning);
      this.viewDate = new Date(this.currentPlanning.nurseryPlanningBeginning);
      this.maxDate = new Date(this.currentPlanning.nurseryPlanningEnd);
      this.dateOrViewChanged();
      this.currentPlanning.nurseryPlanningSlots.map(
        slot => (slot.startTime = new Date(slot.startTime))
      );
      this.currentPlanning.nurseryPlanningSlots.map(
        slot => (slot.endTime = new Date(slot.endTime))
      );
      this.currentPlanning.nurseryPlanningSlots.map(
        slot => (slot.date = new Date(slot.date))
      );
      this.currentPlanning.nurseryPlanningSlots.map(slot =>
        this.addNewSlot(slot)
      );
    });
  }
}
