import { Component, OnInit, Input } from "@angular/core";
import { NurseryPlanning } from "src/app/common/models/nursery/nurseryPlanning";
import { NurseryPlanningService } from "src/app/common/services/nurseryPlanning/nursery-planning.service";
import { NurseryPlanningSlot } from "src/app/common/models/nursery/nurseryPlanningSlot";
import { NurseryPlanningSlotService } from "src/app/common/services/nurseryPlanningSlot/nursery-planning-slot.service";
import {
  FormBuilder,
  AbstractControl,
  ValidatorFn,
  FormGroup,
  Validators
} from "@angular/forms";
import { ModalService } from "src/app/common/services/common/modal/modal.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbStringAdapter } from "src/app/common/services/common/ngbStringAdapter";

@Component({
  selector: "app-kindergarten-planning-slot-creation-modal",
  templateUrl: "./kindergarten-planning-slot-creation-modal.component.html",
  styleUrls: ["./kindergarten-planning-slot-creation-modal.component.css"]
})
export class KindergartenPlanningSlotCreationModalComponent implements OnInit {
  @Input() beginDate: Date;
  @Input() nurseryPlanning: NurseryPlanning;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  public lessonCreationForm: FormGroup;
  public lessonStartTime: any;
  public lessonEndTime: any;
  private nurseryPlanningService: NurseryPlanningService;
  private nurseryPlanningSlotService: NurseryPlanningSlotService;

  constructor(
    private _builder: FormBuilder,
    private _modalService: ModalService,
    public activeModal: NgbActiveModal,
    private _planningService: NurseryPlanningService,
    private _nurseryPlanningSlotService: NurseryPlanningSlotService,
    public _dateFormatter: NgbStringAdapter
  ) {
    this.lessonCreationForm = _builder.group({
      lessonDate: ["", Validators.required],
      lessonStartTime: ["", Validators.required],
      lessonEndTime: ["", Validators.required],
      lessonNumberPlaces: ["", Validators.required]
    });
    this.lessonCreationForm
      .get("lessonEndTime")
      .setValidators([
        Validators.required,
        this.isAfterStartTime(
          this.lessonCreationForm.controls["lessonStartTime"].value
        )
      ]);
    this.nurseryPlanningService = _planningService;
    this.nurseryPlanningSlotService = _nurseryPlanningSlotService;
  }

  submitLesson() {
    let nurseryPlanningSlot = new NurseryPlanningSlot();
    console.log(this.lessonCreationForm);
    nurseryPlanningSlot.date = this.lessonCreationForm.controls[
      "lessonDate"
    ].value;
    nurseryPlanningSlot.duration =
      parseInt(this.lessonCreationForm.controls["lessonEndTime"].value) -
      parseInt(this.lessonCreationForm.controls["lessonStartTime"].value);
    let startTime = this.lessonCreationForm.controls[
      "lessonStartTime"
    ].value.split(":");
    nurseryPlanningSlot.startTime = new Date(
      nurseryPlanningSlot.date.getTime()
    );
    nurseryPlanningSlot.startTime.setHours(startTime[0], startTime[1]);
    let endTime = this.lessonCreationForm.controls["lessonEndTime"].value.split(
      ":"
    );
    nurseryPlanningSlot.endTime = new Date(nurseryPlanningSlot.date.getTime());
    nurseryPlanningSlot.endTime.setHours(endTime[0], endTime[1]);
    nurseryPlanningSlot.nurseryPlanning = this.nurseryPlanning.nurseryPlanningId;
    nurseryPlanningSlot.nurserySlotNumberPlaces = this.lessonCreationForm.controls[
      "lessonNumberPlaces"
    ].value;
    this.nurseryPlanningSlotService
      .saveNurseryPlanning(nurseryPlanningSlot)
      .subscribe(result => {
        this.activeModal.close(result);
      });
  }

  ngOnInit() {
    console.log(this.beginDate);
    this.lessonCreationForm.controls["lessonDate"].setValue(this.beginDate);
  }

  dismiss() {
    this._modalService.closeCurrent();
  }
  isAfterStartTime(time: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = new Date(time) > new Date(control.value);
      return forbidden ? { forbiddenTime: { value: control.value } } : null;
    };
  }
}
