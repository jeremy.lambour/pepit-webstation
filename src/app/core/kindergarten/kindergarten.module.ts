import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { KindergartenPlanningComponent } from "./kindergarten-planning/kindergarten-planning.component";
import { KindergartenCreationPlanningComponent } from "./kindergarten-planning/kindergarten-creation-planning/kindergarten-creation-planning.component";
import { KindergartenUpdatePlanningComponent } from "./kindergarten-planning/kindergarten-update-planning/kindergarten-update-planning.component";
import { KindergartenComponent } from "./kindergarten.component";
import { NurseryCardComponent } from "./nursery-card/nursery-card.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "../shared/shared.module";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { ContextMenuModule } from "ngx-contextmenu";
import { KindergartenPlanningViewComponent } from "./kindergarten-planning-view/kindergarten-planning-view.component";
import { KindergartenPlanningSlotCreationModalComponent } from "./kindergarten-planning-view/kindergarten-planning-slot-creation-modal/kindergarten-planning-slot-creation-modal.component";
import { KindergartenPlanningSlotEditionModalComponent } from "./kindergarten-planning-view/kindergarten-planning-slot-edition-modal/kindergarten-planning-slot-edition-modal.component";

@NgModule({
  declarations: [
    KindergartenPlanningComponent,
    KindergartenCreationPlanningComponent,
    KindergartenUpdatePlanningComponent,
    NurseryCardComponent,
    KindergartenComponent,
    KindergartenPlanningViewComponent,
    KindergartenPlanningSlotCreationModalComponent,
    KindergartenPlanningSlotEditionModalComponent
  ],
  entryComponents: [
    KindergartenPlanningSlotCreationModalComponent,
    KindergartenPlanningSlotEditionModalComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    SharedModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    })
  ]
})
export class KindergartenModule {}
