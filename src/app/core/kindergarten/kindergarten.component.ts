import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Nursery } from "src/app/common/models/nursery/nursery";
import { NurseryService } from "src/app/common/services/nursery/nursery.service";

@Component({
  selector: "app-kindergarten",
  templateUrl: "./kindergarten.component.html",
  styleUrls: ["./kindergarten.component.css"]
})
export class KindergartenComponent implements OnInit {
  public currentSeasonId: string;
  public nurseries: Array<Nursery>;
  private nurseryService: NurseryService;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _nurseryService: NurseryService
  ) {
    this.nurseryService = _nurseryService;
  }

  ngOnInit() {
    this.currentSeasonId = this._activatedRoute.snapshot.queryParamMap.get(
      "seasonId"
    );
    this.nurseryService.getAllNursery().subscribe(result => {
      this.nurseries = result as Array<Nursery>;
    });
  }
}
