import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild, AfterViewChecked, AfterViewInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { injectComponentFactoryResolver } from '@angular/core/src/render3';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { BingMapComponent } from 'src/app/core/common/bing-map/bing-map.component';
import { Shop } from 'src/app/common/models/shop/Shop';
import { Schedule } from 'src/app/common/models/common/Schedule';
import { Path } from 'src/app/common/models/common/Path';
import { ImageDTO } from 'src/app/common/models/common/ImageDTO';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { ShopScheduleService } from 'src/app/common/services/shop/shopSchedule.service';
import { BingMapsLoaderService } from 'src/app/common/services/common/bing-maps-loader.service';
import { ScheduleService } from 'src/app/common/services/common/schedule.service';
import { Router } from '@angular/router';
import { User } from 'src/app/common/models/User/User';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
    selector: 'app-shop-creation',
    templateUrl: './shop-creation.component.html',
    styleUrls: ['./shop-creation.component.css'],
    providers: [ShopServiceService]
})

export class ShopCreationComponent implements OnInit {

    private static adjustedWidth: number;
    private static loadingPhoto: boolean;
    private principal: User;
    public router: Router;
    @ViewChild(BingMapComponent) bingMapComponent: BingMapComponent;

    shopForm: FormGroup;
    submit: Boolean;
    loading: Boolean;


    private shop: Shop;
    public scheduleMonday: Schedule;
    public scheduleTuesday: Schedule;
    public scheduleWednesday: Schedule;
    public scheduleThursday: Schedule;
    public scheduleFriday: Schedule;
    public scheduleSaturday: Schedule;
    public scheduleSunday: Schedule;

    public urlCompo = ShopCreationComponent;
    private base64Image: SafeUrl;
    private base64: string;
    private fileImage: File;
    private p: Path;
    private image: ImageDTO;

    private txtSource: any;
    private txtDestination: any;
    public mapReady: boolean;

    constructor(public fb: FormBuilder, private shopService: ShopServiceService, private ref: ChangeDetectorRef,
        private ngZone: NgZone, private domSanitizer: DomSanitizer,
        private uploadService: UploadService, private bingMapService: BingMapsLoaderService,
        private scheduleService: ScheduleService, private shopScheduleService: ShopScheduleService,
        private _router: Router, private authentificationService: AuthentificationService) {
        this.router = _router;
        this.shopForm = fb.group({
            shop_name: ['', Validators.required],
            txtSource: ['', Validators.required],
            shop_tel: ['', Validators.required],
            schedule_monday_open_hour: ['', Validators.required],
            schedule_monday_close_hour: ['', Validators.required],
            schedule_tuesday_open_hour: ['', Validators.required],
            schedule_tuesday_close_hour: ['', Validators.required],
            schedule_wednesday_open_hour: ['', Validators.required],
            schedule_wednesday_close_hour: ['', Validators.required],
            schedule_thursday_open_hour: ['', Validators.required],
            schedule_thursday_close_hour: ['', Validators.required],
            schedule_friday_open_hour: ['', Validators.required],
            schedule_friday_close_hour: ['', Validators.required],
            schedule_saturday_open_hour: ['', Validators.required],
            schedule_saturday_close_hour: ['', Validators.required],
            schedule_sunday_open_hour: ['', Validators.required],
            schedule_sunday_close_hour: ['', Validators.required],
            shop_photos: ['']
        });
    }


    ngOnInit() {
        this.principal = JSON.parse(this.authentificationService.getUser());
        console.log(this.principal);
        this.mapReady = false;
        this.loading = false;
        ShopCreationComponent.loadingPhoto = false;
        ShopCreationComponent.adjustedWidth = 0;
        this.initMap();
    }


    calculDir(value) {
        this.txtSource = value.city1;
    }



    initMap() {
        this.bingMapService.load().then(res => {
            console.log('BingMapsLoader.load.then', res);
            this.mapReady = true;
        }, 2000);
    }


    submitRegistration(value: Object): void {
        console.log(value);

        this.shop = {
            shopId: null,
            shopName: this.shopForm.get('shop_name').value,
            shopAddress: this.bingMapComponent.placeSuggest1,
            phoneNumber: this.shopForm.get('shop_tel').value,
            shopLatitude: null,
            shopLongitude: null,
            userDTO: this.principal
        };

        this.scheduleMonday = {
            scheduleId: null,
            scheduleDay: 'Lundi',
            scheduleOpenHour: this.shopForm.get('schedule_monday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_monday_close_hour').value
        };

        this.scheduleTuesday = {
            scheduleId: null,
            scheduleDay: 'Mardi',
            scheduleOpenHour: this.shopForm.get('schedule_tuesday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_tuesday_close_hour').value
        };
        this.scheduleWednesday = {
            scheduleId: null,
            scheduleDay: 'Mercredi',
            scheduleOpenHour: this.shopForm.get('schedule_wednesday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_wednesday_close_hour').value
        };

        this.scheduleThursday = {
            scheduleId: null,
            scheduleDay: 'Jeudi',
            scheduleOpenHour: this.shopForm.get('schedule_thursday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_thursday_close_hour').value
        };

        this.scheduleFriday = {
            scheduleId: null,
            scheduleDay: 'Vendredi',
            scheduleOpenHour: this.shopForm.get('schedule_friday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_friday_close_hour').value
        };

        this.scheduleSaturday = {
            scheduleId: null,
            scheduleDay: 'Samedi',
            scheduleOpenHour: this.shopForm.get('schedule_saturday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_saturday_close_hour').value
        };

        this.scheduleSunday = {
            scheduleId: null,
            scheduleDay: 'Dimanche',
            scheduleOpenHour: this.shopForm.get('schedule_sunday_open_hour').value,
            scheduleCloseHour: this.shopForm.get('schedule_sunday_close_hour').value
        };


        if (this.fileImage !== undefined) {
            this.uploadService.UploadImgShop(this.fileImage).subscribe(x => {
                this.p = x;
                // insert la picture dans la table picture via this.p.path
            });
        }

        this.shopService.insertShop(this.shop).subscribe(res => {
            this.shop = res;
            this.scheduleService.insertSchedule(this.scheduleMonday).subscribe(
                result => {
                    this.scheduleMonday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleMonday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.scheduleService.insertSchedule(this.scheduleTuesday).subscribe(
                result => {
                    this.scheduleTuesday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleTuesday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.scheduleService.insertSchedule(this.scheduleWednesday).subscribe(
                result => {
                    this.scheduleWednesday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleWednesday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.scheduleService.insertSchedule(this.scheduleThursday).subscribe(
                result => {
                    this.scheduleThursday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleThursday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.scheduleService.insertSchedule(this.scheduleFriday).subscribe(
                result => {
                    this.scheduleFriday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleFriday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.scheduleService.insertSchedule(this.scheduleSaturday).subscribe(
                result => {
                    this.scheduleSaturday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleSaturday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.scheduleService.insertSchedule(this.scheduleSunday).subscribe(
                result => {
                    this.scheduleSunday = result;
                    this.shopScheduleService.insertShopSchedule(this.scheduleSunday.scheduleId, this.shop.shopId).subscribe();
                }
            );
            this.router.navigate(['/homestores']);
        });

    }

    setImg() {
        /*this.pictureService.setImgShop(this.fileImage).subscribe(res => {
            const bool = res;
        });*/
    }

    changeListener($event): void {
        this.readThis($event.target);
    }

    readThis(inputValue: any): void {
        ShopCreationComponent.loadingPhoto = false;
        this.fileImage = inputValue.files[0];
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
            /*this.base64Image = this.domSanitizer.bypassSecurityTrustUrl(myReader.result);
            const img = new Image();
            img.onload = function () {
                ShopCreationComponent.adjustedWidth = (img.width / img.height) * 150;
                ShopCreationComponent.loadingPhoto = true;
            };
            img.src = myReader.result;*/
        };
        myReader.readAsDataURL(this.fileImage);
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file: File = event.target.files[0];
            this.shopForm.get('photo').setValue(file);
        }
    }


}
