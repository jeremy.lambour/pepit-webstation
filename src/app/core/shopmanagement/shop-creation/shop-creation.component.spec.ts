import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
  flushMicrotasks
} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import {
  HttpClient,
  HttpHandler,
  HttpClientModule
} from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { ShopServiceService } from "src/app/common/services/shop/shop-service.service";
import { ShopScheduleService } from "src/app/common/services/shop/shopSchedule.service";
import { BingMapsLoaderService } from "src/app/common/services/common/bing-maps-loader.service";
import { ShopCreationComponent } from "./shop-creation.component";
import { UploadService } from "src/app/common/services/common/upload.service";
import { ScheduleService } from "src/app/common/services/common/schedule.service";
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

describe("ShopCreationComponent", () => {
  let component: ShopCreationComponent;
  let fixture: ComponentFixture<ShopCreationComponent>;
  let shopServiceService: ShopServiceService;
  let shopScheduleService: ShopScheduleService;
  let bingMapService: BingMapsLoaderService;
  let scheduleService: ScheduleService;
  let authentificationService: AuthentificationService;
  let uploadService: UploadService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShopCreationComponent],
      providers: [
        ShopServiceService,
        ShopScheduleService,
        BingMapsLoaderService,
        UploadService,
        AuthentificationService,
        ScheduleService,
        HttpClient
      ],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopCreationComponent);
    shopServiceService = TestBed.get(ShopServiceService);
    shopScheduleService = TestBed.get(ShopScheduleService);
    bingMapService = TestBed.get(BingMapsLoaderService);
    scheduleService = TestBed.get(ScheduleService);
    authentificationService = TestBed.get(AuthentificationService);
    uploadService = TestBed.get(UploadService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
