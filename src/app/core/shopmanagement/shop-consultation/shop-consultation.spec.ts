import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
  flushMicrotasks
} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import {
  HttpClient,
  HttpHandler,
  HttpClientModule
} from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { ShopServiceService } from "src/app/common/services/shop/shop-service.service";
import { ShopConsultationComponent } from "./shop-consultation.component";
import { ShopScheduleService } from "src/app/common/services/shop/shopSchedule.service";
import { BingMapsLoaderService } from "src/app/common/services/common/bing-maps-loader.service";

describe("ShopConsultationComponent", () => {
  let component: ShopConsultationComponent;
  let fixture: ComponentFixture<ShopConsultationComponent>;
  let shopServiceService: ShopServiceService;
  let shopScheduleService: ShopScheduleService;
  let bingMapService: BingMapsLoaderService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShopConsultationComponent],
      providers: [
        ShopServiceService,
        ShopScheduleService,
        BingMapsLoaderService,
        HttpClient
      ],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopConsultationComponent);
    shopServiceService = TestBed.get(ShopServiceService);
    shopScheduleService = TestBed.get(ShopScheduleService);
    bingMapService = TestBed.get(BingMapsLoaderService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
