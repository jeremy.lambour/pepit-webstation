import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { Shop } from 'src/app/common/models/shop/Shop';
import { Schedule } from 'src/app/common/models/common/Schedule';
import { ShopScheduleService } from 'src/app/common/services/shop/shopSchedule.service';
import { BingMapComponent } from 'src/app/core/common/bing-map/bing-map.component';
import { BingMapsLoaderService } from 'src/app/common/services/common/bing-maps-loader.service';

// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
    selector: 'app-shop-consultation',
    templateUrl: './shop-consultation.component.html',
    styleUrls: ['./shop-consultation.component.css'],
    providers: [ShopServiceService]
})
export class ShopConsultationComponent implements OnInit {

    public loadingConsultShop: Boolean;
    public idShop: number;
    public shop: Shop;
    public listSchedule: Schedule[] = [];
    private txtSource: any;
    private mapReady: boolean;

    @ViewChild(BingMapComponent) bingMapComponent: BingMapComponent;


    constructor(private shopService: ShopServiceService, private route: ActivatedRoute,
        private shopScheduleService: ShopScheduleService, private bingMapService: BingMapsLoaderService) {

    }


    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.idShop = +params.get('ShopId');
            this.getShop(this.idShop);
        });
        this.loadingConsultShop = false;
        this.initMap();
    }

    initMap() {
        this.bingMapService.load().then(res => {
            console.log('BingMapsLoader.load.then', res);
            this.mapReady = true;
        }, 2000);
    }

    getShop(id: number): void {
        this.shopService.getShop(id)
            .subscribe(
                res => {
                    this.shop = res;
                    this.shopScheduleService.getShopSchedule(id).subscribe(
                        result => {
                            this.listSchedule = result;
                            this.loadingConsultShop = true;
                        }
                    );
                }
            );
    }

    calculDir(value) {
        this.txtSource = value.city1;
    }
}
