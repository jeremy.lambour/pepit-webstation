import { SharedModule } from './../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { BingMapComponent } from '../../core/common/bing-map/bing-map.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShopCreationComponent } from './shop-creation/shop-creation.component';
import { ShopEditionComponent } from './shop-edition/shop-edition.component';
import { HomeStoresComponent } from './home-stores/home-stores.component';
import { ShopConsultationComponent } from './shop-consultation/shop-consultation.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BingMapModule } from '../common/bing-map/bing-map.module';

@NgModule({
    imports: [CommonModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        HttpClientModule,
        SharedModule,
        BingMapModule],
    declarations: [
        ShopCreationComponent,
        ShopEditionComponent,
        HomeStoresComponent,
        ShopConsultationComponent
    ]
})
export class ShopManagementModule { }
