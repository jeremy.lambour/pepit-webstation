import { Component, OnInit } from '@angular/core';
import { Shop } from 'src/app/common/models/shop/Shop';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { User } from 'src/app/common/models/User/User';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';
import { useAnimation } from '@angular/animations';

@Component({
  selector: 'app-home-stores',
  templateUrl: './home-stores.component.html',
  styleUrls: ['./home-stores.component.css']
})
export class HomeStoresComponent implements OnInit {

  public loadingHomeStores: boolean;
  public listShop: Shop[] = [];
  private user: User;
  private roleUser: string;

  constructor(private shopService: ShopServiceService, private authentificationService: AuthentificationService) { }

  ngOnInit() {
    this.loadingHomeStores = false;
    this.getListShopToDisplay();
    this.user = JSON.parse(this.authentificationService.getUser());
    this.roleUser = this.user.userRoleDto.authorityDtoName;
  }

  getListShopToDisplay(): void {
    this.shopService.getListShopToDisplay().subscribe(
      res => {
        this.listShop = res;
        this.loadingHomeStores = true;
      }
    );
  }
}
