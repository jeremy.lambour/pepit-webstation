import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
  flushMicrotasks
} from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import {
  HttpClient,
  HttpHandler,
  HttpClientModule
} from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { HomeStoresComponent } from "./home-stores.component";
import { ShopServiceService } from "src/app/common/services/shop/shop-service.service";

describe("HomeStoresComponent", () => {
  let component: HomeStoresComponent;
  let fixture: ComponentFixture<HomeStoresComponent>;
  let shopServiceService: ShopServiceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeStoresComponent],
      providers: [ShopServiceService, HttpClient],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeStoresComponent);
    shopServiceService = TestBed.get(ShopServiceService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
