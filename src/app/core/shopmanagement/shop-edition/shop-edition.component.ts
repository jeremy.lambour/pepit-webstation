import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { injectComponentFactoryResolver } from '@angular/core/src/render3';
import { ActivatedRoute, Router } from '@angular/router';
import { BingMapComponent } from 'src/app/core/common/bing-map/bing-map.component';
import { Shop } from 'src/app/common/models/shop/Shop';
import { Schedule } from 'src/app/common/models/common/Schedule';
import { Path } from 'src/app/common/models/common/Path';
import { ImageDTO } from 'src/app/common/models/common/ImageDTO';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { BingMapsLoaderService } from 'src/app/common/services/common/bing-maps-loader.service';
import { ScheduleService } from 'src/app/common/services/common/schedule.service';
import { ShopScheduleService } from 'src/app/common/services/shop/shopSchedule.service';
import { User } from 'src/app/common/models/User/User';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: 'app-shop-edition',
  templateUrl: './shop-edition.component.html',
  styleUrls: ['./shop-edition.component.css']
})
export class ShopEditionComponent implements OnInit {

  private static adjustedWidth: number;
  private static loadingPhoto: boolean;

  @ViewChild(BingMapComponent) bingMapComponent: BingMapComponent;

  shopForm: FormGroup;
  submit: Boolean;
  loading: Boolean;
  private principal: User;

  private shop: Shop;
  private schedule_monday: Schedule;
  private schedule_tuesday: Schedule;
  private schedule_wednesday: Schedule;
  private schedule_thursday: Schedule;
  private schedule_friday: Schedule;
  private schedule_saturday: Schedule;
  private schedule_sunday: Schedule;
  private listSchedule: Schedule[] = [];


  public scheduleMonday: Schedule;
  public scheduleTuesday: Schedule;
  public scheduleWednesday: Schedule;
  public scheduleThursday: Schedule;
  public scheduleFriday: Schedule;
  public scheduleSaturday: Schedule;
  public scheduleSunday: Schedule;

  public urlCompo = ShopEditionComponent;
  private base64Image: SafeUrl;
  private base64: string;
  private fileImage: File;
  private p: Path;
  private image: ImageDTO;

  private txtSource: any;
  private txtDestination: any;
  private mapReady: boolean;

  private idShop: number;
  private loadingShop: Boolean;

  public router: Router;

  constructor(public fb: FormBuilder, private shopService: ShopServiceService, private ref: ChangeDetectorRef,
    private ngZone: NgZone, private domSanitizer: DomSanitizer, private uploadService: UploadService,
    private bingMapService: BingMapsLoaderService, private route: ActivatedRoute, private shopScheduleService: ShopScheduleService,
    private scheduleService: ScheduleService, private _router: Router, private authentificationService: AuthentificationService) {
    this.router = _router;
    this.shopForm = fb.group({
      shop_name: ['', Validators.required],
      txtSource: ['', Validators.required],
      shop_tel: ['', Validators.required],
      schedule_monday_open_hour: ['', Validators.required],
      schedule_monday_close_hour: ['', Validators.required],
      schedule_tuesday_open_hour: ['', Validators.required],
      schedule_tuesday_close_hour: ['', Validators.required],
      schedule_wednesday_open_hour: ['', Validators.required],
      schedule_wednesday_close_hour: ['', Validators.required],
      schedule_thursday_open_hour: ['', Validators.required],
      schedule_thursday_close_hour: ['', Validators.required],
      schedule_friday_open_hour: ['', Validators.required],
      schedule_friday_close_hour: ['', Validators.required],
      schedule_saturday_open_hour: ['', Validators.required],
      schedule_saturday_close_hour: ['', Validators.required],
      schedule_sunday_open_hour: ['', Validators.required],
      schedule_sunday_close_hour: ['', Validators.required],
      shop_photos: ['']
    });
  }


  ngOnInit() {
    this.loadingShop = false;
    this.principal = JSON.parse(this.authentificationService.getUser());
    this.route.paramMap.subscribe(params => {
      this.idShop = +params.get('ShopId');
      this.getShop(this.idShop);
    });
    this.mapReady = false;
    this.loading = false;
    this.initMap();
    ShopEditionComponent.loadingPhoto = false;
    ShopEditionComponent.adjustedWidth = 0;
  }

  getShop(i: number): void {
    this.shopService.getShop(i)
      .subscribe(
        res => {
          this.shop = res;
          this.shopScheduleService.getShopSchedule(this.idShop).subscribe(
            result => {
              this.listSchedule = result;
              this.listSchedule.forEach(element => {
                if (element.scheduleDay === 'Lundi') {
                  this.schedule_monday = element;
                } else if (element.scheduleDay === 'Mardi') {
                  this.schedule_tuesday = element;
                } else if (element.scheduleDay === 'Mercredi') {
                  this.schedule_wednesday = element;
                } else if (element.scheduleDay === 'Jeudi') {
                  this.schedule_thursday = element;
                } else if (element.scheduleDay === 'Vendredi') {
                  this.schedule_friday = element;
                } else if (element.scheduleDay === 'Samedi') {
                  this.schedule_saturday = element;
                } else if (element.scheduleDay === 'Dimanche') {
                  this.schedule_sunday = element;
                }
              });
              this.loadingShop = true;
            }
          );
        }
      );
  }



  calculDir(value) {
    this.txtSource = value.city1;
  }



  initMap() {
    this.bingMapService.load().then(res => {
      console.log('BingMapsLoader.load.then', res);
      this.mapReady = true;
    });
  }


  submitRegistration(value: Object): void {
    console.log(value);
    this.shop = {
      shopId: this.idShop,
      shopName: this.shopForm.get('shop_name').value,
      shopAddress: this.bingMapComponent.placeSuggest1,
      phoneNumber: this.shopForm.get('shop_tel').value,
      shopLatitude: null,
      shopLongitude: null,
      userDTO: this.principal
    };

    this.scheduleMonday = {
      scheduleId: this.schedule_monday.scheduleId,
      scheduleDay: 'Lundi',
      scheduleOpenHour: this.shopForm.get('schedule_monday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_monday_close_hour').value
    };

    this.scheduleTuesday = {
      scheduleId: this.schedule_tuesday.scheduleId,
      scheduleDay: 'Mardi',
      scheduleOpenHour: this.shopForm.get('schedule_tuesday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_tuesday_close_hour').value
    };
    this.scheduleWednesday = {
      scheduleId: this.schedule_wednesday.scheduleId,
      scheduleDay: 'Mercredi',
      scheduleOpenHour: this.shopForm.get('schedule_wednesday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_wednesday_close_hour').value
    };

    this.scheduleThursday = {
      scheduleId: this.schedule_thursday.scheduleId,
      scheduleDay: 'Jeudi',
      scheduleOpenHour: this.shopForm.get('schedule_thursday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_thursday_close_hour').value
    };

    this.scheduleFriday = {
      scheduleId: this.schedule_friday.scheduleId,
      scheduleDay: 'Vendredi',
      scheduleOpenHour: this.shopForm.get('schedule_friday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_friday_close_hour').value
    };

    this.scheduleSaturday = {
      scheduleId: this.schedule_saturday.scheduleId,
      scheduleDay: 'Samedi',
      scheduleOpenHour: this.shopForm.get('schedule_saturday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_saturday_close_hour').value
    };

    this.scheduleSunday = {
      scheduleId: this.schedule_sunday.scheduleId,
      scheduleDay: 'Dimanche',
      scheduleOpenHour: this.shopForm.get('schedule_sunday_open_hour').value,
      scheduleCloseHour: this.shopForm.get('schedule_sunday_close_hour').value
    };

    if (this.fileImage !== undefined) {
      this.uploadService.UploadImgShop(this.fileImage).subscribe(x => {
        this.p = x;
        // insert la picture dans la table picture via this.p.path
      });
    }

    this.shopService.updateShop(this.shop).subscribe(res => {
      this.shop = res;
      this.scheduleService.updateSchedule(this.scheduleMonday).subscribe(
        result => {
          this.scheduleMonday = result;
        }
      );
      this.scheduleService.updateSchedule(this.scheduleTuesday).subscribe(
        result => {
          this.scheduleTuesday = result;
        }
      );
      this.scheduleService.updateSchedule(this.scheduleWednesday).subscribe(
        result => {
          this.scheduleWednesday = result;
        }
      );
      this.scheduleService.updateSchedule(this.scheduleThursday).subscribe(
        result => {
          this.scheduleThursday = result;
        }
      );
      this.scheduleService.updateSchedule(this.scheduleFriday).subscribe(
        result => {
          this.scheduleFriday = result;
        }
      );
      this.scheduleService.updateSchedule(this.scheduleSaturday).subscribe(
        result => {
          this.scheduleSaturday = result;
        }
      );
      this.scheduleService.updateSchedule(this.scheduleSunday).subscribe(
        result => {
          this.scheduleSunday = result;
        }
      );
      this.router.navigate(['/homestores']);
    });
  }

  setImg() {
    /*this.pictureService.setImgShop(this.fileImage).subscribe(res => {
        const bool = res;
    });*/
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    ShopEditionComponent.loadingPhoto = false;
    this.fileImage = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      /*this.base64Image = this.domSanitizer.bypassSecurityTrustUrl(myReader.result);
      const img = new Image();
      img.onload = function () {
          ShopCreationComponent.adjustedWidth = (img.width / img.height) * 150;
          ShopCreationComponent.loadingPhoto = true;
      };
      img.src = myReader.result;*/
    };
    myReader.readAsDataURL(this.fileImage);
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file: File = event.target.files[0];
      this.shopForm.get('photo').setValue(file);
    }
  }

  deleteShop() {
    this.shopScheduleService.deleteShopSchedule(this.idShop).subscribe(
      res => {
        this.shopService.deleteShop(this.idShop).subscribe(
          resShop => {
            this.scheduleService.deleteSchedule(this.schedule_monday.scheduleId).subscribe(
              resMonday => {
                this.scheduleService.deleteSchedule(this.schedule_tuesday.scheduleId).subscribe(
                  resTuesday => {
                    this.scheduleService.deleteSchedule(this.schedule_wednesday.scheduleId).subscribe(
                      resWednesday => {
                        this.scheduleService.deleteSchedule(this.schedule_thursday.scheduleId).subscribe(
                          resThursday => {
                            this.scheduleService.deleteSchedule(this.schedule_friday.scheduleId).subscribe(
                              resFriday => {
                                this.scheduleService.deleteSchedule(this.schedule_saturday.scheduleId).subscribe(
                                  resSaturday => {
                                    this.scheduleService.deleteSchedule(this.schedule_sunday.scheduleId).subscribe(
                                      resSunday => {
                                        this.router.navigate(['/homestores']);
                                      }
                                    );
                                  }
                                );
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
  }

}
