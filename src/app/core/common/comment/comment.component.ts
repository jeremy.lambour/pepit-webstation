import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { User } from 'src/app/common/models/User/User';
import { Comment } from 'src/app/common/models/common/comment/Comment';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
})
export class CommentComponent implements OnInit {
  public CommentForm: FormGroup;
  @Input() comments: Comment[] = [];
  public postComment: Comment;
  public user: User;
  public rating: number;
  private note: number;
  @Output() sendCommentEmit = new EventEmitter<Comment>();

  constructor(
    private authentificationService: AuthentificationService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.rating = 0;
    this.note = 0;
    this.initForm();
    this.initModel();
  }

  initForm() {
    this.CommentForm = this.fb.group({
      comment_txt: ['', Validators.required],
    });
  }

  private initModel(): void {
    this.user = JSON.parse(this.authentificationService.getUser());
  }

  sendComment() {
    this.postComment = {
      commentId: 0,
      content: this.CommentForm.get('comment_txt').value,
      from: this.user,
      note: this.note,
    };
    this.CommentForm.get('comment_txt').setValue('');
    this.comments.push(this.postComment);
    this.rating = 1;
    this.sendCommentEmit.emit(this.postComment);
  }

  ratingClick(event) {
    this.note = event.rating;
  }

  sortAccounts(prop: string) {
    const sorted = this.comments.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
    // asc/desc
    if (prop.charAt(0) === '-') { sorted.reverse(); }
    return sorted;
  }
}
