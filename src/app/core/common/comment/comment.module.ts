import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentComponent } from './comment.component';
import { CommentMessageComponent } from './comment_message/comment-message/comment-message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { PrintRatingComponent } from './print-rating/print-rating.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    CommentComponent,
    CommentMessageComponent,
    StarRatingComponent,
    PrintRatingComponent,
  ],
  exports: [CommentComponent],
})
export class CommentModule { }
