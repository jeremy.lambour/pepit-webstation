import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentMessageComponent } from './comment-message.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Comment } from 'src/app/common/models/common/comment/Comment';

const authority = {
  authorityDtoId: 1,
  authorityDtoName: 'user'
};

const userDto = {
  userId: 1,
  username: 'BobD',
  password: '1234',
  firstName: 'Bob',
  lastName: 'Dylan',
  birthDate: null,
  age: 35,
  phoneNumber: 1234567890,
  address: 'address',
  email: 'boby.dylan@gmail.com',
  city: 'Paris',
  postalCode: 95000,
  userRoleDto: authority
};


describe('CommentMessageComponent', () => {
  let component: CommentMessageComponent;
  let fixture: ComponentFixture<CommentMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentMessageComponent],
      providers: [CommentMessageComponent, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentMessageComponent);
    component = fixture.componentInstance;
    component.comment = new Comment();
    component.comment.commentId = 1;
    component.comment.content = '';
    component.comment.from = userDto;
    component.comment.note = 5;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
