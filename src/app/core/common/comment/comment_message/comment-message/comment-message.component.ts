import { Component, OnInit, Input } from '@angular/core';
import { Comment } from 'src/app/common/models/common/comment/Comment';

@Component({
  selector: 'app-comment-message',
  templateUrl: './comment-message.component.html',
  styleUrls: ['./comment-message.component.css'],
})
export class CommentMessageComponent implements OnInit {
  @Input() comment: Comment;
  public note: number;

  constructor() {}

  ngOnInit() {
    this.note = this.comment.note;
  }
}
