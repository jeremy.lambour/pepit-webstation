import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintRatingComponent } from './print-rating.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PrintRatingComponent', () => {
  let component: PrintRatingComponent;
  let fixture: ComponentFixture<PrintRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrintRatingComponent],
      providers: [PrintRatingComponent, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintRatingComponent);
    component = fixture.componentInstance;
    component.rating = 5;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
