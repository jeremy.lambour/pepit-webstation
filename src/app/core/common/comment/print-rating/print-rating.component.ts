import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-print-rating',
  templateUrl: './print-rating.component.html',
  styleUrls: ['./print-rating.component.css'],
})
export class PrintRatingComponent implements OnInit {
  @Input() rating: number;

  constructor() {}

  ngOnInit() {}
}
