import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BingMapComponent } from './bing-map.component';

@NgModule({
    imports: [CommonModule, NgbModule, FormsModule, ReactiveFormsModule, RouterModule],
    declarations: [
        BingMapComponent
    ],
    exports: [BingMapComponent]
})
export class BingMapModule { }
