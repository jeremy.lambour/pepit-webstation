import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import 'bingmaps';
import { Observable, Subject } from 'rxjs';
import { BingMapsLoaderService } from '../../../common/services/common/bing-maps-loader.service';

@Component({
  selector: 'app-bing-map',
  templateUrl: './bing-map.component.html',
  styleUrls: ['./bing-map.component.css']
})
export class BingMapComponent implements OnInit {
  private static map: Microsoft.Maps.Map;

  private static placeSuggest2: string;
  private static directionsManager: Microsoft.Maps.Directions.DirectionsManager;
  private static boolUpdate = new Subject<boolean>();
  private static placeSuggest1: string;
  private static locinput: string;
  @Output() eventCalculDir = new EventEmitter<any>();
  @Input() consult: boolean;
  @Input() city1: string;
  @Input() modif: boolean;
  @Input() location: string;

  private searchManager: any;

  constructor() {

    BingMapComponent.boolUpdate.next(false);
  }

  static autoSuggesttraceRoute() {
    const itineraryContainer = '';
    const map = BingMapComponent.map;

    let city1: string;

    city1 = BingMapComponent.placeSuggest1;
  }

  get placeSuggest1() { return BingMapComponent.placeSuggest1; }

  public static getSubject(): Observable<boolean> {
    return BingMapComponent.boolUpdate.asObservable();
  }

  ngOnInit() {

    if (typeof Microsoft !== 'undefined') {
      this.loadMap();
    }
    if (this.modif) {
      BingMapComponent.placeSuggest1 = this.city1;
      this.autoSuggest();
    } else if (this.consult) {
      BingMapComponent.locinput = this.location;
      /*this.geocodeQuery(this.location);*/
    } else {
      BingMapComponent.placeSuggest1 = '';
      this.autoSuggest();
    }
    this.checkRout();
  }

  checkRout() {
    BingMapComponent.getSubject().subscribe((response) => {
      if (response) {
        this.eventCalculDir.emit({
          city1: BingMapComponent.placeSuggest1,
        });
      }
    });
  }

  loadMap() {
    BingMapComponent.map = new Microsoft.Maps.Map(document.getElementById('mapId'), {
      credentials: 'Amu8hyyQNoJ07Vb9LRKOQcNsGNyqqTbE0i_LIamCVYxVdzWW8tlwUf2K_Waavl6k',
      showDashboard: false,
      showLocateMeButton: false,
    });
  }

  autoSuggest() {
    const map = BingMapComponent.map;
    // tslint:disable-next-line:prefer-const
    let suggestionSelected1 = this.suggestionSelected1;

    /* this.geocodeQuery(this.suggestionSelected1);*/

    Microsoft.Maps.loadModule('Microsoft.Maps.AutoSuggest', function () {
      const manager = new Microsoft.Maps.AutosuggestManager({ map: map });
      manager.attachAutosuggest('#txtSource', '#searchBoxContainer1', suggestionSelected1);
    });
  }

  suggestionSelected1(result) {
    if (BingMapComponent.map.entities.getLength() > 1) {
      BingMapComponent.map.entities.clear();
      BingMapComponent.placeSuggest1 = '';
    } else {
      BingMapComponent.placeSuggest1 = result.formattedSuggestion;
      const pin = new Microsoft.Maps.Pushpin(result.location);
      BingMapComponent.map.entities.push(pin);
      BingMapComponent.map.setView({ bounds: result.bestView });
    }

  }

  /*geocodeQuery(query: any) {
    if (!this.searchManager) {
      Microsoft.Maps.loadModule('Microsoft.Maps.Search', function () {
        this.searchManager = new Microsoft.Maps.Search.SearchManager(BingMapComponent.map);
        this.geocodeQuery(query);
      });
    } else {
      const searchRequest = {
        where: query,
        callback: function (r) {
          // Add the first result to the map and zoom into it.
          if (r && r.results && r.results.length > 0) {
            const pin = new Microsoft.Maps.Pushpin(r.results[0].location);
            BingMapComponent.map.entities.push(pin);

            BingMapComponent.map.setView({ bounds: r.results[0].bestView });
          }
        },
        errorCallback: function (e) {
          // If there is an error, alert the user about it.
          alert('No results found.');
        }
      };

      this.searchManager.geocode(searchRequest);
    }
  }*/

}
