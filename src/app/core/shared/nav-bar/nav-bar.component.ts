import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';
import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';

import { OcticonsDirective } from '../directives/octicons.directive';
import { User } from 'src/app/common/models/User/User';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  public roleUser: string;
  public isShow: boolean;
  public isConnect: boolean;
  public user: User;

  constructor(private authentificationService: AuthentificationService) { }

  ngOnInit() {
    this.checkConnect();
  }


  checkConnect() {
    this.authentificationService.getIsConnect().subscribe(next => {
      if (next) {
        this.user = JSON.parse(this.authentificationService.getUser());
        console.log(this.user);
        if (this.user !== undefined) {
          this.roleUser = this.user.userRoleDto.authorityDtoName;
        }
        this.isConnect = true;
      } else {
        this.isConnect = false;
      }
    });
  }

  account() {
    this.authentificationService.account();
  }

  logOut() {
    this.authentificationService.logOut();
  }

  connect() {
    this.isConnect = this.authentificationService.checkCredentials();
  }

}
