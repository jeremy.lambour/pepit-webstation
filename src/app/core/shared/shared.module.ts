import { NgModule } from "@angular/core";
import { OcticonsDirective } from "./directives/octicons.directive";
import { ChatModule } from "./chat/chat.module";

@NgModule({
  declarations: [OcticonsDirective],
  exports: [OcticonsDirective]
})
export class SharedModule {}
