import { ChatService } from './../../../common/services/chat/chat.service';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ViewChildren,
  ElementRef,
  QueryList,
  OnDestroy,
} from '@angular/core';
import { Action } from 'src/app/common/models/common/chat/action';
import { Message } from 'src/app/common/models/common/chat/message';
import { User } from 'src/app/common/models/User/User';
import { Event } from 'src/app/common/models/common/chat/event';
import { SocketService } from 'src/app/common/services/chat/socket/socket.service';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit, AfterViewInit {
  action = Action;
  user: User;
  messages: Message[] = [];
  messageContent: string;
  ioConnection: any;
  public ChatForm: FormGroup;
  public isOpen: boolean;
  public isCo: boolean;
  public isDisabled: boolean;
  public roleUser: String;
  private roomName: string;

  @ViewChild('bootsList') bootsList: ElementRef;

  @ViewChildren('bootsListChild') bootsListChild: QueryList<
    ChatMessageComponent
  >;

  constructor(
    private socketService: SocketService,
    private authentificationService: AuthentificationService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.isOpen = false;
    this.isCo = false;
    this.initModel();
    this.initForm();
    this.roleUser = this.user.userRoleDto.authorityDtoName;
    if (this.roleUser === 'ADMIN') {
      this.initIoConnection();
    }
  }

  ngAfterViewInit(): void {
    this.bootsListChild.changes.subscribe(elements => {
      this.scrollToBottom();
    });
  }

  initForm() {
    this.ChatForm = this.fb.group({
      chat_txt: ['', Validators.required],
    });

    this.ChatForm.get('chat_txt').disable();
    this.isDisabled = true;
  }

  private scrollToBottom(): void {
    try {
      this.bootsList.nativeElement.scrollTop = this.bootsList.nativeElement.scrollHeight;
    } catch (err) { }
  }

  private initModel(): void {
    this.user = JSON.parse(this.authentificationService.getUser());
    console.log(this.user);
  }

  get getAction() {
    return Action;
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService
      .onMessage()
      .subscribe((message: Message) => {
        console.log('message : ' + message.content);
        this.messages.push(message);
      });

    this.socketService.onCreateRoom().subscribe((message: Message) => {
      console.log('init : ' + message.roomName);
      this.messages.push(message);
    });

    this.socketService.onEvent(Event.CONNECT).subscribe(() => {
      this.socketService.sendState('AwaitingUser', this.user);
    });

    this.socketService.onEvent(Event.DISCONNECT).subscribe(() => {
      console.log('disconnected');
    });

    this.ChatForm.get('chat_txt').enable();
    this.isDisabled = false;
    this.isCo = true;
  }

  public sendMessage(message: string): void {
    if (!message) {
      return;
    }

    this.socketService.send({
      from: this.user,
      content: this.ChatForm.get('chat_txt').value,
    });
    this.ChatForm.get('chat_txt').setValue('');
  }

  openChat() {
    if (this.isOpen) {
      this.isOpen = false;
    } else {
      this.isOpen = true;
    }
  }

  refresh() {
    this.socketService.refresh();
  }

  logOut() {
    this.socketService.logOut();
    this.ChatForm.get('chat_txt').disable();
    this.isDisabled = true;
    this.isCo = false;
  }

}
