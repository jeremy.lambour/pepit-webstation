import { Component, OnInit, Input } from '@angular/core';
import { Message } from 'src/app/common/models/common/chat/message';
import { Action } from 'src/app/common/models/common/chat/action';

@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.css'],
})
export class ChatMessageComponent implements OnInit {
  action = Action;
  @Input() message: Message;
  constructor() {}

  ngOnInit() {
    console.log(this.message.content);
  }

  get getAction() {
    return this.action;
  }
}
