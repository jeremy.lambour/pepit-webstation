import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {
  public loginForm: FormGroup;
  public authentificationService: AuthentificationService;
  constructor(
    private _builder: FormBuilder,
    private _authentificationSerive: AuthentificationService,
    private _router: Router
  ) {
    this.loginForm = _builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.authentificationService = _authentificationSerive;
  }

  ngOnInit() { }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  login() {
    this.authentificationService
      .obtainAccessToken(
        this.loginForm.get('username').value,
        this.loginForm.get('password').value
      )
      .subscribe(
        data => {
          this.authentificationService.saveToken(data);
          this.authentificationService.findUser(this.loginForm.get('username').value).subscribe(
            response => {
              if (response) {
                this.authentificationService.setUser(response);
                this._router.navigate(['/home']);
              }
            });
        },
        err => {
          console.log('Erreur d\'authentification', err);
        }
      );
  }

  createUser() {
    this.authentificationService.createUser();
  }
}
