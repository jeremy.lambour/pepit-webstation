import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { REquipment } from 'src/app/common/models/reservation/rEquipment';
import { DatePipe } from '@angular/common';
import * as jsPDF from 'jspdf';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-customer-card',
  templateUrl: './customer-card.component.html',
  styleUrls: ['./customer-card.component.css']
})
export class CustomerCardComponent implements OnInit {

  private idUser: number;
  public listReservEquip: REquipment[] = [];
  public loadingReservations: boolean;
  public rEquipment: REquipment;
  public remis: Boolean;

  constructor(private equipmentService: EquipmentServiceService,
    private rEquipService: EquipmentReservationService, private route: ActivatedRoute, public datepipe: DatePipe) { }

  ngOnInit() {
    this.remis = false;
    this.loadingReservations = false;
    this.route.paramMap.subscribe(params => {
      this.idUser = +params.get('userId');
      this.getCustomer(this.idUser);
    });
  }

  getCustomer(idUser: number) {
    this.rEquipService.getReservationEquipmentUserDateAfterNow(idUser).subscribe(res => {
      this.listReservEquip = res;
      this.loadingReservations = true;
    });
  }

  download(idReserv: number) {
    this.rEquipService.findReservEquipmentByIdReserv(idReserv).subscribe(
      res => {
        this.rEquipment = res;
        const doc = new jsPDF();
        doc.text(20, 20, 'Pré-facture Magasin : ' + this.rEquipment.equipmentDto.shop.shopName);
        doc.text(20, 40, 'Nom Client : ' + this.rEquipment.reservationDto.user.firstName);
        doc.text(20, 50, 'Equipement Réservé : ' + this.rEquipment.equipmentDto.equipmentName);
        doc.text(20, 60, 'Taille Réservée : ' + this.rEquipment.equipmentDto.equipmentSize.toString());
        doc.text(20, 70, 'Etat du Produit : ' + this.rEquipment.equipmentDto.equipmentState);
        doc.text(20, 80, 'Date de Début : ' + this.datepipe.transform(this.rEquipment.dateStart, 'dd/MM/yyyy'));
        doc.text(20, 90, 'Date de Fin : ' + this.datepipe.transform(this.rEquipment.dateEnd, 'dd/MM/yyyy'));
        doc.text(20, 100, 'Quantité : ' + this.rEquipment.quantity.toString());
        doc.text(20, 110, 'Prix Unité : ' + this.rEquipment.equipmentDto.priceLocation.toString() + ' €');
        doc.text(20, 150, 'Prix Total : ' + this.rEquipment.price.toString() + ' €');
        doc.save('pre_facture.pdf');
      }
    );

  }

  give(equipment: Equipment, quantity: number, reserv: REquipment) {
    equipment.equipmentStock = equipment.equipmentStock - quantity;
    equipment.equipmentOut = equipment.equipmentOut + quantity;
    this.equipmentService.updateEquipment(equipment).subscribe(
      res => {
        this.rEquipService.updateIsGiven(true, reserv.equipmentDto.equipmentId,
          reserv.reservationDto.reservationId).subscribe(
            result => {
              this.getCustomer(this.idUser);
            }
          );
      }
    );
  }

}
