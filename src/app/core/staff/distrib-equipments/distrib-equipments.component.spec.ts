import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistribEquipmentsComponent } from './distrib-equipments.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Season } from 'src/app/common/models/common/Season';
import { GrdFilterPipe } from '../grd-filter-pipe';


describe('DistribEquipmentsComponent', () => {
  let component: DistribEquipmentsComponent;
  let fixture: ComponentFixture<DistribEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DistribEquipmentsComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      providers: [DatePipe, GrdFilterPipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistribEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {

    expect(component).toBeTruthy();
  });*/

});