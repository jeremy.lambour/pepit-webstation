import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { REquipment } from 'src/app/common/models/reservation/rEquipment';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { DatePipe } from '@angular/common';
import * as jsPDF from 'jspdf';


@Component({
  selector: 'app-equipments-to-prepare',
  templateUrl: './equipments-to-prepare.component.html',
  styleUrls: ['./equipments-to-prepare.component.css']
})
export class EquipmentsToPrepareComponent implements OnInit {

  public loadingPdfEquip: boolean;
  public reservEquip: REquipment;
  public date_temp: Date;
  public listReservEquip: Array<REquipment> = [];
  private dateStart: string;


  constructor(private equipmentReservationService: EquipmentReservationService,
    private route: ActivatedRoute,
    public datepipe: DatePipe) { }

  ngOnInit() {
    this.loadingPdfEquip = false;
    this.date_temp = new Date();
    this.dateStart = this.datepipe.transform(this.date_temp, 'yyyy-MM-dd');
    this.getListReservEquip();
  }

  getListReservEquip() {
    this.equipmentReservationService
      .getListReservByDay(this.dateStart)
      .subscribe(response => {
        this.listReservEquip = response;
        this.loadingPdfEquip = true;
      });
  }

  generate() {
    const doc = new jsPDF();
    let cmpt = 20;
    doc.text(20, 20, 'N°');
    doc.text(35, 20, 'Nom');
    doc.text(85, 20, 'Taille');
    doc.text(105, 20, 'Qté');
    doc.text(120, 20, 'N°client');
    doc.text(150, 20, 'Nom client');
    this.listReservEquip.forEach(element => {
      cmpt = cmpt + 5;
      doc.text(20, cmpt, element.equipmentDto.equipmentId.toString());
      doc.text(35, cmpt, element.equipmentDto.equipmentName);
      doc.text(85, cmpt, element.equipmentDto.equipmentSize);
      doc.text(105, cmpt, element.quantity.toString());
      doc.text(120, cmpt, element.reservationDto.user.userId.toString());
      doc.text(150, cmpt, element.reservationDto.user.firstName);
    });
    doc.save('liste_equipements.pdf');
  }

}
