import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/common/models/User/User';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-take-back-equipments',
  templateUrl: './take-back-equipments.component.html',
  styleUrls: ['./take-back-equipments.component.css']
})
export class TakeBackEquipmentsComponent implements OnInit {

  public searchText: string;
  public customerData: User[] = [];
  public data: any[] = [];
  public loadingCustomers: boolean;

  private user: User;

  constructor(private rEquipService: EquipmentReservationService, private authentificationService: AuthentificationService) { }

  ngOnInit() {
    this.loadingCustomers = false;
    this.getUser();
  }

  getUser() {
    this.user = JSON.parse(this.authentificationService.getUser());
    this.rEquipService.getClientsByShopManager(this.user).subscribe(res => {
      this.customerData = res;
      this.customerData.forEach(element => {
        this.data.push({ 'identifier': element.userId, 'name': element.firstName, 'email': element.email });
      });
      this.loadingCustomers = true;
    });
  }

}
