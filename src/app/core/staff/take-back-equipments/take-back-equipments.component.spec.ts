import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeBackEquipmentsComponent } from './take-back-equipments.component';


import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Season } from 'src/app/common/models/common/Season';
import { GrdFilterPipe } from '../grd-filter-pipe';


describe('TakeBackEquipmentsComponent', () => {
  let component: TakeBackEquipmentsComponent;
  let fixture: ComponentFixture<TakeBackEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TakeBackEquipmentsComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      providers: [DatePipe, GrdFilterPipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeBackEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {

    expect(component).toBeTruthy();
  });*/

});