import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.css']
})
export class HomeCardComponent implements OnInit {
  @Input() title: string;
  @Input() link: string;
  @Input() path: string;
  private router: Router;

  constructor(private _router: Router) {
    this.router = _router;
  }

  ngOnInit() {
  }

  click() {
    this.router.navigate([this.link]);
  }

}
