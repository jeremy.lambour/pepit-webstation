import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';
import { User } from 'src/app/common/models/User/User';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public roleUser: string;
  private user: User;
  private isConnect: boolean;

  constructor(private authentificationService: AuthentificationService) { }

  ngOnInit() {
    this.checkConnect();
  }

  checkConnect() {
    this.authentificationService.getIsConnect().subscribe(next => {
      if (next) {
        this.user = JSON.parse(this.authentificationService.getUser());
        this.roleUser = this.user.userRoleDto.authorityDtoName;
        console.log(this.user);
        this.isConnect = true;
      } else {
        this.isConnect = false;
      }
    });
  }

}
