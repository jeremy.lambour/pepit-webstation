import { HomeAdministrationComponent } from './../admin/home-administration/home-administration.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeCardComponent } from './home-card/home-card.component';
import { StaffHomeComponent } from '../staff/staff-home/staff-home.component';

@NgModule({
  declarations: [HomeComponent, HomeCardComponent, StaffHomeComponent, HomeAdministrationComponent],
  imports: [
    CommonModule
  ]
})
export class HomeModule { }
