import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateEquipmentReservationComponent } from './create/create-equipment-reservation.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../shared/shared.module';
import { ConsultReservEquipmentComponent } from './consult/consult-reserv-equipment.component';
import { ReservationCardEquipComponent } from './reservation-card-equip/reservation-card-equip.component';
import { ReservationLiftCardComponent } from './reservation-lift-card/reservation-lift-card.component';
import { ReservationCardFlatComponent } from './reservation-card-flat/reservation-card-flat.component';
import { EquipmentsToPrepareComponent } from '../../staff/equipments-to-prepare/equipments-to-prepare.component';
@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        HttpClientModule,
        SharedModule
    ],
    declarations: [
        CreateEquipmentReservationComponent,
        ReservationCardEquipComponent,
        ConsultReservEquipmentComponent,
        ReservationLiftCardComponent,
        ReservationCardFlatComponent,
        EquipmentsToPrepareComponent
    ]
})
export class EquipmentReservationModule { }
