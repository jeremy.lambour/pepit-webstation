import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ConsultReservEquipmentComponent } from './consult-reserv-equipment.component';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

describe('ConsultReservComponent', () => {
  let component: ConsultReservEquipmentComponent;
  let fixture: ComponentFixture<ConsultReservEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsultReservEquipmentComponent],
      providers: [AuthentificationService, EquipmentReservationService, SkiLiftReservationService, FlatReservationService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultReservEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
