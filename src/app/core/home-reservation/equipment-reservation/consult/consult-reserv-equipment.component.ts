import { Component, OnInit } from '@angular/core';
import { REquipment } from 'src/app/common/models/reservation/rEquipment';
import { RSkiLift } from 'src/app/common/models/reservation/rSkiLift';
import { User } from 'src/app/common/models/User/User';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-consult-reserv-equipment',
  templateUrl: './consult-reserv-equipment.component.html',
  styleUrls: ['./consult-reserv-equipment.component.css']
})
export class ConsultReservEquipmentComponent implements OnInit {
  public loadingListReservEquip: boolean;
  public loadingListReservFlat: boolean;
  public listReservEquip: Array<REquipment> = [];
  public listReservLift: Array<RSkiLift> = [];
  public listReservFlat: Array<RFlat> = [];
  public listReservHouse: String;
  private user: User;
  private authentificationService: AuthentificationService;
  private equipmentReservationService: EquipmentReservationService;
  private skiLiftReservationService: SkiLiftReservationService;
  private flatReservationService: FlatReservationService;

  constructor(
    private _authentificationService: AuthentificationService,
    private _equipmentReservationService: EquipmentReservationService,
    private _skiLiftReservationService: SkiLiftReservationService,
    private _flatReservationService: FlatReservationService
  ) {
    this.authentificationService = _authentificationService;
    this.equipmentReservationService = _equipmentReservationService;
    this.skiLiftReservationService = _skiLiftReservationService;
    this.flatReservationService = _flatReservationService;
  }

  ngOnInit() {
    this.loadingListReservEquip = false;
    this.loadingListReservFlat = false;
    this.getUser();
    this.getListReservEquip();
  }

  getUser() {
    this.user = JSON.parse(this.authentificationService.getUser());
  }

  getListReservEquip() {
    if (this.user !== null) {
      this.equipmentReservationService
        .getListReserv(this.user.userId)
        .subscribe(response => {
          this.listReservEquip = response;
          this.getListReservLift();
        });
    }
  }

  getListReservLift() {
    if (this.user !== null) {
      this.skiLiftReservationService.getLiftUser(this.user.userId).subscribe(
        response => {
          this.listReservLift = response;
          this.getReservationsFlatUser();
        }
      );
    }

  }

  getReservationsFlatUser(): void {
    if (this.user !== null) {
      this.flatReservationService.getReservationsFlatUser(this.user).subscribe(res => {
        this.listReservFlat = res;
        this.loadingListReservFlat = true;
      });
    }

  }
}
