import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { CreateEquipmentReservationComponent } from './create-equipment-reservation.component';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

describe('CreateEquipmentReservationComponent', () => {
  let component: CreateEquipmentReservationComponent;
  let fixture: ComponentFixture<CreateEquipmentReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateEquipmentReservationComponent],
      providers: [EquipmentReservationService, ShopServiceService, AuthentificationService, EquipmentServiceService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEquipmentReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
