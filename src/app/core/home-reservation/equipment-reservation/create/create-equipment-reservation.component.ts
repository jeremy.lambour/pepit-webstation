import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Equipment } from 'src/app/common/models/equipment/equipement';
import { REquipment } from 'src/app/common/models/reservation/rEquipment';
import { Reservation } from 'src/app/common/models/reservation/reservation';
import { Shop } from 'src/app/common/models/shop/Shop';
import { User } from 'src/app/common/models/User/User';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { ShopServiceService } from 'src/app/common/services/shop/shop-service.service';
import { EquipmentServiceService } from 'src/app/common/services/equipment/equipment-service.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-create-equipment-reservation',
  templateUrl: './create-equipment-reservation.component.html',
  styleUrls: ['./create-equipment-reservation.component.css']
})
export class CreateEquipmentReservationComponent implements OnInit {
  @Input() user: User;
  public registerReservEquip: FormGroup;
  public model: any = {};
  public loading = false;
  public loadingEquipment: boolean;
  public loadingUser: boolean;
  public equipmentName: String; // a changer
  public quantity: number; // a changer
  public price: number; // a changer
  public imgEquipPath: string;
  public equipment: Equipment;
  public reservation: Reservation;
  private rEquip: REquipment;
  private idEquipment: number;
  private dateStart: AbstractControl;
  private dateEnd: AbstractControl;
  private quantityForm: AbstractControl;
  public router: Router;
  public checked: Boolean = false;
  public cancellationOption: any;
  public priceVingtPourcent: any;
  public priceDifference: any;

  constructor(
    private equipmentReservService: EquipmentReservationService,
    private shopService: ShopServiceService,
    private route: ActivatedRoute,
    private authentificationService: AuthentificationService,
    private equipmentService: EquipmentServiceService,
    private _router: Router
  ) {
    this.router = _router;
  }

  ngOnInit() {
    this.loadingUser = false;
    this.loadingEquipment = false;
    this.equipmentName = 'nom equipement';
    this.price = 0;
    this.imgEquipPath =
      'https://www.decathlon.fr/media/839/8398648/big_1bad8b42-bbc3-4dc7-97be-fac02627cebb.jpg';
    this.getUser();
    this.route.paramMap.subscribe(params => {
      this.idEquipment = +params.get('EquipmentId');
      this.getEquipment(this.idEquipment);
    });
    this.initForm();
    this.computePrice();
  }

  changeValue(value) {
    this.checked = !value;
    if (this.checked === true) {
      this.priceVingtPourcent = this.priceVingtPourcent + this.cancellationOption;
    }
    if (this.checked === false) {
      this.priceVingtPourcent = (20 * this.price) / 100;
    }
  }

  getUser() {
    this.user = JSON.parse(this.authentificationService.getUser());
    this.loadingUser = true;
  }

  getEquipment(idEquipment: number) {
    this.equipmentService.getEquipment(idEquipment).subscribe(res => {
      this.equipment = res;
      this.quantity = this.equipment.equipmentStock - this.equipment.equipmentOut;
      this.loadingEquipment = true;
    });
  }

  initForm() {
    this.registerReservEquip = new FormGroup({
      dateStart: new FormControl('', Validators.required),
      dateEnd: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    if (this.registerReservEquip.valid) {
      console.log(this.registerReservEquip.get('dateStart').value);
      console.log(this.registerReservEquip.get('dateEnd').value);

      if (this.checked === false) {
        this.cancellationOption = 0;
      }

      this.reservation = {
        reservationId: 0,
        nbPersons: 1,
        insuranceLossMaterial: 150,
        houseCancelOpt: 0,
        equipmentCancelOpt: this.cancellationOption,
        reservCancelopt: 0,
        user: this.user
      };

      this.rEquip = {
        equipmentDto: this.equipment,
        reservationDto: this.reservation,
        dateStart: this.registerReservEquip.get('dateStart').value,
        dateEnd: this.registerReservEquip.get('dateEnd').value,
        quantity: this.registerReservEquip.get('quantity').value,
        price: this.price,
        isGiven: false
      };
      this.checkAvailability();
    }
  }

  checkAvailability() {
    this.equipmentReservService
      .checkAvailability(this.rEquip)
      .subscribe(response => {
        if (response) {
          console.log('Le produit est disponible à la date selectionnée !');
          this.insertRerservEquip();
        }
      });
  }

  insertRerservEquip() {
    this.equipmentReservService.create(this.rEquip).subscribe(response => {
      this.rEquip.equipmentDto.equipmentStock = this.rEquip.equipmentDto.equipmentStock - this.rEquip.quantity;
      this.rEquip.equipmentDto.equipmentOut = this.rEquip.quantity;
      this.equipmentService.updateEquipment(this.rEquip.equipmentDto).subscribe(
        res => {
          this.router.navigate(['/consultReservEquip/']);
        }
      );
    });
  }

  computePrice() {
    this.dateStart = this.registerReservEquip.controls['dateStart'];
    this.dateEnd = this.registerReservEquip.controls['dateEnd'];
    this.quantityForm = this.registerReservEquip.controls['quantity'];

    this.dateStart.valueChanges.subscribe(value => {
      if (value !== null && value !== undefined && this.dateEnd.valid && this.quantityForm.valid) {
        let dateStart: Date;
        let dateEnd: Date;
        let currentDate: Date;
        dateStart = new Date(value);
        dateEnd = new Date(this.dateEnd.value);

        currentDate = new Date();
        const diffDate = Math.abs(dateEnd.getDate() - dateStart.getFullYear());
        this.price = (diffDate * this.equipment.priceLocation) * this.quantityForm.value;
        this.priceVingtPourcent = (20 * this.price) / 100;
        this.priceDifference = this.price - this.priceVingtPourcent;
        this.cancellationOption = (10 * this.price) / 100;
      }
    });

    this.dateEnd.valueChanges.subscribe(value => {
      if (value !== null && value !== undefined && this.dateStart.valid && this.quantityForm.valid) {
        let dateStart: Date;
        let dateEnd: Date;
        let currentDate: Date;
        dateStart = new Date(value);
        dateEnd = new Date(this.dateStart.value);

        currentDate = new Date();
        const diffDate = Math.abs(dateEnd.getDate() - dateStart.getDate());
        this.price = (diffDate * this.equipment.priceLocation) * this.quantityForm.value;
        this.priceVingtPourcent = (20 * this.price) / 100;
        this.priceDifference = this.price - this.priceVingtPourcent;
        this.cancellationOption = (10 * this.price) / 100;
      }
    });


  }
}
