import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reservation-card-flat',
  templateUrl: './reservation-card-flat.component.html',
  styleUrls: ['./reservation-card-flat.component.css']
})
export class ReservationCardFlatComponent implements OnInit {
  @Input() rFlat: RFlat;
  constructor() { }

  ngOnInit() {
    console.log(this.rFlat);
  }

}
