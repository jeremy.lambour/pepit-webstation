import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ReservationCardFlatComponent } from './reservation-card-flat.component';
import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { Reservation } from 'src/app/common/models/reservation/reservation';

const authority = {
  authorityDtoId: 1,
  authorityDtoName: 'user'
};

const userDto = {
  userId: 1,
  username: 'BobD',
  password: '1234',
  firstName: 'Bob',
  lastName: 'Dylan',
  birthDate: new Date(),
  age: 35,
  phoneNumber: 1234567890,
  address: 'address',
  email: 'boby.dylan@gmail.com',
  city: 'Paris',
  postalCode: 95000,
  userRoleDto: authority
};

const hotelDto = {
  hotelId: 1,
  hotelName: 'hotelName',
  hotelRoomsNumber: 3,
  phoneNumber: 'phoneNumber',
  mailAddress: 'mailAddress',
  address: 'address',
  userDto: userDto
};

const flatCategoryDto = {
  flatCategoryId: 1,
  flatCategoryName: 'studio 3/4 personnes'
};

const flat = {
  flatId: 1, flatDescription: 'desc1', flatGuidance: 'guidance1', flatCapacity: 1, hotelDto: hotelDto,
  flatCategoryDto: flatCategoryDto
};

describe('ReservationCardFlatComponent', () => {
  let component: ReservationCardFlatComponent;
  let fixture: ComponentFixture<ReservationCardFlatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationCardFlatComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationCardFlatComponent);
    component = fixture.componentInstance;
    component.rFlat = new RFlat();
    component.rFlat.flatDto = flat;
    component.rFlat.reservationDto = new Reservation();
    component.rFlat.reservationDto.reservationId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

});
