import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { ReservationLiftCardComponent } from './reservation-lift-card.component';
import { RSkiLift } from 'src/app/common/models/reservation/rSkiLift';
import { DatePipe } from '@angular/common';

const passTypeDto = {
  passTypeId: 1,
  passTypeName: '1'
};

const ageRangeDto = {
  ageRangeId: 1,
  ageRangeName: '1'
};

const skiLiftDto = {
  skiLiftId: 1,
  duration: 'duration1',
  domain: 'domain1',
  price: 1,
  passType: passTypeDto,
  ageRange: ageRangeDto
};

describe('ReservationLiftCardComponent', () => {
  let component: ReservationLiftCardComponent;
  let fixture: ComponentFixture<ReservationLiftCardComponent>;
  let service: SkiLiftReservationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationLiftCardComponent],
      providers: [SkiLiftReservationService, HttpClient, DatePipe],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationLiftCardComponent);
    service = TestBed.get(SkiLiftReservationService);
    component = fixture.componentInstance;
    component.rskiLift = new RSkiLift();
    component.rskiLift.skiLiftDto = skiLiftDto;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
