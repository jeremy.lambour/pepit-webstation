import { Component, Input, OnInit } from '@angular/core';
import { RSkiLift } from 'src/app/common/models/reservation/rSkiLift';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import * as jsPDF from 'jspdf';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-reservation-lift-card',
  templateUrl: './reservation-lift-card.component.html',
  styleUrls: ['./reservation-lift-card.component.css']
})
export class ReservationLiftCardComponent implements OnInit {
  @Input() rskiLift: RSkiLift;
  private isDelete: boolean;

  private skiLiftReservationService: SkiLiftReservationService;
  constructor(private _skiLiftReservationService: SkiLiftReservationService, public datepipe: DatePipe) {
    this.skiLiftReservationService = _skiLiftReservationService;
  }

  ngOnInit() {
    console.log(this.rskiLift);
    this.isDelete = false;
  }

  deleteReserv() {
    this.skiLiftReservationService
      .deleteReservLift(this.rskiLift)
      .subscribe(response => {
        if (response) {
          this.isDelete = true;
        }
      });
  }

  download() {
    const doc = new jsPDF();
    doc.text(20, 20, 'Compte');
    doc.text(20, 25, this.rskiLift.userDto.firstName);
    doc.text(70, 20, 'Nom Ticket');
    doc.text(70, 25, this.rskiLift.nameReserv);
    doc.text(105, 20, 'Domaine');
    if (this.rskiLift.skiLiftDto.domain !== null) {
      doc.text(105, 25, this.rskiLift.skiLiftDto.domain);
    }
    doc.text(20, 35, 'Type');
    doc.text(20, 40, this.rskiLift.skiLiftDto.passType.passTypeName);
    doc.text(70, 35, 'Durée');
    doc.text(70, 40, this.rskiLift.skiLiftDto.duration + ' jours');
    doc.text(105, 35, 'Début');
    if (this.rskiLift.dateStart !== null) {
      doc.text(105, 40, this.datepipe.transform(this.rskiLift.dateStart, 'dd/MM/yyyy'));
    }

    doc.save('forfait_remontee_mecanique.pdf');
  }
}
