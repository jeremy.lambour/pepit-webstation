import { Component, Input, OnInit } from '@angular/core';
import { REquipment } from 'src/app/common/models/reservation/rEquipment';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reservation-card-equip',
  templateUrl: './reservation-card-equip.component.html',
  styleUrls: ['./reservation-card-equip.component.css']
})
export class ReservationCardEquipComponent implements OnInit {
  @Input() rEquip: REquipment;
  @Input() staff: boolean;
  dateStart: Date;
  dateEnd: Date;
  private isDelete: boolean;
  closeResult: string;
  public dateOK: boolean;
  constructor(
    private equipmentReservationService: EquipmentReservationService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

    console.log(this.rEquip);
    this.dateStart = new Date(this.rEquip.dateStart);
    this.dateEnd = new Date(this.rEquip.dateEnd);
    if (this.dateEnd > new Date()) {
      this.dateOK = true;
    }
    this.isDelete = false;
  }

  deleteReserv() {
    this.equipmentReservationService
      .deleteReservationEquipment(this.rEquip)
      .subscribe(response => {
        if (response) {
          this.isDelete = true;
        }
      });
  }

  open(content) {
    this.deleteReserv();
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
