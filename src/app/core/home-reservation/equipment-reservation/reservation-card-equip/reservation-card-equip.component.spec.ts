import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { EquipmentReservationService } from 'src/app/common/services/reservation/equipment-reservation.service';
import { ReservationCardEquipComponent } from './reservation-card-equip.component';
import { REquipment } from 'src/app/common/models/reservation/rEquipment';
import { Equipment } from 'src/app/common/models/equipment/equipement';

const authority = {
  authorityDtoId: 1,
  authorityDtoName: 'user'
};

const userDto = {
  userId: 1,
  username: 'BobD',
  password: '1234',
  firstName: 'Bob',
  lastName: 'Dylan',
  birthDate: null,
  age: 35,
  phoneNumber: 1234567890,
  address: 'address',
  email: 'boby.dylan@gmail.com',
  city: 'Paris',
  postalCode: 95000,
  userRoleDto: authority
};

const shopDto = {
  shopId: 1,
  shopName: 'shop1',
  shopAddress: 'address1',
  phoneNumber: '0123456789',
  shopLatitude: 56,
  shopLongitude: 59,
  userDTO: userDto
};

const equipment = {
  equipmentId: 1,
  equipmentName: 'equipment1',
  equipmentDescription: 'description1',
  equipmentState: 'state1',
  equipmentStock: 1,
  equipmentOut: 1,
  equipmentSize: 1,
  priceLocation: 1,
  shop: shopDto
};

describe('ReservationCardEquipComponent', () => {
  let component: ReservationCardEquipComponent;
  let fixture: ComponentFixture<ReservationCardEquipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationCardEquipComponent],
      providers: [EquipmentReservationService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationCardEquipComponent);
    component = fixture.componentInstance;
    component.rEquip = new REquipment();
    component.rEquip.equipmentDto = equipment;
    component.rEquip.dateStart = new Date();
    component.rEquip.dateEnd = new Date();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
