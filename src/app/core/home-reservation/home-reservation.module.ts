import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentReservationModule } from './equipment-reservation/equipment-reservation.module';
import { LiftReservationModuleModule } from './lift-reservation/lift-reservation-module.module';
import { HomeReservationComponent } from './home-reservation.component';

@NgModule({
  declarations: [HomeReservationComponent],
  imports: [
    CommonModule,
    EquipmentReservationModule,
    LiftReservationModuleModule,
  ]
})
export class HomeReservationModule { }
