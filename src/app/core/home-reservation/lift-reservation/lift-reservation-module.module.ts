import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LiftReservationComponent } from './consult/lift-reservation.component';
import { CreateLiftReservationComponent } from './create/create-lift-reservation.component';
import { LiftcardComponent } from './consult/liftcard/liftcard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LiftReservationComponent, CreateLiftReservationComponent, LiftcardComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LiftReservationModuleModule { }
