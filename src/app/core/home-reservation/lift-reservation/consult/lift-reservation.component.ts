import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lift-reservation',
  templateUrl: './lift-reservation.component.html',
  styleUrls: ['./lift-reservation.component.css']
})
export class LiftReservationComponent implements OnInit {
  public isAlpin: boolean;

  constructor() { }

  ngOnInit() {
    this.isAlpin = false;
  }

  clickAlpin() {
    this.isAlpin = true;
  }
}
