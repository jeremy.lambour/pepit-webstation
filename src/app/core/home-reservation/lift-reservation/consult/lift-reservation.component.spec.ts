import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { LiftReservationComponent } from './lift-reservation.component';

describe('LiftReservationComponent', () => {
  let component: LiftReservationComponent;
  let fixture: ComponentFixture<LiftReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LiftReservationComponent],
      providers: [HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiftReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
