import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liftcard',
  templateUrl: './liftcard.component.html',
  styleUrls: ['./liftcard.component.css']
})
export class LiftcardComponent implements OnInit {
  @Input() title: string;
  @Input() link: string;
  @Input() path: string;
  private router: Router;

  constructor(private _router: Router) {
    this.router = _router;
  }

  ngOnInit() {
  }

  click() {
    if (this.link !== '') {
      this.router.navigate([this.link]);
    }
  }
}
