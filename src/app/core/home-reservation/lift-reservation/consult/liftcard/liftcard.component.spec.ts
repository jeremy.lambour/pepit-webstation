import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { RSkiLift } from 'src/app/common/models/reservation/rSkiLift';
import { LiftcardComponent } from './liftcard.component';

const passTypeDto = {
  passTypeId: 1,
  passTypeName: '1'
};

const ageRangeDto = {
  ageRangeId: 1,
  ageRangeName: '1'
};

const skiLiftDto = {
  skiLiftId: 1,
  duration: 'duration1',
  domain: 'domain1',
  price: 1,
  passType: passTypeDto,
  ageRange: ageRangeDto
};

describe('LiftCardComponent', () => {
  let component: LiftcardComponent;
  let fixture: ComponentFixture<LiftcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LiftcardComponent],
      providers: [HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiftcardComponent);
    component = fixture.componentInstance;
    component.link = 'link';
    component.path = 'path';
    component.title = 'title';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
