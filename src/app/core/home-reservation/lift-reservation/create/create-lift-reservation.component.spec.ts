import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CreateLiftReservationComponent } from './create-lift-reservation.component';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

describe('CreateLiftReservationComponent', () => {
  let component: CreateLiftReservationComponent;
  let fixture: ComponentFixture<CreateLiftReservationComponent>;
  let skiLiftReservationService: SkiLiftReservationService;
  let authentificationService: AuthentificationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateLiftReservationComponent],
      providers: [SkiLiftReservationService, AuthentificationService, HttpClient],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLiftReservationComponent);
    skiLiftReservationService = TestBed.get(SkiLiftReservationService);
    authentificationService = TestBed.get(AuthentificationService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
