import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SkiLift } from 'src/app/common/models/equipment/skiLift';
import { RSkiLift } from 'src/app/common/models/reservation/rSkiLift';
import { AgeRange } from 'src/app/common/models/User/ageRange';
import { User } from 'src/app/common/models/User/User';
import { SkiLiftReservationService } from 'src/app/common/services/reservation/skilift-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-create-lift-reservation',
  templateUrl: './create-lift-reservation.component.html',
  styleUrls: ['./create-lift-reservation.component.css']
})
export class CreateLiftReservationComponent implements OnInit {
  public liftForm: FormGroup;
  public isAlpin: boolean;
  public nameDomain: string;
  public nameForfait: string;
  public reduc: string;
  public listSkilift: Array<SkiLift> = [];
  public loadingSkilift: boolean;
  public prix: number;
  private user: User;
  private isReduc: boolean;
  private age: number;
  private typeId: number;
  private dateInput: AbstractControl;
  private nbDayInput: AbstractControl;
  private ageRange: AgeRange;
  private skiLift: SkiLift;
  private rSkiLift: RSkiLift;
  public priceVingtPourcent: any;
  public priceDifference: any;
  private dateStart: AbstractControl;

  constructor(
    private route: ActivatedRoute,
    private skiLiftService: SkiLiftReservationService,
    private authentificationService: AuthentificationService
  ) { }

  ngOnInit() {
    this.rSkiLift = new RSkiLift();
    this.loadingSkilift = false;
    this.reduc = 'Aucune';
    this.isReduc = false;
    this.isAlpin = false;
    this.nameForfait = 'Ski Nordique';
    this.route.paramMap.subscribe(params => {
      this.typeId = +params.get('typeId');
    });
    this.getUser();
    this.getSkiLift();
    this.initForm();
    this.checkForfait();
    this.checkForm();
  }

  getUser() {
    this.user = JSON.parse(this.authentificationService.getUser());
  }

  getSkiLift() {
    this.skiLiftService.getAll().subscribe(response => {
      this.listSkilift = response;
      this.loadingSkilift = true;
    });
  }

  initForm() {
    this.liftForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      dateBirth: new FormControl('', Validators.required),
      nbDay: new FormControl('', Validators.required),
      dateStart: new FormControl('', Validators.required)
    });

    this.dateInput = this.liftForm.controls['dateBirth'];

    this.dateInput.valueChanges.subscribe(value => {
      if (value !== null && value !== undefined) {
        let date: Date;
        let currentDate: Date;
        date = new Date(value);
        currentDate = new Date();
        this.age = currentDate.getFullYear() - date.getFullYear();
        if (this.age > 0 && this.age < 5) {
          this.skiLiftService.getAgeRange(2).subscribe(response => {
            this.ageRange = response;
          });
        } else if (this.age > 5 && this.age < 15) {
          this.skiLiftService.getAgeRange(3).subscribe(response => {
            this.ageRange = response;
          });
        } else if (this.age > 15 && this.age < 65) {
          this.skiLiftService.getAgeRange(4).subscribe(response => {
            this.ageRange = response;
          });
          if (this.age > 18 && this.age < 25) {
            this.reduc = '10% (réduction étudiante)';
            this.isReduc = true;
          } else {
            this.reduc = 'Aucune';
            this.isReduc = false;
          }
        } else if (this.age > 65 && this.age < 75) {
          this.skiLiftService.getAgeRange(5).subscribe(response => {
            this.ageRange = response;
          });
        } else if (this.age > 75) {
          this.skiLiftService.getAgeRange(6).subscribe(response => {
            this.ageRange = response;
          });
        }
        if (this.liftForm.valid) {
          this.computePrice();
        }
      }
    });
  }

  checkForfait() {
    if (this.typeId === 1) {
      this.isAlpin = true;
      this.nameDomain = 'ToutSchuss';
      this.nameForfait = 'Ski Alpin';
    } else if (this.typeId === 2) {
      this.nameDomain = 'ToutSchuss Diamant';
      this.nameForfait = 'Ski Alpin';
      this.isAlpin = true;
    } else if (this.typeId === 3) {
      this.nameDomain = null;
      this.nameForfait = 'Ski Nordique';
      this.isAlpin = false;
    }
  }

  checkForm() {
    this.nbDayInput = this.liftForm.controls['nbDay'];
    this.nbDayInput.valueChanges.subscribe(response => {
      this.computePrice();
    });
  }

  computePrice() {
    for (const skilift of this.listSkilift) {
      if (this.nameDomain === 'ToutSchuss') {
        this.calculPrice('ToutSchuss', skilift);
      }
      if (this.nameDomain === 'ToutSchuss Diamant') {
        this.calculPrice('ToutSchuss Diamant', skilift);
      }
      if (this.nameDomain === null) {
        this.calculPrice(null, skilift);
      }
    }
  }

  calculPrice(nomDomain: string, skilift: SkiLift) {
    if (this.nameDomain === nomDomain) {
      if (skilift.domain === nomDomain) {
        if (skilift.ageRange.ageRangeId === this.ageRange.ageRangeId) {
          if (skilift.duration === this.liftForm.get('nbDay').value) {
            this.skiLift = skilift;
            if (this.isReduc) {
              this.prix = (skilift.price + 2) * 0.9;
              this.priceVingtPourcent = (20 * this.prix) / 100;
              this.priceDifference = this.prix - this.priceVingtPourcent;
            } else {
              this.prix = skilift.price + 2;
              this.priceVingtPourcent = (20 * this.prix) / 100;
              this.priceDifference = this.prix - this.priceVingtPourcent;
            }
          }
        }
      }
    }
  }

  onSubmit() {
    if (this.liftForm.valid) {
      this.rSkiLift.skiLiftDto = this.skiLift;
      this.rSkiLift.userDto = this.user;
      this.rSkiLift.nameReserv = this.liftForm.get('firstname').value;
      this.rSkiLift.insuranceSnow = 0;
      this.rSkiLift.caution = 2;
      this.rSkiLift.price = this.prix;
      this.rSkiLift.dateStart = this.liftForm.get('dateStart').value;
      this.rSkiLift.dateEnd = this.liftForm.get('dateStart').value;

      this.skiLiftService.create(this.rSkiLift).subscribe(response => {
        if (response) {
          this.skiLiftService.goHome();
          console.log('response');
          console.log('insert ok');
        }
      });
    }
  }
}
