import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/common/models/User/User';
import { RegisterService } from 'src/app/common/services/accountRegister/register.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, AfterViewChecked {
  public user: User;
  public accountForm: FormGroup;
  public formChange: boolean;
  private dateInput: AbstractControl;
  constructor(
    private authentificationService: AuthentificationService,
    private accountService: RegisterService
  ) { }

  ngOnInit() {
    this.formChange = false;
    this.getUser();
    this.initForm();
  }

  ngAfterViewChecked() {
    this.accountForm.valueChanges.subscribe(value => {
      this.formChange = true;
    });
  }

  getUser() {
    this.user = JSON.parse(this.authentificationService.getUser());
  }

  initForm() {
    this.accountForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      dateBirth: new FormControl('', Validators.required),
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ),
      street: new FormControl('', Validators.required),
      PhoneNumber: new FormControl('', Validators.required),
      postalCode: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required)
    });

    this.dateInput = this.accountForm.controls['dateBirth'];

    this.dateInput.valueChanges.subscribe(value => {
      if (value !== null && value !== undefined) {
        let date: Date;
        let currentDate: Date;
        date = new Date(value);
        currentDate = new Date();
        const diffDate = currentDate.getFullYear() - date.getFullYear();
        if (diffDate > 0 && diffDate > 18) {
          this.user.age = diffDate;
          this.formChange = true;
        }
      }
    });
  }

  onSubmit() {
    if (this.accountForm.valid) {
      console.log(this.accountForm.get('firstname').value);
      this.user.firstName = this.accountForm.get('firstname').value;
      this.user.lastName = this.accountForm.get('lastname').value;
      this.user.birthDate = this.accountForm.get('dateBirth').value;
      this.user.email = this.accountForm.get('email').value;
      this.user.address = this.accountForm.get('street').value;
      this.user.phoneNumber = this.accountForm.get('PhoneNumber').value;
      this.user.postalCode = this.accountForm.get('postalCode').value;
      this.user.city = this.accountForm.get('city').value;

      if (this.user !== null) {
        this.accountService.updateAccount(this.user).subscribe(response => {
          if (response) {
            this.formChange = false;
            this.authentificationService.setUser(this.user);

          }
        });
      }
    }
  }
}
