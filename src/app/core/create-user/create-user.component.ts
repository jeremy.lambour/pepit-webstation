import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/common/models/User/User';

import { Authority } from '../../common/models/User/authority';
import { RegisterService } from '../../common/services/accountRegister/register.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  registerUser: FormGroup;
  model: any = {};
  loading = false;
  user: User;

  constructor(
    private router: Router,
    private registerService: RegisterService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.registerUser = new FormGroup({
      login: new FormControl('', Validators.required),
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ),
      password: new FormControl('', Validators.required),
      confirmPass: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    if (this.registerUser.valid) {
      let authority: Authority;

      authority = {
        authorityDtoId: 0,
        authorityDtoName: 'USER'
      };

      this.user = {
        userId: 0,
        username: this.registerUser.get('login').value,
        firstName: '',
        lastName: '',
        password: btoa(this.registerUser.get('password').value),
        email: '' + this.registerUser.get('email').value,
        birthDate: null,
        age: 0,
        phoneNumber: 0,
        address: '',
        city: '',
        postalCode: 0,
        userRoleDto: authority
      };
      this.registerService.create(this.user).subscribe(response => {
        if (response) {
          this.goBack();
        }
      });
    }
  }

  goBack() {
    this.registerService.goBack();
  }
}
