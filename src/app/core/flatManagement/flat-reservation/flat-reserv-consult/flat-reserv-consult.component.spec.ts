import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FlatReservConsultComponent } from './flat-reserv-consultat.component';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';

describe('FlatReservConsultComponent', () => {
    let component: FlatReservConsultComponent;
    let fixture: ComponentFixture<FlatReservConsultComponent>;
    let flatReservationService: FlatReservationService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FlatReservConsultComponent],
            providers: [FlatReservationService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FlatReservConsultComponent);
        flatReservationService = TestBed.get(FlatReservationService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
