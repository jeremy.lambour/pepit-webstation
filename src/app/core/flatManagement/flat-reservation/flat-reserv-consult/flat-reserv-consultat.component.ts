
import { Component, OnInit } from '@angular/core';
import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { Authority } from 'src/app/common/models/User/authority';
import { User } from 'src/app/common/models/User/User';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: 'app-flat-reserv-consult',
  templateUrl: './flat-reserv-consult.component.html',
  styleUrls: ['./flat-reserv-consult.component.css'],
  providers: [FlatReservationService]
})
export class FlatReservConsultComponent implements OnInit {
  private user: User;
  private listReservationFlat: RFlat[] = [];

  private loading: Boolean;

  constructor(private flatReservationService: FlatReservationService, private authentificationService: AuthentificationService) { }

  ngOnInit() {
    this.loading = false;
    this.user = new User();
    this.user = JSON.parse(this.authentificationService.getUser());
    if (this.user !== null) {
      this.getReservationsFlatUser(this.user);
    }

  }

  getReservationsFlatUser(user: User): void {
    this.flatReservationService.getReservationsFlatUser(user).subscribe(res => {
      this.listReservationFlat = res;
      this.loading = true;
    });
  }
}
