import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { Reservation } from 'src/app/common/models/reservation/reservation';
import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { Authority } from 'src/app/common/models/User/authority';
import { User } from 'src/app/common/models/User/User';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: 'app-flat-reservation',
  templateUrl: './flat-reservation.component.html',
  styleUrls: ['./flat-reservation.component.css'],
  providers: [FlatService]
})
export class FlatReservationComponent implements OnInit {
  public router: Router;
  private user: User;
  private authority: Authority;

  public flatReservationForm: FormGroup;
  public flat: Flat;

  public arrivingDate: Date;
  public leavingDate: Date;
  public adultsNumber: number;
  public kidsNumber: number;

  public available: Flat;
  public reservation: Reservation;
  public rFlat: RFlat;
  public display: Boolean;

  constructor(
    public fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private domSanitizer: DomSanitizer,
    private _router: Router,
    private route: ActivatedRoute,
    private flatService: FlatService,
    private flatReservationService: FlatReservationService
  ) {
    this.router = _router;
    this.flatReservationForm = fb.group({
      arrivingDate: ['', Validators.required],
      leavingDate: ['', Validators.required],
      adultsNumber: ['', Validators.required],
      kidsNumber: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.display = false;
    this.flat = new Flat();
    this.route.paramMap.subscribe(params => {
      this.flat.flatId = +params.get('flatId');
      this.getFlat(this.flat.flatId);
    });
  }

  getFlat(i: number): void {
    this.flatService.getFlat(i).subscribe(res => {
      this.flat = res;
    });
  }

  submitRegistration(value: Object): void {
    this.arrivingDate = this.flatReservationForm.get('arrivingDate').value;
    this.leavingDate = this.flatReservationForm.get('leavingDate').value;
    this.adultsNumber = this.flatReservationForm.get('adultsNumber').value;
    this.kidsNumber = this.flatReservationForm.get('kidsNumber').value;

    this.getFlatPrice(this.arrivingDate, this.leavingDate);
  }

  getFlatPrice(arrivingDate: Date, leavingDate: Date): void {
    this.flatReservationService
      .getAvailabilityFlat(arrivingDate, leavingDate, this.flat)
      .subscribe(available => {
        this.available = available;

        if (this.available.flatId !== null) {
          this.router.navigate(
            ['/flatPriceDisplay/:flatId/:startDate/:endDate'],
            {
              queryParams: {
                idFlat: this.available.flatId,
                startDate: this.arrivingDate,
                endDate: this.leavingDate
              }
            }
          );
        } else {
          this.display = true;
        }

        /*  this.flatReservationService.insertReservationFlat(this.rFlat).subscribe(
                      insertedReservation => {
                          this.rFlat = insertedReservation;
                      }
                  );*/
      });
  }
}
