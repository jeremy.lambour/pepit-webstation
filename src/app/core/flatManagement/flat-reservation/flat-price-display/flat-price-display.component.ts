import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { Reservation } from 'src/app/common/models/reservation/reservation';
import { RFlat } from 'src/app/common/models/reservation/rFlat';
import { Authority } from 'src/app/common/models/User/authority';
import { User } from 'src/app/common/models/User/User';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-flat-price-display",
  templateUrl: "./flat-price-display.component.html",
  styleUrls: ["./flat-price-display.component.css"],
  providers: [FlatReservationService]
})
export class FlatPriceDisplayComponent implements OnInit {
  public loadingConsultFlat: Boolean;
  public idFlatString: string;
  public flat: Flat;
  public arrivingDateString: string;
  public leavingDateString: string;
  public idFlat: number;

  public price: any;
  public priceVingtPourcent: any;
  public priceDifference: any;
  public cancellationOption: any;
  public reservation: Reservation;
  public rFlat: RFlat;
  private user: User;
  private authority: Authority;
  private authentificationService: AuthentificationService;

  public checked: Boolean = false;

  constructor(
    private flatService: FlatService,
    private route: ActivatedRoute,
    private flatReservationService: FlatReservationService,
    private _authentificationService: AuthentificationService,
    private _router: Router
  ) {
    this.authentificationService = _authentificationService;
  }

  ngOnInit() {
    this.user = JSON.parse(this.authentificationService.getUser());
    this.loadingConsultFlat = false;
    this.idFlatString = this.route.snapshot.queryParamMap.get("idFlat");
    this.arrivingDateString = this.route.snapshot.queryParamMap.get(
      "startDate"
    );
    this.leavingDateString = this.route.snapshot.queryParamMap.get("endDate");
    this.getFlatString(this.idFlatString);
  }

  changeValue(value) {
    this.checked = !value;
    if (this.checked === true) {
      this.priceVingtPourcent = this.priceVingtPourcent + this.cancellationOption;
    }
    if (this.checked === false) {
      this.priceVingtPourcent = (20 * this.price) / 100;
    }
  }

  getFlatString(i: string): void {
    this.flatService.getFlatString(i).subscribe(res => {
      this.flat = res;
      this.getPriceFlatForDates(
        this.idFlatString,
        this.arrivingDateString,
        this.leavingDateString
      );
    });
  }

  getPriceFlatForDates(id: string, startDate: string, endDate: string): void {
    this.flatReservationService
      .getPriceFlatForDates(id, startDate, endDate)
      .subscribe(res => {
        this.price = res;
        this.priceVingtPourcent = (20 * this.price) / 100;
        this.priceDifference = this.price - this.priceVingtPourcent;
        this.cancellationOption = (10 * this.price) / 100;
        this.loadingConsultFlat = true;
      });
  }

  book(): void {
    this.authority = {
      authorityDtoId: 1,
      authorityDtoName: "USER"
    };

    if (this.checked === false) {
      this.cancellationOption = 0;
    }

    this.reservation = {
      reservationId: 0,
      nbPersons: 0,
      insuranceLossMaterial: 0,
      houseCancelOpt: this.cancellationOption,
      equipmentCancelOpt: 0,
      reservCancelopt: 0,
      user: this.user
    };
    const arrivingDate = new Date(this.arrivingDateString);
    const leavingDate = new Date(this.leavingDateString);

    this.rFlat = {
      flatDto: this.flat,
      reservationDto: this.reservation,
      startDate: arrivingDate,
      endDate: leavingDate,
      price: this.price
    };

    this.flatReservationService
      .insertReservationFlat(this.rFlat)
      .subscribe(insertedReservation => {
        this.rFlat = insertedReservation;
        this._router.navigate(['/consultReservEquip/']);
      });
  }
}
