import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FlatPriceDisplayComponent } from './flat-price-display.component';
import { FlatReservationService } from 'src/app/common/services/reservation/flat-reservation.service';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

describe('FlatPriceDisplayComponent', () => {
    let component: FlatPriceDisplayComponent;
    let fixture: ComponentFixture<FlatPriceDisplayComponent>;
    let flatService: FlatService;
    let flatReservationService: FlatReservationService;
    let authentificationService: AuthentificationService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FlatPriceDisplayComponent],
            providers: [FlatService, FlatReservationService, AuthentificationService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FlatPriceDisplayComponent);
        flatService = TestBed.get(FlatService);
        flatReservationService = TestBed.get(FlatReservationService);
        authentificationService = TestBed.get(AuthentificationService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
