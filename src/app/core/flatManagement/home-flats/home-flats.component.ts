import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';
import { User } from 'src/app/common/models/User/User';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: "app-home-flats",
  templateUrl: "./home-flats.component.html",
  styleUrls: ["./home-flats.component.css"]
})
export class HomeFlatsComponent implements OnInit {
  public loadingHomeFlats: boolean;
  public listFlat: Flat[] = [];
  private listCategories: FlatCategory[] = [];
  private listNumbersFlatInEachCategory: number[] = [];
  private listToDisplay: String[] = [];
  private flatType: String;
  private listTypeFlat: Flat[] = [];
  private user: User;
  private roleUser: string;

  private idHotel: number;
  public hotel: Hotel;

  constructor(
    private flatService: FlatService,
    private route: ActivatedRoute,
    private hotelService: HotelService,
    private flatCategoryService: FlatCategoryService,
    private authentificationService: AuthentificationService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(this.authentificationService.getUser());
    this.roleUser = this.user.userRoleDto.authorityDtoName;
    this.route.paramMap.subscribe(params => {
      this.idHotel = +params.get("hotelId");
      this.getHotel(this.idHotel);
    });
    this.loadingHomeFlats = false;
  }

  getHotel(i: number): void {
    this.hotelService.getHotel(i).subscribe(res => {
      this.hotel = res;
      this.getListFlatToDisplay();
    });
  }

  getListFlatToDisplay(): void {
    this.flatService.getListFlatToDisplay(this.hotel.hotelId).subscribe(res => {
      this.listFlat = res;
      this.getFlatCategories();
    });
  }

  getFlatCategories(): void {
    this.flatCategoryService.getFlatCategories().subscribe(res => {
      this.listCategories = res;
      this.listCategories.forEach(element => {
        this.listNumbersFlatInEachCategory.push(0);
      });
      this.listFlat.forEach(element => {
        for (let i = 0; i < this.listCategories.length; i++) {
          if (
            element.flatCategoryDto.flatCategoryName ===
            this.listCategories[i].flatCategoryName
          ) {
            this.listNumbersFlatInEachCategory[i] =
              this.listNumbersFlatInEachCategory[i] + 1;
            if (this.listNumbersFlatInEachCategory[i] === 1) {
              this.listTypeFlat.push(element);
            }
          }
        }
      });
      for (let i = 0; i < this.listCategories.length; i++) {
        this.flatType =
          "L'hôtel possède : " + this.listNumbersFlatInEachCategory[i];
        this.flatType +=
          " appartements de type : " + this.listCategories[i].flatCategoryName;
        this.listToDisplay.push(this.flatType);
        this.flatType = "";
      }

      this.loadingHomeFlats = true;
    });
  }
}
