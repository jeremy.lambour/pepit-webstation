import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeHotelsComponent } from './home-hotels/home-hotels.component';
import { HotelCreationComponent } from './hotel-creation/hotel-creation.component';
import { BingMapModule } from '../common/bing-map/bing-map.module';
import { FlatCreationComponent } from './flat-creation/flat-creation.component';
import { HotelEditionComponent } from './hotel-edition/hotel-edition.component';
import { FlatCategoryCreationComponent } from './flatCategory-creation/flatCategory-creation.component';
import { FlatEditionComponent } from './flat-edition/flat-edition.component';
import { HotelConsultationComponent } from './hotel-consultation/hotel-consultation.component';
import { HomeFlatsComponent } from './home-flats/home-flats.component';
import { FlatConsultationComponent } from './flat-consultation/flat-consultation.component';
import { FlatReservationComponent } from './flat-reservation/flat-reservation.component';
import { FlatPriceDisplayComponent } from './flat-reservation/flat-price-display/flat-price-display.component';
import { FlatReservConsultComponent } from './flat-reservation/flat-reserv-consult/flat-reserv-consultat.component';
import { SharedModule } from '../shared/shared.module';
import { CommentModule } from '../common/comment/comment.module';

@NgModule({
    imports: [CommonModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        BingMapModule,
        SharedModule,
        CommentModule],
    declarations: [
        HotelCreationComponent,
        HomeHotelsComponent,
        FlatCreationComponent,
        HotelEditionComponent,
        FlatCategoryCreationComponent,
        FlatEditionComponent,
        HotelConsultationComponent,
        HomeFlatsComponent,
        FlatConsultationComponent,
        FlatReservationComponent,
        FlatPriceDisplayComponent,
        FlatReservConsultComponent
    ],
})
export class FlatManagementModule { }
