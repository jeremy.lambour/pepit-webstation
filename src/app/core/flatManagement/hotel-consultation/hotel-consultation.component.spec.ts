import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HotelConsultationComponent } from './hotel-consultation.component';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';

describe('HotelConsultationComponent', () => {
    let component: HotelConsultationComponent;
    let fixture: ComponentFixture<HotelConsultationComponent>;
    let hotelService: HotelService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HotelConsultationComponent],
            providers: [HotelService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HotelConsultationComponent);
        hotelService = TestBed.get(HotelService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
