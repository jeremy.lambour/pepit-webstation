import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { BingMapsLoaderService } from 'src/app/common/services/common/bing-maps-loader.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';
import { BingMapComponent } from 'src/app/core/common/bing-map/bing-map.component';
import { CommentHotel } from 'src/app/common/models/common/comment/hotel/CommentHotel';
import { CommentHotelService } from 'src/app/common/services/common/comment/hotel/comment-hotel.service';
import { Comment } from 'src/app/common/models/common/comment/Comment';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: 'app-hotel-consultation',
  templateUrl: './hotel-consultation.component.html',
  styleUrls: ['./hotel-consultation.component.css'],
  providers: [HotelService]
})
export class HotelConsultationComponent implements OnInit {
  public loadingConsultHotel: Boolean;
  public idHotel: number;
  public hotel: Hotel;
  private txtSource: any;
  private mapReady: boolean;
  public listComment: Comment[] = [];
  public listCommentHotel: CommentHotel[] = [];
  public loadingComment: boolean;
  private comment: Comment;
  private commentHotel: CommentHotel;

  @ViewChild(BingMapComponent) bingMapComponent: BingMapComponent;

  constructor(
    private hotelService: HotelService,
    private route: ActivatedRoute,
    private bingMapService: BingMapsLoaderService,
    private commentHotelService: CommentHotelService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idHotel = +params.get('hotelId');
      this.getHotel(this.idHotel);
    });
    this.loadingConsultHotel = false;
    this.initMap();
  }

  initMap() {
    this.bingMapService.load().then(res => {
      console.log('BingMapsLoader.load.then', res);
      this.mapReady = true;
    }, 2000);
  }

  getHotel(id: number): void {
    this.hotelService.getHotel(id).subscribe(res => {
      this.hotel = res;
      this.loadingConsultHotel = true;
      this.getComment();
    });
  }

  calculDir(value) {
    this.txtSource = value.city1;
  }

  getComment() {
    this.commentHotelService.getListCommentHotel(this.hotel.hotelId).subscribe(response => {
      console.log(response);
      this.listCommentHotel = response;
      for (const c of this.listCommentHotel) {
        this.comment = {
          commentId: c.commentDto.commentId,
          content: c.commentDto.content,
          from: c.commentDto.from,
          note: c.commentDto.note
        };
        this.listComment.push(this.comment);
      }
      this.loadingComment = true;
    });
  }

  sendCommentEquip($event) {

    this.commentHotel = {
      commentDto: $event,
      hotelDto: this.hotel
    };
    console.log(this.commentHotel);
    this.commentHotelService
      .insertCommentHotel(this.commentHotel)
      .subscribe(response => {
        console.log('commentInseré');
      });
  }
}
