import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-flat-consultation",
  templateUrl: "./flat-consultation.component.html",
  styleUrls: ["./flat-consultation.component.css"],
  providers: [FlatService]
})
export class FlatConsultationComponent implements OnInit {
  public loadingConsultFlat: Boolean;
  public idFlat: number;
  public flat: Flat = new Flat();

  constructor(
    private flatService: FlatService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idFlat = +params.get("flatId");
      this.getFlat(this.idFlat);
    });
    this.loadingConsultFlat = false;
  }

  getFlat(id: number): void {
    this.flatService.getFlat(id).subscribe(res => {
      this.flat = res;
      this.loadingConsultFlat = true;
    });
  }
}
