import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';

import { FlatConsultationComponent } from './flat-consultation.component';
import { BingMapComponent } from '../../common/bing-map/bing-map.component';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';

const authority = {
    authorityDtoId: 1,
    authorityDtoName: 'user'
};

const userDto = {
    userId: 1,
    username: 'BobD',
    password: '1234',
    firstName: 'Bob',
    lastName: 'Dylan',
    birthDate: new Date(),
    age: 35,
    phoneNumber: 1234567890,
    address: 'address',
    email: 'boby.dylan@gmail.com',
    city: 'Paris',
    postalCode: 95000,
    userRoleDto: authority
};

const hotelDto = {
    hotelId: 1,
    hotelName: 'hotelName',
    hotelRoomsNumber: 3,
    phoneNumber: 'phoneNumber',
    mailAddress: 'mailAddress',
    address: 'address',
    userDto: userDto
};

const flatCategoryDto = {
    flatCategoryId: 1,
    flatCategoryName: 'studio 3/4 personnes'
};

const flat = {
    flatId: 1, flatDescription: 'desc1', flatGuidance: 'guidance1', flatCapacity: 1, hotelDto: hotelDto,
    flatCategoryDto: flatCategoryDto
};

describe('FlatConsultationComponent', () => {
    let component: FlatConsultationComponent;
    let fixture: ComponentFixture<FlatConsultationComponent>;
    let flatService: FlatService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FlatConsultationComponent],
            providers: [FlatService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FlatConsultationComponent);
        flatService = TestBed.get(FlatService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set loadingConsultFlat to true', async(() => {
        component.getFlat(1);
        expect(component.loadingConsultFlat).toBeFalsy();
    }));

});
