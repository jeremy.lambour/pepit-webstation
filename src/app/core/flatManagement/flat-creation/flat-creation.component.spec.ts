import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { BingMapComponent } from '../../common/bing-map/bing-map.component';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatCreationComponent } from './flat-creation.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';

describe('FlatCreationComponent', () => {
    let component: FlatCreationComponent;
    let fixture: ComponentFixture<FlatCreationComponent>;
    let flatService: FlatService;
    let flatCategoryService: FlatCategoryService;
    let uploadService: UploadService;
    let hotelService: HotelService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FlatCreationComponent],
            providers: [FlatService, FlatCategoryService, UploadService, HotelService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FlatCreationComponent);
        flatService = TestBed.get(FlatService);
        flatCategoryService = TestBed.get(FlatCategoryService);
        uploadService = TestBed.get(UploadService);
        hotelService = TestBed.get(HotelService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
