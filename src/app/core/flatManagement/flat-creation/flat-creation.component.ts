import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageDTO } from 'src/app/common/models/common/ImageDTO';
import { Path } from 'src/app/common/models/common/Path';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-flat-creation",
  templateUrl: "./flat-creation.component.html",
  styleUrls: ["./flat-creation.component.css"],
  providers: [FlatService]
})
export class FlatCreationComponent implements OnInit {
  private static adjustedWidth: number;
  private static loadingPhoto: boolean;
  public router: Router;

  public flatForm: FormGroup;
  public submit: Boolean;
  public loading: Boolean;

  private flat: Flat;
  private hotel: Hotel;
  private flatCategory: FlatCategory;

  public urlCompo = FlatCreationComponent;
  private base64Image: SafeUrl;
  private base64: string;
  private fileImage: File;
  private p: Path;
  private image: ImageDTO;

  private listCategories: FlatCategory[] = [];

  constructor(
    public fb: FormBuilder,
    private flatService: FlatService,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private domSanitizer: DomSanitizer,
    private uploadService: UploadService,
    private _router: Router,
    private route: ActivatedRoute,
    private hotelService: HotelService,
    private flatCategoryService: FlatCategoryService
  ) {
    this.router = _router;
    this.flatForm = fb.group({
      flatDescription: ["", Validators.required],
      flatGuidance: ["", Validators.required],
      flatCapacity: ["", Validators.required],
      flatCategory: ["", Validators.required],
      numbersFlat: ["", Validators.required],
      photo: [""]
    });
  }

  ngOnInit() {
    this.hotel = new Hotel();
    this.loading = false;
    this.route.paramMap.subscribe(params => {
      this.hotel.hotelId = +params.get("hotelId");
      this.getHotel(this.hotel.hotelId);
    });

    FlatCreationComponent.loadingPhoto = false;
    FlatCreationComponent.adjustedWidth = 0;
  }

  getHotel(i: number): void {
    this.hotelService.getHotel(i).subscribe(res => {
      this.hotel = res;
      this.getFlatCategories();
    });
  }

  getFlatCategories(): void {
    this.flatCategoryService.getFlatCategories().subscribe(res => {
      this.listCategories = res;
      this.loading = true;
    });
  }

  submitRegistration(value: Object): void {
    console.log(value);
    this.flatCategory = {
      flatCategoryId: this.flatForm.get("flatCategory").value,
      flatCategoryName: ""
    };
    this.flat = {
      flatId: null,
      flatDescription: this.flatForm.get("flatDescription").value,
      flatGuidance: this.flatForm.get("flatGuidance").value,
      flatCapacity: this.flatForm.get("flatCapacity").value,
      hotelDto: this.hotel,
      flatCategoryDto: this.flatCategory
    };

    if (this.fileImage !== undefined) {
      // insert la picture dans la table picture via this.p.path
      this.uploadService.UploadImgShop(this.fileImage).subscribe(x => {
        this.p = x;
      });
    }

    for (let i = 0; i < this.flatForm.get("numbersFlat").value; i++) {
      this.flatService.insertFlat(this.flat).subscribe(res => {
        this.flat = res;
        this.router.navigate(["/homeHotels"]);
      });
    }
  }

  setImg() {
    /*this.pictureService.setImgShop(this.fileImage).subscribe(res => {
            const bool = res;
        });*/
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    FlatCreationComponent.loadingPhoto = false;
    this.fileImage = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      // TO DO nexte feature
      /*this.base64Image = this.domSanitizer.bypassSecurityTrustUrl(myReader.result);
            const img = new Image();
            img.onload = function () {
                ShopCreationComponent.adjustedWidth = (img.width / img.height) * 150;
                ShopCreationComponent.loadingPhoto = true;
            };
            img.src = myReader.result;*/
    };
    myReader.readAsDataURL(this.fileImage);
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file: File = event.target.files[0];
      this.flatForm.get("photo").setValue(file);
    }
  }
}
