import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageDTO } from 'src/app/common/models/common/ImageDTO';
import { Path } from 'src/app/common/models/common/Path';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-flat-edition",
  templateUrl: "./flat-edition.component.html",
  styleUrls: ["./flat-edition.component.css"],
  providers: [FlatService]
})
export class FlatEditionComponent implements OnInit {
  private static adjustedWidth: number;
  private static loadingPhoto: boolean;
  public router: Router;

  flatForm: FormGroup;
  submit: Boolean;
  loading: Boolean;

  private flat: Flat;
  private hotel: Hotel;
  private flatCategory: FlatCategory;

  public urlCompo = FlatEditionComponent;
  private base64Image: SafeUrl;
  private base64: string;
  private fileImage: File;
  private p: Path;
  private image: ImageDTO;

  private listCategories: FlatCategory[] = [];

  constructor(
    public fb: FormBuilder,
    private flatService: FlatService,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private domSanitizer: DomSanitizer,
    private uploadService: UploadService,
    private _router: Router,
    private route: ActivatedRoute,
    private hotelService: HotelService,
    private flatCategoryService: FlatCategoryService
  ) {
    this.router = _router;
    this.flatForm = fb.group({
      flatDescription: ["", Validators.required],
      flatGuidance: ["", Validators.required],
      flatCapacity: ["", Validators.required],
      photo: [""]
    });
  }

  ngOnInit() {
    this.flat = new Flat();
    this.loading = false;
    this.route.paramMap.subscribe(params => {
      this.flat.flatId = +params.get("flatId");
      this.getFlat(this.flat.flatId);
    });

    FlatEditionComponent.loadingPhoto = false;
    FlatEditionComponent.adjustedWidth = 0;
  }

  getFlat(i: number): void {
    this.flatService.getFlat(i).subscribe(res => {
      this.flat = res;
      this.getFlatCategories();
    });
  }

  getFlatCategories(): void {
    this.flatCategoryService.getFlatCategories().subscribe(res => {
      this.listCategories = res;
      this.loading = true;
    });
  }

  submitRegistration(value: Object): void {
    console.log(value);
    this.flat = {
      flatId: this.flat.flatId,
      flatDescription: this.flatForm.get("flatDescription").value,
      flatGuidance: this.flatForm.get("flatGuidance").value,
      flatCapacity: this.flatForm.get("flatCapacity").value,
      hotelDto: this.flat.hotelDto,
      flatCategoryDto: this.flat.flatCategoryDto
    };

    if (this.fileImage !== undefined) {
      this.uploadService.UploadImgShop(this.fileImage).subscribe(x => {
        this.p = x;
        // insert la picture dans la table picture via this.p.path
      });
    }

    this.flatService.updateFlat(this.flat).subscribe(res => {
      this.flat = res;
      this.router.navigate(["/homeHotels"]);
    });
  }

  setImg() {
    /*this.pictureService.setImgShop(this.fileImage).subscribe(res => {
            const bool = res;
        });*/
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    FlatEditionComponent.loadingPhoto = false;
    this.fileImage = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      /*this.base64Image = this.domSanitizer.bypassSecurityTrustUrl(myReader.result);
            const img = new Image();
            img.onload = function () {
                ShopCreationComponent.adjustedWidth = (img.width / img.height) * 150;
                ShopCreationComponent.loadingPhoto = true;
            };
            img.src = myReader.result;*/
    };
    myReader.readAsDataURL(this.fileImage);
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file: File = event.target.files[0];
      this.flatForm.get("photo").setValue(file);
    }
  }

  deleteFlat() {
    this.flatService.deleteFlat(this.flat.flatId).subscribe(res => {
      this.router.navigate(["/homeHotels"]);
    });
  }
}
