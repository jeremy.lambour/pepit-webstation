import { Component, OnInit } from '@angular/core';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';
import { User } from 'src/app/common/models/User/User';
import { AuthentificationService } from 'src/app/common/services/authentification/authentification.service';

@Component({
  selector: "app-home-hotels",
  templateUrl: "./home-hotels.component.html",
  styleUrls: ["./home-hotels.component.css"]
})
export class HomeHotelsComponent implements OnInit {
  public loadingHomeHotels: boolean;
  public listHotel: Hotel[] = [];
  private user: User;
  private roleUser: string;

  constructor(private hotelService: HotelService, private authentificationService: AuthentificationService) { }

  ngOnInit() {
    this.user = JSON.parse(this.authentificationService.getUser());
    this.roleUser = this.user.userRoleDto.authorityDtoName;
    this.loadingHomeHotels = false;
    this.getListHotelToDisplay();
  }

  getListHotelToDisplay(): void {
    this.hotelService.getListHotelToDisplay().subscribe(res => {
      this.listHotel = res;
      this.loadingHomeHotels = true;
    });
  }
}
