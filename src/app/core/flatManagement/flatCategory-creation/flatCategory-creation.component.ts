import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-flatcategory-creation",
  templateUrl: "./flatCategory-creation.component.html",
  styleUrls: ["./flatCategory-creation.component.css"],
  providers: [FlatCategoryService]
})
export class FlatCategoryCreationComponent implements OnInit {
  public router: Router;

  flatCategoryForm: FormGroup;
  submit: Boolean;

  private flatCategory: FlatCategory;

  constructor(
    public fb: FormBuilder,
    private flatCategoryService: FlatCategoryService,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private domSanitizer: DomSanitizer,
    private _router: Router,
    private route: ActivatedRoute
  ) {
    this.router = _router;
    this.flatCategoryForm = fb.group({
      flat_category_name: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.flatCategory = new FlatCategory();
  }

  submitRegistration(value: Object): void {
    console.log(value);
    this.flatCategory = {
      flatCategoryId: null,
      flatCategoryName: this.flatCategoryForm.get("flat_category_name").value
    };

    this.flatCategoryService
      .insertFlatCategory(this.flatCategory)
      .subscribe(res => {
        this.flatCategory = res;
        this.router.navigate(["/homeHotels"]);
      });
  }
}
