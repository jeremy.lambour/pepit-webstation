import { ChangeDetectorRef, Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ImageDTO } from 'src/app/common/models/common/ImageDTO';
import { Path } from 'src/app/common/models/common/Path';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { BingMapsLoaderService } from 'src/app/common/services/common/bing-maps-loader.service';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';

import { BingMapComponent } from '../../common/bing-map/bing-map.component';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-hotel-creation",
  templateUrl: "./hotel-creation.component.html",
  styleUrls: ["./hotel-creation.component.css"],
  providers: [HotelService]
})
export class HotelCreationComponent implements OnInit {
  private static adjustedWidth: number;
  private static loadingPhoto: boolean;
  public router: Router;
  @ViewChild(BingMapComponent) bingMapComponent: BingMapComponent;

  public hotelForm: FormGroup;
  public submit: Boolean;
  public loading: Boolean;

  private hotel: Hotel;

  public urlCompo = HotelCreationComponent;
  private base64Image: SafeUrl;
  private base64: string;
  private fileImage: File;
  private p: Path;
  private image: ImageDTO;

  private txtSource: any;
  private mapReady: boolean;

  constructor(
    public fb: FormBuilder,
    private hotelService: HotelService,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private domSanitizer: DomSanitizer,
    private uploadService: UploadService,
    private bingMapService: BingMapsLoaderService,
    private _router: Router
  ) {
    this.router = _router;
    this.hotelForm = fb.group({
      hotelName: ["", Validators.required],
      txtSource: ["", Validators.required],
      phoneNumber: ["", Validators.required],
      hotelRoomsNumber: ["", Validators.required],
      mailAddress: ["", Validators.required],
      photo: [""]
    });
  }

  ngOnInit() {
    this.mapReady = false;
    this.loading = false;
    HotelCreationComponent.loadingPhoto = false;
    HotelCreationComponent.adjustedWidth = 0;
    this.initMap();
  }

  calculDir(value) {
    this.txtSource = value.city1;
  }

  initMap() {
    this.bingMapService.load().then(res => {
      console.log("BingMapsLoader.load.then", res);
      this.mapReady = true;
    });
  }

  submitRegistration(value: Object): void {
    this.hotel = {
      hotelId: null,
      hotelName: this.hotelForm.get("hotelName").value,
      hotelRoomsNumber: this.hotelForm.get("hotelRoomsNumber").value,
      phoneNumber: this.hotelForm.get("phoneNumber").value,
      mailAddress: this.hotelForm.get("mailAddress").value,
      address: this.bingMapComponent.placeSuggest1,
      userDto: null
    };

    if (this.fileImage !== undefined) {
      this.uploadService.UploadImgShop(this.fileImage).subscribe(x => {
        this.p = x;
        // insert la picture dans la table picture via this.p.path
      });
    }

    this.hotelService.insertHotel(this.hotel).subscribe(res => {
      this.hotel = res;
      this.router.navigate(["/homeHotels"]);
    });
  }

  setImg() {
    /*this.pictureService.setImgShop(this.fileImage).subscribe(res => {
            const bool = res;
        });*/
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    HotelCreationComponent.loadingPhoto = false;
    this.fileImage = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      /*this.base64Image = this.domSanitizer.bypassSecurityTrustUrl(myReader.result);
            const img = new Image();
            img.onload = function () {
                ShopCreationComponent.adjustedWidth = (img.width / img.height) * 150;
                ShopCreationComponent.loadingPhoto = true;
            };
            img.src = myReader.result;*/
    };
    myReader.readAsDataURL(this.fileImage);
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file: File = event.target.files[0];
      this.hotelForm.get("photo").setValue(file);
    }
  }
}
