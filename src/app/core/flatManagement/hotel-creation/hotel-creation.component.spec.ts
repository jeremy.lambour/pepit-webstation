import { async, ComponentFixture, TestBed, tick, fakeAsync, flushMicrotasks } from '@angular/core/testing';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HotelCreationComponent } from './hotel-creation.component';

describe('HotelCreationComponent', () => {
    let component: HotelCreationComponent;
    let fixture: ComponentFixture<HotelCreationComponent>;
    let flatService: FlatService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HotelCreationComponent],
            providers: [FlatService, HttpClient],
            imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule, ReactiveFormsModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HotelCreationComponent);
        flatService = TestBed.get(FlatService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
