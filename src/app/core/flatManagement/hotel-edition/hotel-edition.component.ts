import { ChangeDetectorRef, Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageDTO } from 'src/app/common/models/common/ImageDTO';
import { Path } from 'src/app/common/models/common/Path';
import { Flat } from 'src/app/common/models/flatManagement/Flat';
import { FlatCategory } from 'src/app/common/models/flatManagement/FlatCategory';
import { Hotel } from 'src/app/common/models/flatManagement/Hotel';
import { BingMapsLoaderService } from 'src/app/common/services/common/bing-maps-loader.service';
import { UploadService } from 'src/app/common/services/common/upload.service';
import { FlatService } from 'src/app/common/services/flatManagement/flat/flat.service';
import { FlatCategoryService } from 'src/app/common/services/flatManagement/flat/flatCategory.service';
import { HotelService } from 'src/app/common/services/flatManagement/hotel/hotel.service';
import { BingMapComponent } from 'src/app/core/common/bing-map/bing-map.component';

/// <reference path="types/MicrosoftMaps/CustomMapStyles.d.ts" />
/// <reference path="types/MicrosoftMaps/Microsoft.Maps.d.ts" />

@Component({
  selector: "app-hotel-edition",
  templateUrl: "./hotel-edition.component.html",
  styleUrls: ["./hotel-edition.component.css"]
})
export class HotelEditionComponent implements OnInit {
  private static adjustedWidth: number;
  private static loadingPhoto: boolean;

  @ViewChild(BingMapComponent) bingMapComponent: BingMapComponent;

  public hotelForm: FormGroup;
  public submit: Boolean;
  public loading: Boolean;

  private hotel: Hotel;

  public urlCompo = HotelEditionComponent;
  private base64Image: SafeUrl;
  private base64: string;
  private fileImage: File;
  private p: Path;
  private image: ImageDTO;

  private txtSource: any;
  private txtDestination: any;
  private mapReady: boolean;

  private idHotel: number;
  private loadingHotel: Boolean;

  private listFlat: Flat[] = [];
  private listCategories: FlatCategory[] = [];
  private listNumbersFlatInEachCategory: number[] = [];
  private listToDisplay: String[] = [];
  private flatType: String;
  private listTypeFlat: Flat[] = [];

  public router: Router;

  constructor(
    public fb: FormBuilder,
    private hotelService: HotelService,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone,
    private domSanitizer: DomSanitizer,
    private uploadService: UploadService,
    private bingMapService: BingMapsLoaderService,
    private route: ActivatedRoute,
    private _router: Router,
    private flatService: FlatService,
    private flatCategoryService: FlatCategoryService
  ) {
    this.router = _router;
    this.hotelForm = fb.group({
      hotelName: ["", Validators.required],
      txtSource: ["", Validators.required],
      phoneNumber: ["", Validators.required],
      hotelRoomsNumber: ["", Validators.required],
      mailAddress: ["", Validators.required],
      photo: [""]
    });
  }

  ngOnInit() {
    this.loadingHotel = false;
    this.route.paramMap.subscribe(params => {
      this.idHotel = +params.get("HotelId");
      this.getHotel(this.idHotel);
    });
    this.mapReady = false;
    this.loading = false;
    this.initMap();
    HotelEditionComponent.loadingPhoto = false;
    HotelEditionComponent.adjustedWidth = 0;
  }

  getHotel(i: number): void {
    this.hotelService.getHotel(i).subscribe(res => {
      this.hotel = res;
      this.getListFlatToDisplay();
    });
  }

  getListFlatToDisplay(): void {
    this.flatService.getListFlatToDisplay(this.hotel.hotelId).subscribe(res => {
      this.listFlat = res;
      this.getFlatCategories();
    });
  }

  getFlatCategories(): void {
    this.flatCategoryService.getFlatCategories().subscribe(res => {
      this.listCategories = res;
      this.listCategories.forEach(element => {
        this.listNumbersFlatInEachCategory.push(0);
      });
      this.listFlat.forEach(element => {
        for (let i = 0; i < this.listCategories.length; i++) {
          if (
            element.flatCategoryDto.flatCategoryName ===
            this.listCategories[i].flatCategoryName
          ) {
            this.listNumbersFlatInEachCategory[i] =
              this.listNumbersFlatInEachCategory[i] + 1;
            if (this.listNumbersFlatInEachCategory[i] === 1) {
              this.listTypeFlat.push(element);
            }
          }
        }
      });
      for (let i = 0; i < this.listCategories.length; i++) {
        this.flatType =
          "L'hôtel possède : " + this.listNumbersFlatInEachCategory[i];
        this.flatType +=
          " appartements de type : " + this.listCategories[i].flatCategoryName;
        this.listToDisplay.push(this.flatType);
        this.flatType = "";
      }

      this.loadingHotel = true;
    });
  }

  calculDir(value) {
    this.txtSource = value.city1;
  }

  initMap() {
    this.bingMapService.load().then(res => {
      console.log("BingMapsLoader.load.then", res);
      this.mapReady = true;
    });
  }

  submitRegistration(value: Object): void {
    console.log(value);
    this.hotel = {
      hotelId: this.idHotel,
      hotelName: this.hotelForm.get("hotel_name").value,
      hotelRoomsNumber: this.hotelForm.get("hotel_rooms_number").value,
      phoneNumber: this.hotelForm.get("phone_number").value,
      mailAddress: this.hotelForm.get("mail_address").value,
      address: this.bingMapComponent.placeSuggest1,
      userDto: null
    };

    if (this.fileImage !== undefined) {
      // insert la picture dans la table picture via this.p.path
      this.uploadService.UploadImgShop(this.fileImage).subscribe(x => {
        this.p = x;
      });
    }

    this.hotelService.updateHotel(this.hotel).subscribe(res => {
      this.hotel = res;
      this.router.navigate(["/homeHotels"]);
    });
  }

  setImg() {
    /*this.pictureService.setImgShop(this.fileImage).subscribe(res => {
            const bool = res;
        });*/
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    HotelEditionComponent.loadingPhoto = false;
    this.fileImage = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      /*this.base64Image = this.domSanitizer.bypassSecurityTrustUrl(myReader.result);
            const img = new Image();
            img.onload = function () {
                ShopCreationComponent.adjustedWidth = (img.width / img.height) * 150;
                ShopCreationComponent.loadingPhoto = true;
            };
            img.src = myReader.result;*/
    };
    myReader.readAsDataURL(this.fileImage);
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file: File = event.target.files[0];
      this.hotelForm.get("photo").setValue(file);
    }
  }

  deleteHotel() {
    this.hotelService.deleteHotelFlat(this.hotel.hotelId).subscribe(res => {
      this.hotelService.deleteHotel(this.hotel.hotelId).subscribe(result => {
        this.router.navigate(["/homeHotels"]);
      });
    });
  }
}
