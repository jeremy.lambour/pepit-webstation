import { Component, OnInit } from '@angular/core';
import { ThemeService } from './common/services/common/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'webstation-front';

  constructor(private themeService: ThemeService) {
  }

  ngOnInit() {
    this.themeService.checkSeason();
  }
}
