import { FlatAdminComponent } from "./core/admin/flat-admin/flat-admin.component";
import { SeasonAdminComponent } from "./core/admin/season-admin/season-admin.component";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { NgModule, LOCALE_ID } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import {
  NgbModule,
  NgbTimeAdapter,
  NgbDateAdapter,
  NgbModalModule
} from "@ng-bootstrap/ng-bootstrap";
import { DatePipe, registerLocaleData } from "@angular/common";
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { PieChartEquipmentStatisticsModule } from './core/SalesStatistics/equipment-pie-chart/pie-chart-equipment-statistics.module';
import { RouterModule } from '@angular/router';
import { ApplicationInsightsModule, AppInsightsService } from '@markpieszak/ng-application-insights';
import { MatSelectModule } from '@angular/material/select';
import { MaterialModule } from './core/SalesStatistics/material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LineChartFlatReservationModule } from './core/SalesStatistics/flat-line-chart/line-chart-flat-statistics.module';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AccountComponent } from "./core/account/account.component";
import { AuthentificationModule } from "./core/authentification/authentification.module";
import { CreateUserModule } from "./core/create-user/createUser.module";
import { EquipmentManagementModule } from "./core/equipmentmanagement/equipment-management.module";
import { FlatManagementModule } from "./core/flatManagement/flat-management.module";
import { HomeReservationModule } from "./core/home-reservation/home-reservation.module";
import { HomeModule } from "./core/home/home.module";
import { NavBarComponent } from "./core/shared/nav-bar/nav-bar.component";
import { SharedModule } from "./core/shared/shared.module";
import { ShopManagementModule } from "./core/shopmanagement/shop-management.module";
import { DistribEquipmentsComponent } from "./core/staff/distrib-equipments/distrib-equipments.component";
import { TakeBackEquipmentsComponent } from "./core/staff/take-back-equipments/take-back-equipments.component";
import { GrdFilterPipe } from "./core/staff/grd-filter-pipe";
import { CustomerCardComponent } from "./core/staff/customer-card/customer-card.component";
import { CustomerCardTbeComponent } from "./core/staff/customer-card-tbe/customer-card-tbe.component";
import { ChatModule } from "./core/shared/chat/chat.module";
import { CommentModule } from "./core/common/comment/comment.module";
import { EquipmentAdminComponent } from "./core/admin/equipment-admin/equipment-admin.component";
import { SkiLiftAdminComponent } from "./core/admin/ski-lift-admin/ski-lift-admin.component";
import { NgbTimeStringAdapter } from "./common/services/common/NgbTimeStringAdapter";
import { NgbStringAdapter } from "./common/services/common/ngbStringAdapter";
import { KindergartenModule } from "./core/kindergarten/kindergarten.module";
import { SkiingSchoolModule } from "./core/skiing-school/skiing-school.module";
import { SeasonViewComponent } from "./core/season/season-view/season-view.component";
import { SeasonCardComponent } from "./core/season/season-card/season-card.component";
import localeFr from "@angular/common/locales/fr";
import { JwtInterceptor } from "./common/services/authentification/jwtInterceptor";
registerLocaleData(localeFr);
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AccountComponent,
    DistribEquipmentsComponent,
    TakeBackEquipmentsComponent,
    GrdFilterPipe,
    CustomerCardComponent,
    CustomerCardTbeComponent,
    SeasonAdminComponent,
    FlatAdminComponent,
    EquipmentAdminComponent,
    SkiLiftAdminComponent,
    SeasonViewComponent,
    SeasonCardComponent
  ],
  imports: [
    NgbModalModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    AuthentificationModule,
    CommentModule,
    CreateUserModule,
    ShopManagementModule,
    FlatManagementModule,
    EquipmentManagementModule,
    SharedModule,
    HomeReservationModule,
    HomeModule,
    ChatModule,
    SkiingSchoolModule,
    KindergartenModule,
    PieChartEquipmentStatisticsModule,
    ChartsModule,
    MatSelectModule,
    LineChartFlatReservationModule,
    MaterialModule,
    RouterModule.forRoot([
      { path: '', pathMatch: 'full', redirectTo: 'app' }
    ]),
    ApplicationInsightsModule.forRoot({
      instrumentationKey: 'app_insights_instrumentation_key'
    }
    )
  ],


  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: NgbDateAdapter, useClass: NgbStringAdapter },
    { provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter },
    { provide: LOCALE_ID, useValue: "fr" },
    AppInsightsService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AppModule { }
