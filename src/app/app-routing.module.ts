import { EquipmentAdminComponent } from "./core/admin/equipment-admin/equipment-admin.component";
import { FlatAdminComponent } from "./core/admin/flat-admin/flat-admin.component";
import { LiftReservationComponent } from "./core/home-reservation/lift-reservation/consult/lift-reservation.component";
import { CreateLiftReservationComponent } from "./core/home-reservation/lift-reservation/create/create-lift-reservation.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthentificationComponent } from "./core/authentification/authentification.component";
import { CreateUserComponent } from "./core/create-user/create-user.component";
import { ShopCreationComponent } from "./core/shopmanagement/shop-creation/shop-creation.component";
import { HomeStoresComponent } from "./core/shopmanagement/home-stores/home-stores.component";
import { ShopConsultationComponent } from "./core/shopmanagement/shop-consultation/shop-consultation.component";
import { ShopEditionComponent } from "./core/shopmanagement/shop-edition/shop-edition.component";
import { HomeHotelsComponent } from "./core/flatManagement/home-hotels/home-hotels.component";
import { HotelCreationComponent } from "./core/flatManagement/hotel-creation/hotel-creation.component";
import { FlatCreationComponent } from "./core/flatManagement/flat-creation/flat-creation.component";
import { HotelEditionComponent } from "./core/flatManagement/hotel-edition/hotel-edition.component";
import { FlatCategoryCreationComponent } from "./core/flatManagement/flatCategory-creation/flatCategory-creation.component";
import { FlatEditionComponent } from "./core/flatManagement/flat-edition/flat-edition.component";
import { HotelConsultationComponent } from "./core/flatManagement/hotel-consultation/hotel-consultation.component";
import { HomeFlatsComponent } from "./core/flatManagement/home-flats/home-flats.component";
import { FlatConsultationComponent } from "./core/flatManagement/flat-consultation/flat-consultation.component";
import { EquipmentCreationComponent } from "./core/equipmentmanagement/equipment-creation/equipment-creation.component";
import { EquipmentEditionComponent } from "./core/equipmentmanagement/equipment-edition/equipment-edition.component";
import { EquipmentHomeStoresComponent } from "./core/equipmentmanagement/equipment-home-stores/equipment-home-stores.component";
import { EquipmentConsultationComponent } from "./core/equipmentmanagement/equipment-consultation/equipment-consultation.component";
import { FlatReservationComponent } from "./core/flatManagement/flat-reservation/flat-reservation.component";
import { FlatPriceDisplayComponent } from "./core/flatManagement/flat-reservation/flat-price-display/flat-price-display.component";
import { FlatReservConsultComponent } from "./core/flatManagement/flat-reservation/flat-reserv-consult/flat-reserv-consultat.component";
// tslint:disable-next-line:max-line-length
import { CreateEquipmentReservationComponent } from "./core/home-reservation/equipment-reservation/create/create-equipment-reservation.component";
import { HomeReservationComponent } from "./core/home-reservation/home-reservation.component";
import { HomeComponent } from "./core/home/home.component";
import { ConsultReservEquipmentComponent } from "./core/home-reservation/equipment-reservation/consult/consult-reserv-equipment.component";
import { AccountComponent } from "./core/account/account.component";
import { StaffHomeComponent } from "./core/staff/staff-home/staff-home.component";
import { EquipmentsToPrepareComponent } from "./core/staff/equipments-to-prepare/equipments-to-prepare.component";
import { DistribEquipmentsComponent } from "./core/staff/distrib-equipments/distrib-equipments.component";
import { TakeBackEquipmentsComponent } from "./core/staff/take-back-equipments/take-back-equipments.component";
import { CustomerCardComponent } from "./core/staff/customer-card/customer-card.component";
import { CustomerCardTbeComponent } from "./core/staff/customer-card-tbe/customer-card-tbe.component";
import { ChatComponent } from "./core/shared/chat/chat.component";
import { CommentComponent } from "./core/common/comment/comment.component";
import { HomeAdministrationComponent } from "./core/admin/home-administration/home-administration.component";
import { SeasonAdminComponent } from "./core/admin/season-admin/season-admin.component";
import { SkiLiftAdminComponent } from "./core/admin/ski-lift-admin/ski-lift-admin.component";
import { SeasonViewComponent } from "./core/season/season-view/season-view.component";
import { KindergartenPlanningViewComponent } from "./core/kindergarten/kindergarten-planning-view/kindergarten-planning-view.component";
import { KindergartenUpdatePlanningComponent }
  from "./core/kindergarten/kindergarten-planning/kindergarten-update-planning/kindergarten-update-planning.component";
import { KindergartenCreationPlanningComponent }
  from "./core/kindergarten/kindergarten-planning/kindergarten-creation-planning/kindergarten-creation-planning.component";
import { KindergartenPlanningComponent } from "./core/kindergarten/kindergarten-planning/kindergarten-planning.component";
import { KindergartenComponent } from "./core/kindergarten/kindergarten.component";
import { MonitorViewComponent } from "./core/skiing-school/monitor-view/monitor-view.component";
import { PlanningViewComponent } from "./core/skiing-school/planning-view/planning-view.component";
import { PlanningEditionComponent } from "./core/skiing-school/planning-edition/planning-edition.component";
import { PlanningCreationComponent } from "./core/skiing-school/planning-creation/planning-creation.component";
import { PlanningHomeComponent } from "./core/skiing-school/planning-home/planning-home.component";
import { AuthGuard } from "./common/services/authentification/auth.guard";

import { PieChartEquipmentStatisticsComponent } from './core/SalesStatistics/equipment-pie-chart/pie-chart-equipment-statistics.component';
import { LineChartFlatReservationComponent } from './core/SalesStatistics/flat-line-chart/line-chart-flat-statistics.component';

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "login", component: AuthentificationComponent },
  { path: "createUser", component: CreateUserComponent },
  { path: "account", component: AccountComponent },
  {
    path: "shopCreation",
    component: ShopCreationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "hotelCreation",
    component: HotelCreationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "homestores",
    component: HomeStoresComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "homeHotels",
    component: HomeHotelsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "consultShop/:ShopId",
    component: ShopConsultationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "editShop/:ShopId",
    component: ShopEditionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "creationFlat/:hotelId",
    component: FlatCreationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "editHotel/:HotelId",
    component: HotelEditionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "creationFlatCategory",
    component: FlatCategoryCreationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "editFlat/:flatId",
    component: FlatEditionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "consultHotel/:hotelId",
    component: HotelConsultationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "homeFlats/:hotelId",
    component: HomeFlatsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "consultFlat/:flatId",
    component: FlatConsultationComponent,
    canActivate: [AuthGuard]
  },
  { path: "equipmentCreation/:ShopId", component: EquipmentCreationComponent },
  {
    path: "editEquipment/:EquipmentId",
    component: EquipmentEditionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "equipmenthomestores/:ShopId",
    component: EquipmentHomeStoresComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "consultEquipment/:EquipmentId",
    component: EquipmentConsultationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "flatReservation/:flatId",
    component: FlatReservationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "flatPriceDisplay/:flatId/:startDate/:endDate",
    component: FlatPriceDisplayComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "flatReservationConsultation",
    component: FlatReservConsultComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "consultReservEquip",
    component: ConsultReservEquipmentComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "createReservEquip/:EquipmentId",
    component: CreateEquipmentReservationComponent,
    canActivate: [AuthGuard]
  },
  { path: "liftReservation", component: LiftReservationComponent },
  {
    path: "createLiftReservation/:typeId",
    component: CreateLiftReservationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "homeReservation",
    component: HomeReservationComponent,
    canActivate: [AuthGuard]
  },
  { path: "home", component: HomeComponent },
  { path: "staff", component: StaffHomeComponent, canActivate: [AuthGuard] },
  {
    path: "listMaterials",
    component: EquipmentsToPrepareComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "distribEquipments",
    component: DistribEquipmentsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "takeBackEquipments",
    component: TakeBackEquipmentsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "customerCard/:userId",
    component: CustomerCardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "customerCardTbe/:userId",
    component: CustomerCardTbeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "createLiftReservation/:typeId",
    component: CreateLiftReservationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "homeReservation",
    component: HomeReservationComponent,
    canActivate: [AuthGuard]
  },
  { path: "home", component: HomeComponent },
  { path: "chat", component: ChatComponent },
  { path: "comment", component: CommentComponent },
  {
    path: "admin",
    component: HomeAdministrationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "seasonAdmin",
    component: SeasonAdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "flatAdmin",
    component: FlatAdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "equipmentAdmin",
    component: EquipmentAdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "skiLiftAdmin",
    component: SkiLiftAdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "monitor/:monitorId/:seasonId",
    component: PlanningHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "planningCreation/:seasonId/:monitorId",
    component: PlanningCreationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "planningEdition/:planningId/:seasonId/:monitorId",
    component: PlanningEditionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "planningView/:planningId",
    component: PlanningViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "monitors/:seasonId",
    component: MonitorViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "kindergarten/:seasonId",
    component: KindergartenComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "kindergartenPlanning/:seasonId/:nurseryId",
    component: KindergartenPlanningComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "kinderGartenCreation/:seasonId/:nurseryId",
    component: KindergartenCreationPlanningComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "kinderGartenUpdate/:nurseryPlanningId/:seasonId/:nurseryId",
    component: KindergartenUpdatePlanningComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "kinderGartenPlanningView/:nurseryPlanningId",
    component: KindergartenPlanningViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "season",
    component: SeasonViewComponent,
    canActivate: [AuthGuard]
  },

  { path: 'stats', component: PieChartEquipmentStatisticsComponent, canActivate: [AuthGuard] },
  { path: 'statsLog', component: LineChartFlatReservationComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
